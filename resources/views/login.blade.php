<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from uselooper.com/auth-signin-v2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 25 Sep 2019 14:10:02 GMT -->

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- End Required meta tags -->
    <!-- Begin SEO tag -->
    <title> Hello Book || Login </title>
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('/')}}assets/images/logo-removebg-preview.png">
    <link rel="shortcut icon" href="{{asset('/')}}assets/images/logo-removebg-preview.png">
    <!-- Favicons -->
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600" rel="stylesheet"><!-- End Google font -->
    <!-- BEGIN PLUGINS STYLES -->
    <link rel="stylesheet" href="{{asset('/')}}assets/vendor/fontawesome/css/all.css"><!-- END PLUGINS STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" href="{{asset('/')}}assets/stylesheets/theme.min.css" data-skin="default">
    <link rel="stylesheet" href="{{asset('/')}}css/custom.css">
</head>

<body>
<main class="container-fluid auth auth-floated">
    <div class="row">
        <div class="col-md-8">
            <div id="announcement" class="auth-announcement"
                 style="background-image:url('{{asset('/assets/images/Meeting Room Symbol.png')}}');background-repeat: no-repeat;background-position: center center;background-size: cover;">
                <img src="{{asset('/assets/images/unnamed.png')}}" alt="" class="login-img">
            </div>
        </div>
        <div class="col-md-4">
            <div class='auth-form'>
                <div class="mb-4">
                    <div class="mb-3 text-center">
                        <img class="rounded" src="{{asset('/')}}assets/images/logo-removebg-preview.png" alt="" height="72">
                    </div>
                </div>
                @include('status')
                    <form action="{{route('login')}}" method="POST">
                        {{csrf_field()}}
                        <div class="form-group mb-4">
                            <label class="d-block text-left" for="inputUser">Username</label>
                            <input type="text" id="inputUser" name="email" value="{{old('email')}}" class="form-control form-control-lg" required autofocus="" />
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group mb-4">
                            <label class="d-block text-left" for="inputPassword">Password</label>
                            <input type="password" id="inputPassword" name="password" value="{{old('password')}}" class="form-control form-control-lg" required>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group mb-4">
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign In</button>
                        </div><!-- /.form-group -->
                        <!-- copyright -->
                    </form><!-- /.auth-form -->
                    <p class="mb-0 px-3 text-muted text-center"> Silken Sewing © {{date('Y')}} All Rights Reserved.</p>
            </div>
        </div>
    </div>
</main><!-- /.auth -->
<!-- BEGIN BASE JS -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/popper.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script> <!-- END BASE JS -->
</body>
<!-- Mirrored from uselooper.com/auth-signin-v2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 25 Sep 2019 14:10:02 GMT -->

</html>
