@extends('layout.master')
@section('content')
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <!-- metric row -->
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-body">
                                <div class="container center-div d-flex justify-content-center align-items-center">
                                    <img src="{{asset(auth()->user()->image ? '/uploads/'.auth()->user()->image :'uploads/image.png') }}" alt="Avatar"  class="profile-image img-thumbnail">
                                    <form action="{{route('profile.update')}}" method="post" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="profile-overlay">
                                            <label for="file-input">
                                                <i class="fa fa-image profile-icon"></i>
                                            </label>
                                            <input id="file-input" type="file" name="image" onchange="submit()"/>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card" style="padding-bottom: 38px">
                        <div class="card-body">
                            <div class="card-title d-flex justify-content-between align-items-center">
                                <h5>User Info</h5>
                            </div>
                            <div class="row mb-2">
                                <div class="col-6 text-muted">Name:</div>
                                <div class="col-6">{{auth()->user()->name}}</div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-6 text-muted">Mobile No:</div>
                                <div class="col-6">{{auth()->user()->msisdn}}</div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-6 text-muted">Email:</div>
                                <div class="col-6">{{auth()->user()->email}}</div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-6 text-muted">User Name:</div>
                                <div class="col-6">{{auth()->user()->username}}</div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-6 text-muted">Employee ID:</div>
                                <div class="col-6">{{auth()->user()->employee_id}}</div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-6 text-muted">Department:</div>
                                <div class="col-6">{{auth()->user()->getDepartment->name}}</div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-6 text-muted">Designation:</div>
                                <div class="col-6">{{auth()->user()->getDesignation->name}}</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- /metric row -->
        </div><!-- /.section-block -->
    </div><!-- /.section-block -->
    <!-- grid row -->
@endsection
@push('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
        });
    </script>
@endpush
