@extends('layout.master')
@push('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css" rel="stylesheet">
    <style>
        /* Hide image title (caption) */
        .lb-data .lb-caption {
            display: none !important;
        }

        /* Move the close button to the top */
        .lb-close {
            top: 10px;         /* Adjust the position as needed */
            right: 10px;       /* Align it to the right */
            width: 30px;       /* Adjust size if needed */
            height: 30px;
            background: #000;  /* Set background color (optional) */
            color: #fff;       /* Set text color */
            border-radius: 50%; /* Optional: Make it a circular button */
            line-height: 30px; /* Center the "×" inside the button */
            text-align: center;
            opacity: 0.8;      /* Slightly transparent */
            z-index: 1050;     /* Ensure it stays on top of other elements */
        }
    </style>
@endpush
@section('content')
    @include('breadcrumb',['title' => 'Complain Detail'])
    <div class="main_content_iner">
        <section class="col-md-12 m-auto ">
            <div class="card">
                <div class="card-header m-0 p-3 d-flex align-items-center justify-content-between">
                    <h6 class="m-0"><i class="i-Clock"></i> {{ date('d M, Y h:i A', strtotime($complain->created_at)) }}</h6>
                    <div class="d-flex align-items-center gap-2">
                        <span class="badge badge-info mr-1" title="Complain solve estimated date">{{$complain->estimated_date}}</span>
                        <span class="badge badge-info mr-1" title="Complain solver name">{{$complain->registerTo->name}}</span>
                        <span class="badge {{complainstatusColorList($complain->getOriginal('status'))}} mr-1" title="Complain status">{{$complain->status}}</span>
                        @if($complain->rating)
                            <span class="badge badge-primary" title="Rating {{$complain->rating}}">{{$complain->rating}} <i class="fa fa-star"></i></span>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div>
                        <h3><i class="fa fa-tag"></i> {{ $complain->title }}</h3>
                    </div>
                    <div class="mt-3">
                        <h5>{!! $complain->description !!}</h5>
                    </div>
                    @if(in_array($complain->getOriginal('status'), [2,3]))
                        <div class="mt-3">
                            @if($complain->getOriginal('status') == 2)
                                <span class="badge badge-primary">Withdraw reason</span> : <span class="badge badge-info">{{$complain->withdraw_reason}}</span>
                            @else
                                <span class="badge badge-primary">Reject reason</span> : <span class="badge badge-info">{{$complain->reject_reason}}</span>
                            @endif
                        </div>
                    @endif
                </div>
            </div>
            @if (count($complain->images))
                <div class="card mt-1">
                    <div class="card-header"><i class="fa fa-images"></i> References </div>
                    <div class="card-body">
                        <div class="d-flex align-content-between flex-wrap">
                            @foreach ($complain->images as $key => $image)
                                <div class="p-2">
                                    <a href="{{ $image }}" class="lsb-preview" data-lightbox="complain_{{$complain->id}}" data-title="Image {{$key}}">
                                        <img class="img-fluid p-2 border rounded"
                                             src="{{ $image }}" style="height:150px"
                                             alt="">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            <div class="card mt-3">
                <div class="card-header row m-0 ">
                    <h6 class="fa fa-comment"> Comments</h6>
                </div>
                <div class="card-body ">
                    <div>
                        @foreach ($complain->comments as $comment)
                            <div
                                class="mb-1 p-2">
                                <div class="d-flex">
                                    <div class="d-block rounded">
                                        <img src="{{ asset('/uploads').'/'.$comment->author->image }}" alt="user image" style="height: 30px;width: 30px;border-radius: 100%;">
                                    </div>
                                    <div class="flex-grow-1 ml-2 ">
                                        <p>{{$comment->comment}}</p>
                                        @if($comment->image)
                                            <div class="p-2">
                                                <a href="{{ $comment->image }}" class="lsb-preview" data-lightbox="comment_{{$comment->id}}" data-title="Image {{$comment->id}}">
                                                    <img class="img-fluid p-2 border rounded"
                                                         src="{{ $comment->image }}" style="height:150px"
                                                         alt="">
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <span id="new_message_append"></span>

                    </div>
                    @if ($complain->status !== 5 && in_array(auth()->user()->id, [$complain->register_by, $complain->register_to]))
                        <div class="mb-5 mt-3 comment-input-div p-2 br_5" style="background-color: #d0d0d0">
                            <form action="{{route('complain.comment.add', $complain)}}" method="POST" enctype="multipart/form-data" >
                                {!! Form::token() !!}
                                <div class="row">
                                    <div class="col-md-9">
                                        <textarea class="form-control" id="commentbox" name="comment"></textarea>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="file" name="image" id="image" class="form-control">
                                    </div>
                                    <div class="col-md-12 text-right mt-2">
                                        <button class="btn btn-info">Send</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js"></script>
    <script>
        $(document).ready(function() {
            lightbox.option({
                'resizeDuration': 200,
                'wrapAround': true,       // Enable navigation looping
                'alwaysShowNavOnTouchDevices': true,
                'fadeDuration': 300,      // Smooth fade transitions
                'imageFadeDuration': 300, // Image fade transitions
            });
        });
    </script>
@endpush
