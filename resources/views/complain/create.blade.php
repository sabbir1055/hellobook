@extends('layout.master')
@push('style')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
@endpush
@section('content')
    @include('breadcrumb',['title' => 'Complain Register'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid mb-3">
                {!! Form::open(['route' => 'complain.store','method'=>'post', 'enctype'=>'multipart/form-data']) !!}
                {!! Form::token() !!}
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mx-sm-3 mb-2">
                                {!! Form::label('title','Title')!!}
                                {!! Form::text('title', old('title'), ['class'=>'form-control mb-3','required'=>'required','id'=>'title']);!!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mx-sm-3 mb-2">
                                {!! Form::label('register_to','Complain Solver')!!}
                                {!! Form::select('register_to', $complainSolvers, old('register_to'), ['class'=>'form-control mb-3','required'=>'required','placeholder' => 'Select one', 'id'=>'register_to']);!!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mx-sm-3 mb-2">
                                {!! Form::label('estimated_date','Estimated Date')!!}
                                {!! Form::text('estimated_date', old('estimated_date'), ['class'=>'form-control datepicker mb-3','required'=>'required','id'=>'estimated_date']);!!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group mx-sm-3 mb-2">
                                {!! Form::label('description','Description')!!}
                                {!! Form::textarea('description', old('description'), ['class'=>'form-control mb-3', 'id' => 'description']);!!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mx-sm-3 mb-2">
                                {!! Form::label('images','References')!!}
                                {!! Form::file('images[]', ['class'=>'form-control mb-3','id'=>'images', 'multiple' => true]);!!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mx-sm-3 mb-2">
                                {!! Form::label('link_complain_id','Repeated Issue ID')!!}
                                {!! Form::text('link_complain_id', old('link_complain_id'), ['class'=>'form-control mb-3','id'=>'link_complain_id']);!!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary add-btn">Create</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#description').summernote();
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date()
            });
        });
    </script>
@endpush
