@extends('layout.master')
@push('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        .rating {
            display: flex;
            flex-direction: row-reverse;
            justify-content: center;
        }

        .rating > input{ display:none;}

        .rating > label {
            position: relative;
            width: 1em;
            font-size: 6vw;
            color: #FFD600;
            cursor: pointer;
        }
        .rating > label::before{
            content: "\2605";
            position: absolute;
            opacity: 0;
        }
        .rating > label:hover:before,
        .rating > label:hover ~ label:before {
            opacity: 1 !important;
        }

        .rating > input:checked ~ label:before{
            opacity:1;
        }

        .rating:hover > input:checked ~ label:before{ opacity: 0.4; }

        h1, p{
            text-align: center;

        }

        h1{
            margin-top:150px;
        }
        p{ font-size: 1.2rem;}
        @media only screen and (max-width: 600px) {
            h1{font-size: 14px;}
            p{font-size: 12px;}
        }
    </style>
@endpush
@section('content')
    @if($viewType == 0)
        @permission('register_complain')
        <header class="page-title-bar">
            <a href="{{route('complain.create')}}"><span class="fas fa-plus fa-2x text-white"></span></a>
        </header>
        @endpermission
    @endif
    @include('breadcrumb',['title' => 'Complain List'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid mb-3">
                <div class="card-body">
                    <form action="">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('f_registered_date','Registered Date')!!}
                                    {!! Form::text('f_registered_date', @$req['f_registered_date'], ['class'=>'form-control mb-3','id'=>'f_registered_date']);!!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('f_done_date','Done Date')!!}
                                    {!! Form::date('f_done_date', @$req['f_done_date'], ['class'=>'form-control mb-3','id'=>'f_done_date']);!!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('f_status','Status')!!}
                                    {!! Form::select('f_status', [''=>'Select a status']+$statusList, @$req['f_status'], ['class'=>'form-control mb-3 select2','id'=>'f_status']);!!}
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">&nbsp</label>
                                    <div class="btn-group btn-block" role="group" aria-label="Basic example">
                                        <button type="submit" class="btn btn-primary">Filter</button>
                                        @permission('can_all_complain_download')
                                            <a href="{{request()->fullUrlWithQuery(['download' => 'yes'])}}" class="btn btn-secondary">
                                                <i class="fa fa-download"></i>
                                            </a>
                                        @endpermission
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <div class="card card-fluid">
                <div class="table-responsive table-bordered">
                    <table class="table">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL</th>
                            <th> Complain ID</th>
                            @if($viewType != 0)
                                <td>Registered By</td>
                            @endif
                            <th> Title</th>
                            <th> Repeated Complain</th>
                            <th> Complain Receiver </th>
                            <th> Registered Date</th>
                            <th> Estimated Date</th>
                            <th> Done Date</th>
                            <th> Status</th>
                            <th> Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $complains->appends($req)->firstItem(); @endphp
                        @foreach($complains as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->complain_id}} </td>
                                @if($viewType != 0)
                                    <td class="align-middle"> {{$value->registerBy->name ?? null}} </td>
                                @endif
                                <td class="align-middle"> {{$value->title}} </td>
                                <td class="align-middle">
                                    @if($value->linkedComplainUrl)
                                        <a href="{{$value->linkedComplainUrl}}" target="_blank">
                                            View repeated complain
                                        </a>
                                    @else
                                        N\A
                                    @endif
                                </td>
                                <td class="align-middle"> {{ $value->registerTo ? $value->registerTo->name . "(".$value->registerTo->employee_id.")" : null }} </td>
                                <td class="align-middle"> {{$value->registerd_date}} </td>
                                <td class="align-middle"> {{$value->estimated_date}} </td>
                                <td class="align-middle"> {{$value->done_date ?? 'N\A'}} </td>
                                <td class="align-middle"> <i class="badge {{complainstatusColorList($value->getOriginal('status'))}}">{{$value->status}}</i> </td>
                                <td class="align-middle">
                                    <a href="{{route("complain.view", [$value])}}">
                                        <button class="btn btn-sm btn-icon btn-secondary" title="view detail"><i
                                                class="fa fa-eye"></i></button>
                                    </a>
                                    @if(in_array(auth()->user()->id, [$value->register_by, $value->register_to]) && $viewType !== 2 && !in_array($value->getOriginal('status'), [2,3,5]))
                                        <a href="#">
                                            <button class="btn btn-danger btn-sm update_complain_status"
                                                    title="Status update of complain" data-complain="{{$value->id}}">
                                                <i class="fas fa-cogs"></i>
                                            </button>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example" class="m-3">
                    <span>Showing {{ $complains->appends($req)->firstItem() }} to {{ $complains->appends($req)->lastItem() }} of {{ $complains  ->appends($req)->total() }} entries</span>
                    <div>{{ $complains->appends($req)->render() }}</div>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @if(in_array($viewType, [0,1]))
        @include('modals.complain.complainStatusModal', ['registerBy' => $viewType === 0])
    @endif
@endsection
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        $(function () {
            $('#f_registered_date').daterangepicker({
                opens: 'left', // Position of the calendar
                // startDate: moment().subtract(7, 'days'), // Default start date (e.g., 7 days ago)
                // endDate: moment(), // Default end date (today)
                locale: {
                    format: 'YYYY-MM-DD', // Date format
                    separator: ' TO ', // Separator between start and end dates
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                },
            });
        });

        $('.update_complain_status').click(function () {
            var complain_id = $(this).data('complain')
            $('#complain_id').val(complain_id)
            $('#complainStatusModal').modal('show')
        })

        $('#status').change(function (event){
            var status = event.target.value;
            if(status == 2 || status == 3){
                $('.reason_block').removeClass('d-none')
            } else {
                $('.reason_block').addClass('d-none')
            }
            if(status == 5){
                $('.rating_block').removeClass('d-none')
            } else {
                $('.rating_block').addClass('d-none')
            }
        })
    </script>
@endpush
