@extends('layout.master')
@section('content')
    @if($sim_request->is_accepted === 0)
        @include('common.updateButton')
    @endif
    @include('breadcrumb', ['title' => 'SIM Request Detail'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-center mb-0">
                            <tbody>
                            <tr>
                                <td class="attr-title"><b>Name</b> </td>
                                <td class="attr-dot"> : </td>
                                <td> {{$sim_request->getUser->name}}</td>
                            </tr>
                            <tr>
                                <td class="attr-title"><b>Email</b> </td>
                                <td class="attr-dot"> : </td>
                                <td> {{$sim_request->getUser->email}}</td>
                            </tr>
                            <tr>
                                <td class="attr-title"><b>Limit</b> </td>
                                <td class="attr-dot"> : </td>
                                <td> {{$sim_request->limit}} Tk</td>
                            </tr>
                            <tr>
                                <td class="attr-title"><b>Priority</b> </td>
                                <td class="attr-dot"> : </td>
                                <td> {{[1=>'Low',2=>'Mid',3=>'High'][$sim_request->priority]}}</td>
                            </tr>
                            <tr>
                                <td class="attr-title"><b>Subject</b> </td>
                                <td class="attr-dot"> : </td>
                                <td> {{$sim_request->subject}}</td>
                            </tr>
                            <tr>
                                <td class="attr-title"><b>Messsage</b> </td>
                                <td class="attr-dot"> : </td>
                                <td>
                                    <div class="mail-body">
                                        {{$sim_request->reason}}
                                    </div>
                                </td>
                            </tr><tr>
                                <td class="attr-title"><b>Attachments</b> </td>
                                <td class="attr-dot"> : </td>
                                <td>
                                    @if($sim_request->attachnent)
                                        <div class="image-box">
                                            @if(exif_imagetype(asset('/').'uploads/'.$sim_request->attachnent))
                                                <img src="{{asset('/').'uploads/'.$sim_request->attachnent}}"  class="img img-thumbnail img-responsive" alt="" srcset="">
                                            @else
                                                <a href="asset('/uploads').'/'.$sim_request->attachnent"> Click to download file and see </a>
                                            @endif
                                            @else
                                                <p style="color: red">No Attachment found</p>
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.simRequest.updateSimRequest')
@endsection
@push('script')
    <script>
        $('#edit-btn').click(function(){
            $('#updateSimRequestModal').modal('show')
        })
    </script>
@endpush
