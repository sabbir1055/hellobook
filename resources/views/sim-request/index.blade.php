@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{asset('/')}}assets/select2/select2.min.css">
@endsection
@section('content')
    @permission('add_sim_request','global_sim_request')
    @include('common.addButton')
    @endpermission
    @include('breadcrumb', ['title' => 'SIM Request List'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid">
                <div class="table-responsive">
                    <table class="table text-center table-bordered">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> Requested By </th>
                            <th> Request For </th>
                            <th> Limit </th>
                            <th> Request Date </th>
                            <th> Approve Date </th>
                            <th> Status </th>
                            <th> Decline Reason </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $sim_request->appends($req)->firstItem(); @endphp
                        @foreach($sim_request as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->getRequestUser->name}} </td>
                                <td class="align-middle"> {{$value->getUser->name}} </td>
                                <td class="align-middle"> Tk. {{$value->limit ?? 0}} </td>
                                <td class="align-middle"> {{date('Y-m-d',strtotime($value->created_at))}} </td>
                                <td class="align-middle"> {{ $value->approve_date ? date('Y-m-d h:i A',strtotime($value->approve_date)) : null }} </td>
                                <td class="align-middle">
                                    @if($value->getStatus == 'Pending')
                                        <span class="badge badge-pill badge-warning" style="background:yellow">{{$value->getStatus}}</span>
                                    @elseif($value->getStatus == 'Declined')
                                        <span class="badge badge-pill badge-danger" style="background:red">{{$value->getStatus}}</span>
                                    @else
                                        <span class="badge badge-pill badge-success" style="background:green">{{$value->getStatus}}</span>
                                    @endif
                                </td>
                                <td class="align-middle"> {{$value->decline_reason}} </td>
                                <td class="align-middle">
                                    <a href="{{route('sim.request.view',$value->id)}}" class="btn btn-info btn-sm">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example" class="m-3">
                    <span>Showing {{ $sim_request->appends($req)->firstItem() }} to {{ $sim_request->appends($req)->lastItem() }} of {{ $sim_request  ->appends($req)->total() }} entries</span>
                    <div>{{ $sim_request->appends($req)->render() }}</div>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.simRequest')
@endsection
@push('script')
    <script src="{{asset('/')}}assets/select2/select2.min.js"></script>
    <script>
        $(document).ready(() => {
            $('#simRequestModal').on('shown.bs.modal', function () {
                $('.select2').select2({
                    dropdownParent: $('#simRequestModal'),
                    width: '100%'
                })
            });

            // Destroy Select2 instance when the modal is hidden
            $('#simRequestModal').on('hidden.bs.modal', function () {
                $('.select2').each(function() {
                    if ($(this).data('select2')) {
                        $(this).select2('destroy');
                    }
                });
            });
        })
        $('#add-new-btn').click(function(){
            $('.add-btn').text('Add');
            $('#simRequestModalLabel').text('Add Request');
            var url = "{{route('sim.request.store')}}";
            $('#sim_request_form').attr('action', url);
            $('#sim_request_form').find("input[type=text],input[type=textarea]").val("");
            $('.select2').select2()
            $('#simRequestModal').modal('show')
        })
    </script>
@endpush
