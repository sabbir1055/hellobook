@extends('layout.master')
@section('content')
    @include('breadcrumb', ['title' => 'SIM Request List'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card">
                <div class="table-responsive">
                    <table class="table text-center table-bordered">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> Requested By </th>
                            <th> Request For </th>
                            <th> Request Date </th>
                            <th> Approve Date </th>
                            <th> Limit </th>
                            <th> Status </th>
                            <th> Decline Reason </th>
                            <th> Already Assigned Number </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $sim_request->appends($req)->firstItem(); @endphp
                        @foreach($sim_request as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->getRequestUser->name}} </td>
                                <td class="align-middle"> {{$value->getUser->name}} </td>
                                <td class="align-middle"> {{date('Y-m-d',strtotime($value->created_at))}} </td>
                                <td class="align-middle"> {{$value->approve_date ? date('Y-m-d h:i A',strtotime($value->approve_date)) : null}} </td>
                                <td class="align-middle"> Tk {{$value->limit}} </td>
                                <td class="align-middle">
                                    @if($value->getStatus == 'Pending')
                                        <span class="badge badge-warning" style="background:yellow">{{$value->getStatus}}</span>
                                    @elseif($value->getStatus == 'Declined')
                                        <span class="badge badge-danger" style="background:red">{{$value->getStatus}}</span>
                                    @else
                                        <span class="badge badge-success" style="background:green">{{$value->getStatus}}</span>
                                    @endif
                                </td>
                                <td class="align-middle"> {{$value->decline_reason}} </td>
                                <td class="align-middle"> {{ $value->getUser->assignedNumber->msisdn ?? 'N/A'}} </td>
                                <td class="align-middle">
                                    <a href="{{route('sim_rqeuest_action.view',$value->id)}}" data-sim="{{$value}}" class="btn btn-primary btn-sm accept-btn">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example" class="m-3">
                    <span>Showing {{ $sim_request->appends($req)->firstItem() }} to {{ $sim_request->appends($req)->lastItem() }} of {{ $sim_request  ->appends($req)->total() }} entries</span>
                    <div>{{ $sim_request->appends($req)->render() }}</div>
                </nav>
            </div>
        </div>
    </div>
@endsection

