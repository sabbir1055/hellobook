<script src="{{asset('/')}}js/jquery-3.4.1.min.js"></script>
<script src="{{asset('/')}}js/popper.min.js"></script>
<script src="{{asset('/')}}js/bootstrap.min.js"></script>
<script src="{{asset('/')}}js/metisMenu.js"></script>
<script src="{{asset('/')}}vendors/count_up/jquery.waypoints.min.js"></script>
<script src="{{asset('/')}}vendors/count_up/jquery.counterup.min.js"></script>
<script src="{{asset('/')}}vendors/datatable/js/jszip.min.js"></script>
<script src="{{asset('/')}}vendors/datatable/js/pdfmake.min.js"></script>
<script src="{{asset('/')}}vendors/datatable/js/vfs_fonts.js"></script>
<script src="{{asset('/')}}vendors/tagsinput/tagsinput.js"></script>
<script src="{{asset('/')}}js/bootstrap-datepicker.min.js"></script>
<script src="{{asset('/')}}js/custom.js"></script>
@yield('script')
<script>
    function activeMenu() {
        $('a[href="' + current_url + '"]').addClass('active');
        $('a[href="' + current_url + '"]').closest('ul').addClass('mm-show')
        $('a[href="' + current_url + '"]').closest('ul').parent('li').addClass('mm-active')
    }
    function passwordModal() {
        var old_pass = $('#old_password').val('');
        var new_pass = $('#new_password').val('');
        var confirm_pass = $('#confirm_password').val('');
        $('#password_change_button').attr('disabled', true);
        $('#passwordModal').modal({
            show: true,
            backdrop: true
        });
    }
    $(document).ready(function () {
        activeMenu()
        $(".alert-message").delay(10000).fadeOut("slow");
        $('#new_password').keyup(function() {
            var old_pass = $('#old_password').val();
            var new_pass = $(this).val();
            var confirm_pass = $('#confirm_password').val();
            if (new_pass === confirm_pass && new_pass.length > 5 && confirm_pass.length > 5 && old_pass) {
                $(this).css({
                    'border-color': 'green'
                });
                $('#confirm_password').css({
                    'border-color': 'green'
                });
                $('#password_change_button').attr('disabled', false);
            } else {
                $(this).css({
                    'border-color': 'red'
                });
                $('#password_change_button').attr('disabled', true);
                if (confirm_pass) {
                    $('#confirm_password').css({
                        'border-color': 'red'
                    });
                }
            }
        });
        $('#confirm_password').keyup(function() {
            var old_pass = $('#old_password').val();
            var new_pass = $('#new_password').val();
            var confirm_pass = $(this).val();
            if (new_pass === confirm_pass && new_pass.length > 5 && confirm_pass.length > 5 && old_pass) {
                $(this).css({
                    'border-color': 'green'
                });
                $('#new_password').css({
                    'border-color': 'green'
                });
                $('#password_change_button').attr('disabled', false);
            } else {
                $(this).css({
                    'border-color': 'red'
                });
                $('#password_change_button').attr('disabled', true);
                if (new_pass) {
                    $('#new_password').css({
                        'border-color': 'red'
                    });
                }
            }
        });
        $('#old_password').keyup(function() {
            var old_pass = $('#old_password').val();
            var new_pass = $('#new_password').val();
            var confirm_pass = $('#confirm_password').val();
            if (new_pass === confirm_pass && new_pass.length > 5 && confirm_pass.length > 5 && old_pass) {
                $(this).css({
                    'border-color': 'green'
                });
                $('#password_change_button').attr('disabled', true);
                $('#confirm_password').css({
                    'border-color': 'green'
                });
                $('#password_change_button').attr('disabled', false);
            }
        });
    })
    $(document).ready(function (){
        getNotifications()
        // setInterval(()=> {
        //     getNotifications()
        // }, 10000);
    })

    function getNotifications(){
        $.ajax({
            url: "{{route('notification.all')}}",
            method: 'POST',
            'data': {
                '_token': "{{csrf_token() }}"
            },
            success: function(data) {
                var dom = ``;
                if(data){
                    dom = data.html
                    $('.header-nav-dropdown').addClass('has-notified')
                }
                if(data.count > 0) {
                    $('.notification-count').removeClass('d-none')
                    $('.notification-count').text(data.count)
                } else {
                    $('.notification-count').addClass('d-none')
                }
                $('.notification-data').html(dom)
            },
            err: function(err) {
                alert('No data found !!!!')
            }
        });
    }
</script>
@stack('script')
