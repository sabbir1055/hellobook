<!DOCTYPE html>
<html lang="zxx">
@include('layout.header')
<body class="crm_body_bg">
@include('layout.sidebar')
<section class="main_content dashboard_part large_header_bg">
    <!-- menu  -->
    <div class="container-fluid no-gutters">
        <div class="row">
            <div class="col-lg-12 p-0">
                <div class="header_iner d-flex justify-content-between align-items-center bg-silken">
                    <div class="sidebar_icon d-lg-none">
                        <i class="ti-menu"></i>
                    </div>
                    <div class="line_icon open_miniSide d-none d-lg-block">
                        <img src="{{asset('/')}}img/line_img_white.png" alt="">
                    </div>
                    <div class="header_right d-flex justify-content-between align-items-center">
                        <div class="header_notification_warp d-flex align-items-center">
                            <li>
                                <a class="bell_notification_clicker nav-link-notify" href="#"> <img src="{{asset('/')}}img/icon/bell.svg" alt="">
                                    <div class="text-center notification-count d-none">-</div>
                                </a>
                                <!-- Menu_NOtification_Wrap  -->
                                <div class="Menu_NOtification_Wrap">
                                    <div class="notification_Header">
                                        <h4>Notifications</h4>
                                    </div>
                                    <div class="Notification_body notification-data"><!--/ DATA LOAD FROM JQUERY AXIOS  --></div>
                                    <div class="nofity_footer">
                                        <div class="submit_button text-center pt_20">
                                            <a href="{{route('read.all')}}" class="btn_1 green">Mark All As Read</a>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </div>
                        <div class="profile_info d-flex align-items-center">
                            <div class="profile_thumb mr_20">
                                <img src="{{asset('/uploads').'/'.auth()->user()->image}}" alt="#">
                            </div>
                            <div class="author_name">
                                <h4 class="f_s_15 f_w_500 mb-0 text-white">{{auth()->user()->name}}</h4>
                                <p class="f_s_12 f_w_400 text-white">{{auth()->user()->getDesignation->name}}</p>
                            </div>
                            <div class="profile_info_iner">
                                {{--                                <div class="profile_author_name">--}}
                                {{--                                    <p>{{auth()->user()->name}}</p>--}}
                                {{--                                    <H5>{{auth()->user()->getDesignation->name}}</H5>--}}
                                {{--                                </div>--}}
                                <div class="profile_info_details">
                                    <a href="{{route('profile')}}">My Profile </a>
                                    <a href="javascript:void(0)" onclick="passwordModal()">Change Password</a>
                                    <a href="{{route('logout')}}">Log Out </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ menu  -->
    <div class="main_content_iner overly_inner ">
        @include('status')
        @yield('content')
    </div>
</section>
@yield('modal')
@include('modals.passwordChange')
@include('layout.footer')
</body>
</html>
