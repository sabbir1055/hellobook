<nav class="sidebar dark_sidebar">
    <div class="logo d-flex justify-content-between">
        <a class="large_logo" href="{{route('dashboard')}}"><img src="{{asset('/')}}assets/images/brand-logo.png" alt=""></a>
        <a class="small_logo" href="{{route('dashboard')}}"><img src="{{asset('/')}}assets/images/main_logo.png" alt=""></a>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu">
        <li class="">
            <a href="{{route('dashboard')}}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fas fa-home"></i>
                </div>
                <div class="nav_title">
                    <span>Dashboard </span>
                </div>
            </a>
        </li>

        <!-- Room manage -->
        @permission('manage_room')
        <li class="">
            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fas fa-bookmark"></i>
                </div>
                <div class="nav_title">
                    <span>Meting Room</span>
                </div>
            </a>
            <ul>
                @permission('room_list')
                <li>
                    <a href="{{route('room.index')}}">
                        <span class="menu-icon fa fa-university"></span> Room List
                    </a>
                </li>
                @endpermission
                @permission('booked_room_list')
                <li class="menu-item">
                    <a href="{{route('booking-room.index')}}">
                        <span class="menu-icon fa fa-calendar-alt"></span> Book a room
                    </a>
                </li>
                @endpermission
            </ul>
        </li>
        @endpermission

        <!-- Sim manage -->
        @permission('sim_manage')
        <li class="">
            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fas fa-sim-card"></i>
                </div>
                <div class="nav_title">
                    <span>Sim Management</span>
                </div>
            </a>
            <ul>
                @permission('sim_add_assign')
                <li>
                    <a href="{{route('sim.index')}}">
                        <span class="menu-icon fa fa-list-alt"></span> Sim Add\Assign
                    </a>
                </li>
                @endpermission
                @permission('sim_request_pending_list')
                <li>
                    <a href="{{route('sim.request.list')}}">
                        <span class="menu-icon fa fa-th-list"></span> Sim Request Status
                    </a>
                </li>
                @endpermission
                <li>
                    <a href="{{route('sim.report',1)}}">
                        <span class="menu-icon fa fa-chart-pie"></span> Assigned Sim Report
                    </a>
                </li>
                <li>
                    <a href="{{route('sim.report',0)}}">
                        <span class="menu-icon fa fa-chart-bar"></span> UnAssigned Sim Report
                    </a>
                </li>
            </ul>
        </li>
        @endpermission

        @if(auth()->user()->can(['register_complain','solve_complain','view_complains','view_subordinate_complains']))
            <li class="">
                <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                    <div class="nav_icon_small">
                        <i class="fas fa-wrench"></i>
                    </div>
                    <div class="nav_title">
                        <span>Complain Management</span>
                    </div>
                </a>
                <ul>
                    @permission('register_complain')
                    <li>
                        <a href="{{route('complain.index')}}">
                            <span class="menu-icon fa fa-list-alt"></span> Register Complain
                        </a>
                    </li>
                    @endpermission
                    @permission('solve_complain')
                    <li>
                        <a href="{{route('complain.solvable.index')}}">
                            <span class="menu-icon fa fa-th-list"></span> Receive Complains
                        </a>
                    </li>
                    @endpermission
                    @permission(['view_all_complains','view_subordinate_complains'])
                    <li>
                        <a href="{{route('complain.all.index')}}">
                            <span class="menu-icon fa fa-th-list"></span> All Complains
                        </a>
                    </li>
                    @endpermission
                </ul>
            </li>
        @endif

        @permission(['transport_management'])
        <li class="">
            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fas fa-car"></i>
                </div>
                <div class="nav_title">
                    <span>Transport Management</span>
                </div>
            </a>
            <ul>
                @permission('add_transport')
                <li>
                    <a href="{{route('transport.index')}}">
                        <span class="menu-icon fa fa-list-alt"></span> Transport List
                    </a>
                </li>
                @endpermission
                @permission('approve_reject_transport_request')
                <li>
                    <a href="{{route('transport.request.approval.index')}}">
                        <span class="menu-icon fa fa-th-list"></span> Requisition List
                    </a>
                </li>
                @endpermission
            </ul>
        </li>
        @endpermission

        @permission('add_transport_request')
        <li>
            <a href="{{route('transport.request.index')}}">
                <span class="menu-icon fa fa-th-list"></span> Add Transport Request
            </a>
        </li>
        @endpermission

        @permission('sim_request_list')
        <li class="">
            <a href="{{route('sim.request')}}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fas fa-hand-holding"></i>
                </div>
                <div class="nav_title">
                    <span>Sim Request </span>
                </div>
            </a>
        </li>
        @endpermission

        @permission('manage_user')
        <li class="">
            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fas fa-users-cog"></i>
                </div>
                <div class="nav_title">
                    <span>User Management</span>
                </div>
            </a>
            <ul>
                @permission('manage_permission')
                <li>
                    <a href="{{route('permission.index')}}">
                        <span class="menu-icon fa fa-lock"></span> Permissions
                    </a>
                </li>
                @endpermission
                @permission('manage_role')
                <li>
                    <a href="{{route('role.index')}}">
                        <span class="menu-icon fa fa-list"></span>Roles
                    </a>
                </li>
                @endpermission
                @permission('manage_user')
                <li>
                    <a href="{{route('user.index')}}?f_status=1">
                        <span class="menu-icon fa fa-user"></span>Users
                    </a>
                </li>
                @permission('add_bulk_user')
                <li>
                    <a href="{{route('user.bulk')}}">
                        <span class="menu-icon fa fa-users"></span>Add Bulk User
                    </a>
                </li>
                @endpermission
                @endpermission
            </ul>
        </li>
        @endpermission

        @permission('designation_manage')
        <li class="">
            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fas fa-cog"></i>
                </div>
                <div class="nav_title">
                    <span>Designation Management</span>
                </div>
            </a>
            <ul>
                @permission('designation_manage')
                <li>
                    <a href="{{route('designation.index')}}">
                        <span class="menu-icon fa fa-user-graduate"></span>Designation
                    </a>
                </li>
                @endpermission
                @permission('department_manage')
                <li>
                    <a href="{{route('department.index')}}">
                        <span class="menu-icon fa fa-address-book"></span>Department
                    </a>
                </li>
                @endpermission
            </ul>
        </li>
        @endpermission
    </ul>
</nav>
