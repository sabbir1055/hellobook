<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>HelloBook || @yield('title')</title>
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('/')}}assets/images/logo-removebg-preview.png">
    <link rel="shortcut icon" href="{{asset('/')}}assets/images/logo-removebg-preview.png">
    <link rel="icon" href="{{asset('/')}}assets/images/logo-removebg-preview.png">
    <link rel="stylesheet" href="{{asset('/')}}css/bootstrap.min.css" />
    <link rel="stylesheet" href="{{asset('/')}}vendors/themefy_icon/themify-icons.css" />
    <link rel="stylesheet" href="{{asset('/')}}vendors/font_awesome/css/all.min.css" />
    <link rel="stylesheet" href="{{asset('/')}}vendors/tagsinput/tagsinput.css" />
    <link rel="stylesheet" href="{{asset('/')}}vendors/scroll/scrollable.css" />
    <link rel="stylesheet" href="{{asset('/')}}vendors/morris/morris.css">
    <link rel="stylesheet" href="{{asset('/')}}vendors/material_icon/material-icons.css" />
    <link rel="stylesheet" href="{{asset('/')}}css/metisMenu.css">
    <link rel="stylesheet" href="{{asset('/')}}css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="{{asset('/')}}css/style.css" />
    <link rel="stylesheet" href="{{asset('/')}}css/colors/default.css" id="colorSkinCSS">
    <link rel="stylesheet" href="{{asset('/')}}css/custom.css" />
    @yield('style')
    @stack('style')
    <script type="text/javascript">
        var current_url = "{{ url()->current() }}";
    </script>
</head>
