@extends('layout.master')
@section('content')
    @permission('manage_permission')
    @include('common.addButton')
    @endpermission
    @include('breadcrumb',['title' => 'Permission List'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> Name </th>
                            <th> Display Name </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $permission->appends($req)->firstItem(); @endphp
                        @foreach($permission as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->name}} </td>
                                <td class="align-middle"> {{$value->display_name}} </td>
                                <td class="align-middle">
                                    <a href="javascript:void(0)" data-permission="{{$value}}" class="edit-btn">
                                        <button class="btn btn-sm btn-icon btn-secondary "><i class="fa fa-pencil-alt"></i></button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example" class="m-3">
                    <span>Showing {{ $permission->appends($req)->firstItem() }} to {{ $permission->appends($req)->lastItem() }} of {{ $permission  ->appends($req)->total() }} entries</span>
                    <div>{{ $permission->appends($req)->render() }}</div>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.permissionModal')
@endsection
@push('script')
    <script>
        $('#add-new-btn').click(function(){
            $('.add-btn').text('Add');
            $('#permissionModalLabel').text('Create Permission');
            var url = "{{route('permission.store')}}";
            $('#permission_form').attr('action', url);
            $('#permission_form').find("input[type=text],input[type=textarea]").val("");
            $('#permissionModal').modal('show')
        })
        $('.edit-btn').click(function(){
            $('.add-btn').text('Update');
            $('#permissionModalLabel').text('Update Permission');
            var permission_data = $(this).data('permission');
            var url = window.location.pathname+'/'+permission_data.id+'/update';
            $('#permission_form').attr('action', url);
            $('#name').val(permission_data.name)
            $('#display_name').val(permission_data.display_name)
            $('#description').val(permission_data.description)
            $('#permissionModal').modal('show')
        })
    </script>
@endpush
