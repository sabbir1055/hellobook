@extends('layout.master')
@section('content')
    @include('breadcrumb', ['title' => $id==0 ? 'UnAssigned Sim Report' : 'Assigned Sim Report'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid">
                <div class="table-responsive">
                    <table class="table text-center table-bordered">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> No </th>
                            @if($id == 1)
                                <th> Max Amount (Tk) </th>
                                <th> Assigned To </th>
                            @else
                                <th> Last user </th>
                                <th> Unassigned Date</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $sim->appends($req)->firstItem(); @endphp
                        @foreach($sim as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->msisdn}} </td>
                                @if($id == 1)
                                    <td class="align-middle"> {{$value->max_amount}} </td>
                                    <td class="align-middle"> {{$value->assignedUser->name ?? 'Not Assigned'}} </td>
                                @else
                                    <td class="align-middle"> {{$value->assignedUser->name ?? 'Not Assigned'}} </td>
                                    <td class="align-middle"> {{($value->getUnAssigned ?  $value->getUnAssigned->unassigned_date ? date('Y-m-d h:i A ',strtotime($value->getUnAssigned->unassigned_date)) : null  : null)  }} </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example" class="m-3">
                    <span>Showing {{ $sim->appends($req)->firstItem() }} to {{ $sim->appends($req)->lastItem() }} of {{ $sim  ->appends($req)->total() }} entries</span>
                    <div>{{ $sim->appends($req)->render() }}</div>
                </nav>
            </div>
        </div>
    </div>
@endsection
