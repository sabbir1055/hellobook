@extends('layout.master')

@push('css')
    <style>
        .hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }
    </style>
@endpush

@section('content')
    @include('breadcrumb', ['title' => 'Room List'])
    @permission('add_room')
    @include('common.addButton')
    @endpermission
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> Name </th>
                            <th> Short Name </th>
                            <th> Room Location </th>
                            <th> Room Capacity</th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $room->appends($req)->firstItem(); @endphp
                        @foreach($room as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->name}} </td>
                                <td class="align-middle"> {{$value->short_name}} </td>
                                <td class="align-middle"> {{$value->room_no}} </td>
                                <td class="align-middle"> {{$value->available_seat}} </td>
                                <td class="align-middle">
                                    @permission('add_room')
                                    <a href="javascript:void(0)" data-room="{{$value}}" class="edit-btn">
                                        <button class="btn btn-sm btn-icon btn-secondary"> <i class="fa fa-pencil-alt"></i> </button>
                                    </a>
                                    @endpermission
                                    <a href="javascript:void(0)" class="edit-btn accordion-toggle" data-toggle="collapse" data-target="#demo{{$value->id}}">
                                        <button class="btn btn-sm btn-icon btn-secondary "><i class="fa fa-eye"></i></button>
                                    </a>
                                </td>
                            </tr>
                            <tr >
                                <td colspan="8" class="hiddenRow">
                                    <div class="accordian-body collapse" id="demo{{$value->id}}">
                                        <table class="table table-bordered">
                                            <tr class="text-center">
                                                <td>#SL</td>
                                                <td>Meetig Date</td>
                                                <td>Meeting Start</td>
                                                <td>Meeting End</td>
                                                <td>Meeting Host</td>
                                            </tr>
                                            @foreach($value->getMeetings as $key=>$meeting)
                                                <tr class="text-center">
                                                    <td>{{++$key}}</td>
                                                    <td>{{$meeting->booked_date ?? null }}</td>
                                                    <td>{{date('h:i A',strtotime($meeting->booked_time_from)) ?? null }}</td>
                                                    <td>{{date('h:i A',strtotime($meeting->booked_time_to)) ?? null }}</td>
                                                    <td>{{$meeting->bookedBy->name ?? null }}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example" class="m-3">
                    <span>Showing {{ $room->appends($req)->firstItem() }} to {{ $room->appends($req)->lastItem() }} of {{ $room  ->appends($req)->total() }} entries</span>
                    <div>{{ $room->appends($req)->render() }}</div>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.roomModal')
@endsection
@push('script')
    <script>
        $('#add-new-btn').click(function(){
            $('.add-btn').text('Add')
            $('#roomModalLabel').text('Create Room')
            var url = "{{route('room.store')}}"
            $('#user_form').attr('action', url)
            $('#user_form').find("input[type=text]").val("")
            $('#roomModal').modal('show')
        })
        $('.edit-btn').click(function(){
            $('.add-btn').text('Update');
            $('#roomModalLabel').text('Update Room')
            var room_data = $(this).data('room')
            var url = window.location.pathname+'/'+room_data.id+'/update'
            $('#user_form').attr('action', url)
            $('#name').val(room_data.name)
            $('#short_name').val(room_data.short_name)
            $('#room_no').val(room_data.room_no)
            $('#available_seat').val(room_data.available_seat)
            $('#roomModal').modal('show')
        })
    </script>
@endpush
