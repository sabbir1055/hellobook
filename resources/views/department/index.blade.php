@extends('layout.master')
@section('content')
    @permission('department_manage')
    @include('common.addButton')
    @endpermission
    @include('breadcrumb',['title' => 'Department List'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> Name </th>
                            <th> Short Name </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $department->appends($req)->firstItem(); @endphp
                        @foreach($department as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->name}} </td>
                                <td class="align-middle"> {{$value->short_name}} </td>
                                <td class="align-middle">
                                    <a href="javascript:void(0)" data-department="{{$value}}" class="edit-btn">
                                        <button class="btn btn-sm btn-icon btn-secondary "><i class="fa fa-pencil-alt"></i></button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example" class="m-3">
                    <span>Showing {{ $department->appends($req)->firstItem() }} to {{ $department->appends($req)->lastItem() }} of {{ $department  ->appends($req)->total() }} entries</span>
                    <div>{{ $department->appends($req)->render() }}</div>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.departmentModal')
@endsection
@push('script')
    <script>
        $('#add-new-btn').click(function(){
            $('.add-btn').text('Add')
            $('#departmentModalLabel').text('Create Department')
            var url = "{{route('department.store')}}"
            $('#department_form').attr('action', url)
            $('#department_form').find("input[type=text],textarea").val("")
            $('#departmentModal').modal('show')
        })
        $('.edit-btn').click(function(){
            $('.add-btn').text('Update');
            $('#departmentModalLabel').text('Update Department')
            var department_data = $(this).data('department')
            var url = window.location.pathname+'/'+department_data.id+'/update'
            $('#department_form').attr('action', url)
            $('#name').val(department_data.name)
            $('#short_name').val(department_data.short_name)
            $('#departmentModal').modal('show')
        })
    </script>
@endpush
