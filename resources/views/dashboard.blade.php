@extends('layout.master')
@section('style')
    <link href="{{asset('/')}}assets/fullcalender/packages/core/main.css" rel='stylesheet' />
    <link href="{{asset('/')}}assets/fullcalender/packages/daygrid/main.css" rel='stylesheet' />
@endsection
@push('css')
    <style>
        .fc-day-grid-event .fc-time {
            font-size: 18px !important;
        }
    </style>
@endpush

@section('content')
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="row">
                @include('breadcrumb', ['title' => 'Dashboard'])
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-md-4 mb-5">
                            <a href="{{route('room.index')}}"
                               class="metric metric-bordered align-items-center">
                                @include('common.customCard',['title' => 'Rooms', 'icon' => 'ti-home' , 'data' => $room])
                            </a>
                        </div>
                        <div class="col-md-4 mb-5">
                            <a href="{{route('sim.report', 1)}}"
                               class="metric metric-bordered align-items-center">
                                @include('common.customCard',['title' => 'Assigned SIM', 'icon' => 'ti-check-box' , 'data' => $total_sim->assigned ?? 0])
                            </a>
                        </div>
                        <div class="col-md-4 mb-5">
                            <a href="{{route('sim.report', 0)}}"
                               class="metric metric-bordered align-items-center">
                                @include('common.customCard',['title' => 'Un Assigned SIM', 'icon' => 'ti-na' , 'data' => $total_sim->un_assigned ?? 0])
                            </a>
                        </div>
                        <div class="col-md-4 mb-5">
                            <a href="{{route('booking-room.index')}}"
                               class="metric metric-bordered align-items-center">
                                @include('common.customCard',['title' => 'Last Week Meeting', 'icon' => 'ti-calendar' , 'data' => $past_total_meeting])
                            </a>
                        </div>
                        <div class="col-md-4 mb-5">
                            <a href="{{route('booking-room.index')}}"
                               class="metric metric-bordered align-items-center">
                                @include('common.customCard',['title' => 'Upcoming Meeting', 'icon' => 'ti-calendar' , 'data' => $coming_total_meeting])
                            </a>
                        </div>
                        <div class="col-md-4 mb-5">
                            {{--                            <a href="{{route('sim.request.list')}}"--}}
                            <a href="#"
                               class="metric metric-bordered align-items-center">
                                @include('common.customCard',['title' => 'Total Complain', 'icon' => 'ti-agenda' , 'data' => $total_complain_request])
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card_box box_shadow position-relative mb_30">
                        <!-- .card-body -->
                        <div class="white_box_tittle_custom bg-silken">
                            <h5 class="text-white">Meeting Leaderboard</h5>
                        </div>
                        <!-- .list-group -->
                        <div class="list-group list-group-flush">
                            <!-- .list-group-item -->
                            @foreach($user_meeting as $user)
                                <div class="list-group-item">
                                    <table>
                                        <tr class="text-center">
                                            <td>
                                                <div class="list-group-item-figure">
                                                    <a href="javascript:void(0)"
                                                       data-toggle="tooltip"
                                                       title=" {{$user->name}}">
                                                        <img src="{{asset('/uploads').'/'.$user->image}}" alt="" class="user-avatar">
                                                    </a>
                                                </div>
                                            </td>
                                            <td>
                                                {{$user->name}}
                                            </td>
                                            <td> :</td>
                                            <td>
                                                <strong>{{$user->total}}</strong>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- .list-group-item-figure -->
                                </div><!-- /.list-group-item -->
                            @endforeach
                            <!-- .list-group-item -->
                        </div><!-- /.list-group -->
                    </div><!-- /.card -->
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-md-8">
                    <div class="c_card-card bg-silken">
                        <div class="c_card-title">Last 30 Days Meeting Man Hours</div>
                        <div class="d-flex justify-content-center align-items-center">
                            <canvas id='meeting-man-hour' height="132" style="background-color: #d0d0d0 !important; border-radius: 5px !important;"></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="c_card-card bg-silken">
                        <div class="c_card-title">Complain Status Ratio</div>
                        <div class="d-flex justify-content-center align-items-center">
                            <canvas id='complain-status-ratio' height="135" style="background-color: #d0d0d0 !important; border-radius: 5px !important;"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src='{{asset('/')}}assets/fullcalender/packages/core/main.js'></script>
    <script src='{{asset('/')}}assets/fullcalender/packages/interaction/main.js'></script>
    <script src='{{asset('/')}}assets/fullcalender/packages/daygrid/main.js'></script>
@endsection
@push('script')
    {{--    <script src='{{asset('/')}}js/chart.min.js'></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: [ 'interaction', 'dayGrid' ],
                header: {
                    left: 'prevYear,prev,next,nextYear today',
                    center: 'title',
                    right: 'dayGridMonth,dayGridWeek,dayGridDay'
                },
                defaultDate: "{{date('Y-m-d')}}",
                navLinks: false, // can click day/week names to navigate views
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                events: @php echo $total_meeting; @endphp,
                eventTimeFormat: {
                    hour: 'numeric',
                    minute: '2-digit',
                    meridiem: true
                }
            });

            calendar.render();


            var lineChart = document.getElementById('meeting-man-hour').getContext('2d');

            new Chart(lineChart, {
                // The type of chart we want to create
                type: 'line',

                // The data for our dataset
                data: {
                    labels: [
                        @foreach ($meeting_man_hour as $manhour)
                            "{{$manhour->day}}",
                        @endforeach
                    ],
                    datasets: [{
                        label: "Man Hours",
                        backgroundColor: 'rgb(255, 99, 132)',
                        borderColor: 'rgb(255, 99, 132)',
                        data: [
                            @foreach ($meeting_man_hour as $manhour)
                                "{{$manhour->manhour}}",
                            @endforeach
                        ],
                    }]
                },

                // Configuration options go here
                options: {
                    responsive: true,
                    plugins: {
                        tooltip: {
                            callbacks: {
                                // Format tooltip value
                                label: (context) => {
                                    const label = context.dataset.label || '';
                                    const value = context.parsed.y || 0;
                                    return `${label}: ${value} h`; // Add "h" suffix
                                }
                            }
                        }
                    },
                    scales: {
                        y: {
                            beginAtZero: true,
                            ticks: {
                                // Optional: Add suffix to axis labels too
                                callback: (value) => `${value}h`
                            }
                        }
                    }
                }
            });


            //==================== PIE CHART ============================
            // Get the canvas context
            const pieChart = document.getElementById('complain-status-ratio').getContext('2d');
            let hoveredData = null;

            // Create gradients
            const createGradient = (color1, color2) => {
                const gradient = pieChart.createLinearGradient(0, 0, 400, 0);
                gradient.addColorStop(0, color1);
                gradient.addColorStop(1, color2);
                return gradient;
            };

            const gradients = [
                createGradient('#42d504', '#FFE66D'),
                createGradient('#4ECDC4', '#45B7D1'),
                createGradient('#f53f3f', '#FFEEAD')
            ];

            // Create the pie chart
            new Chart(pieChart, {
                type: 'pie',
                data: {
                    labels: [ @foreach ($complain_status_ratio->getAttributes() as $key => $data)
                        "{{ucfirst(str_replace('_', ' ', $key))}}",
                        @endforeach],
                    datasets: [{
                        label: 'Complain Count',
                        data: [@foreach ($complain_status_ratio->getAttributes() as $key => $data)
                            "{{$data}}",
                            @endforeach],
                        backgroundColor: gradients,
                        borderColor: '#fff', // Optional: Adds a white border around slices
                        borderWidth: 1
                    }]
                },
                options: {
                    cutout: '65%',
                    responsive: true,
                    plugins: {
                        legend: {
                            display: true,
                            position: 'bottom'
                        }
                    }
                }
            });
        });
    </script>
@endpush
