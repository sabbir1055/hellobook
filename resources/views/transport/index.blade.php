@extends('layout.master')

@section('style')
    <link rel="stylesheet" href="{{asset('/')}}assets/select2/select2.min.css">
@endsection

@section('content')
    @permission('add_transport')
    @include('common.addButton')
    @endpermission
    @include('breadcrumb', ['title' => 'Transport List'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> Name </th>
                            <th> Number </th>
                            <th> Capacity </th>
                            <th> Daily Trip </th>
                            <th> Transport Type </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $transports->appends($req)->firstItem(); @endphp
                        @foreach($transports as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->car_name}} </td>
                                <td class="align-middle"> {{$value->car_number}} </td>
                                <td class="align-middle"> {{$value->capacity}} </td>
                                <td class="align-middle"> {{$value->trip_count}} </td>
                                <td class="align-middle"> {{ucfirst(str_replace('_',' ',$value->car_type))}} </td>
                                <td class="align-middle">
                                    @permission('update_transport')
                                    <a href="javascript:void(0)" data-car="{{$value}}" class="edit-btn">
                                        <button class="btn btn-sm btn-icon btn-secondary edit-btn" title="Edit">
                                            <i class="fa fa-pencil-alt"></i>
                                        </button>
                                    </a>
                                    @if(in_array($value->car_type, ['public_transport', 'office_transport']))
                                        <a href="{{route('transport.trip.list', $value->id)}}">
                                            <button class="btn btn-sm btn-icon btn-secondary" title="Edit Trip Users">
                                                <i class="fa fa-car-alt"></i>
                                            </button>
                                        </a>
                                    @endif
                                    @endpermission
                                    <a href="javascript:void(0)" class="edit-btn accordion-toggle" data-toggle="collapse" data-target="#demo{{$value->id}}">
                                        <button class="btn btn-sm btn-icon btn-secondary" title="See Allocated Employees">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            <tr >
                                <td colspan="8" class="hiddenRow">
                                    <div class="accordian-body collapse" id="demo{{$value->id}}">
                                        <table class="table table-bordered">
                                            <tr class="text-center">
                                                <td>#SL</td>
                                                <td>Trip No</td>
                                                <td>Allocated Employees</td>
                                            </tr>
                                            @foreach($value->getTripList as $key => $trip)
                                                <tr class="text-center">
                                                    <td class="align-middle"> {{++$key}} </td>
                                                    <td class="align-middle"> {{$trip->trip_no}} </td>
                                                    <td class="align-middle">
                                                        @forelse($trip->getUserInfo as $user)
                                                            <span class="badge badge-success">{{$user->name}}</span><br>
                                                        @empty
                                                            NO user allocate
                                                        @endforelse
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example" class="m-3">
                    <span>Showing {{ $transports->appends($req)->firstItem() }} to {{ $transports->appends($req)->lastItem() }} of {{ $transports  ->appends($req)->total() }} entries</span>
                    <div>{{ $transports->appends($req)->render() }}</div>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.transportModal')
@endsection
@section('script')
    <script src="{{asset('/')}}assets/select2/select2.min.js"></script>
@endsection
@push('script')

    <script>
        $('#car_type').change(function () {
            if($(this).val() === 'others_transport'){
                $('#trip_count').val(null)
                $('#trip_count').parent().addClass('d-none')
            } else {
                $('#trip_count').parent().removeClass('d-none')
            }
        })
        $('#add-new-btn').click(function(){
            $('.add-btn').text('Add')
            $('#permissionModalLabel').text('Add Transport')
            var url = "{{route('transport.store')}}"
            $('#transport_form').attr('action', url)
            $('#transport_form').find("input[type=text]").val("")
            $("option:selected").prop("selected", false)
            $('.select2').select2({
                dropdownParent: $('#transportModal'),
                width: '100%'
            });
            $('#transportModal').modal('show')
        })
        $('.edit-btn').click(function(){
            $('.add-btn').text('Update');
            $('#permissionModalLabel').text('Update Transport')
            var car_data = $(this).data('car')
            if(car_data){
                var url = window.location.pathname+'/'+car_data.id+'/update'
                $('#transport_form').attr('action', url)
                $('#name').val(car_data.car_name)
                $('#number').val(car_data.car_number)
                $('#capacity').val(car_data.capacity)
                $('#trip_count').val(car_data.trip_count)
                $('#car_type').val(car_data.car_type)
                $('.select2').select2({
                    dropdownParent: $('#transportModal')
                });
                $('#transportModal').modal('show')
            }
        })
    </script>
@endpush
