@extends('layout.master')

@section('style')
    <link rel="stylesheet" href="{{asset('/')}}assets/select2/select2.min.css">
@endsection

@section('content')
    @permission('add_transport')
    @include('common.addButton')
    @endpermission
    @include('breadcrumb',['title' => 'Trip List', 'sub_title' => $transport->car_number])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid">
                <div class="table-responsive table-bordered">
                    <table class="table">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> Name </th>
                            <th> Number </th>
                            <th> Capacity </th>
                            <th> Trip No</th>
                            <th> Allocate Employees </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transport->getTripList as $key => $value)
                            <tr>
                                <td class="align-middle"> {{++$key}} </td>
                                <td class="align-middle"> {{$transport->car_name}} </td>
                                <td class="align-middle"> {{$transport->car_number}} </td>
                                <td class="align-middle"> {{$transport->capacity}} </td>
                                <td class="align-middle"> {{$value->trip_no}} </td>
                                <td class="align-middle">
                                    @forelse($value->getUserInfo as $user)
                                        <span class="badge badge-success">{{$user->name}}</span><br>
                                    @empty
                                        NO user allocate
                                    @endforelse
                                </td>
                                <td class="align-middle">
                                    @permission('update_transport')
                                    <a href="javascript:void(0)" data-car="{{$value}}" class="edit-btn">
                                        <button class="btn btn-sm btn-icon btn-secondary edit-btn"> <i class="fa fa-pencil-alt"></i></button>
                                        <span class="sr-only">Edit</span>
                                    </a>
                                    @endpermission
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.transportTripModal')
@endsection
@section('script')
    <script src="{{asset('/')}}assets/select2/select2.min.js"></script>
@endsection
@push('script')
    <script>
        $(document).ready(() => {
            $('#transportTripModal').on('hidden.bs.modal', function () {
                $('.select2').each(function() {
                    if ($(this).data('select2')) {
                        $(this).select2('destroy');
                    }
                });
            });
        })

        $('.edit-btn').click(function(){
            var car_data = $(this).data('car')
            $('#transportTripLabel').text('Edit Trip')
            if(car_data){
                var url = window.location.pathname+'/'+car_data.id+'/update'
                $('#trip_form').attr('action', url)
                const userList = car_data.get_user_info.map(user => user.id);
                if(userList.length > 0){
                    $('#allocate_users').val(userList)
                }
                $('.select2').select2({
                    dropdownParent: $('#transportTripModal'),
                    width: '100%'
                })
                $('#transportTripModal').modal('show')
            }
        })
    </script>
@endpush
