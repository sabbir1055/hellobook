{{--@if ($paginator->hasPages())--}}
{{--    <ul class="pagination" role="navigation">--}}
{{--         Previous Page Link--}}
{{--        @if ($paginator->onFirstPage())--}}
{{--            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">--}}
{{--                <span class="page-link" aria-hidden="true">&lsaquo;</span>--}}
{{--            </li>--}}
{{--        @else--}}
{{--            <li class="page-item">--}}
{{--                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>--}}
{{--            </li>--}}
{{--        @endif--}}
{{--         Pagination Elements--}}
{{--        @foreach ($elements as $element)--}}
{{--             "Three Dots" Separator--}}
{{--            @if (is_string($element))--}}
{{--                <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>--}}
{{--            @endif--}}

{{--             Array Of Links--}}
{{--            @if (is_array($element))--}}
{{--                @foreach ($element as $page => $url)--}}
{{--                    @if ($page == $paginator->currentPage())--}}
{{--                        <li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>--}}
{{--                    @else--}}
{{--                        <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            @endif--}}
{{--        @endforeach--}}

{{--         Next Page Link--}}
{{--        @if ($paginator->hasMorePages())--}}
{{--            <li class="page-item">--}}
{{--                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>--}}
{{--            </li>--}}
{{--        @else--}}
{{--            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">--}}
{{--                <span class="page-link" aria-hidden="true">&rsaquo;</span>--}}
{{--            </li>--}}
{{--        @endif--}}
{{--    </ul>--}}
{{--@endif--}}

{{--CUSTOM V1--}}


<nav>
    <ul class="pagination justify-content-start"> <!-- Align left -->
        @if ($paginator->onFirstPage())
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span class="page-link" aria-hidden="true">&lsaquo;</span>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
            </li>
        @endif
        <li class="page-item {{ $paginator->currentPage() == 1 ? 'active' : '' }}">
            <a class="page-link" href="{{ $paginator->url(1) }}">1</a>
        </li>
        @if ($paginator->currentPage() > 3)
            <li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>
        @endif
        @php
            $start = max(2, $paginator->currentPage() - 1);
            $end = min($paginator->lastPage() - 1, $paginator->currentPage() + 1);
        @endphp
        @for ($page = $start; $page <= $end; $page++)
            <li class="page-item {{ $paginator->currentPage() == $page ? 'active' : '' }}">
                <a class="page-link" href="{{ $paginator->url($page) }}">{{ $page }}</a>
            </li>
        @endfor
        @if ($paginator->currentPage() < $paginator->lastPage() - 2)
            <li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>
        @endif
        @if ($paginator->lastPage() > 1)
            <li class="page-item {{ $paginator->currentPage() == $paginator->lastPage() ? 'active' : '' }}">
                <a class="page-link" href="{{ $paginator->url($paginator->lastPage()) }}">{{ $paginator->lastPage() }}</a>
            </li>
        @endif
        @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
            </li>
        @else
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span class="page-link" aria-hidden="true">&rsaquo;</span>
            </li>
        @endif
    </ul>
</nav>

