@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{asset('/')}}assets/select2/select2.min.css">
@endsection
@section('content')
    @permission('designation_manage')
    @include('common.addButton')
    @endpermission
    @include('breadcrumb', ['title' => 'Designation List'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> Name </th>
                            <th> Short Name </th>
                            <th> Department </th>
                            <th> Status </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $designation->appends($req)->firstItem(); @endphp
                        @foreach($designation as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->name}} </td>
                                <td class="align-middle"> {{$value->short_name}} </td>
                                <td class="align-middle"> {{$value->getDepartment->name}} </td>
                                <td class="align-middle">
                                    @if($value->status)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">Inactive</span>
                                    @endif
                                </td>
                                <td class="align-middle">
                                    <a href="javascript:void(0)" data-designation="{{$value}}" class="edit-btn">
                                        <button class="btn btn-sm btn-icon btn-secondary "><i class="fa fa-pencil-alt"></i></button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example" class="m-3">
                    <span>Showing {{ $designation->appends($req)->firstItem() }} to {{ $designation->appends($req)->lastItem() }} of {{ $designation  ->appends($req)->total() }} entries</span>
                    <div>{{ $designation->appends($req)->render() }}</div>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.designationModal')
@endsection
@push('script')
    <script src="{{asset('/')}}assets/select2/select2.min.js"></script>
    <script>
        $(document).ready(() => {
            // Destroy Select2 instance when the modal is hidden
            $('#designationModal').on('hidden.bs.modal', function () {
                $('.select2').each(function() {
                    if ($(this).data('select2')) {
                        $(this).select2('destroy');
                    }
                });
            });
        })


        $('#add-new-btn').click(function(){
            $('.add-btn').text('Add')
            $('#designationModalLabel').text('Create Designation')
            var url = "{{route('designation.store')}}"
            $('#designation_form').attr('action', url)
            $('#designation_form').find("input[type=text],textarea").val("")
            $("option:selected").prop("selected", false)
            $('.select2').select2({
                dropdownParent: $('#designationModal'),
                width: '100%'
            })
            $('#designationModal').modal('show')
        })
        $('.edit-btn').click(function(){
            $('.add-btn').text('Update');
            $('#designationModalLabel').text('Update Designation')
            var designation_data = $(this).data('designation')
            var url = window.location.pathname+'/'+designation_data.id+'/update'
            $('#designation_form').attr('action', url)
            $('#name').val(designation_data.name)
            $('#short_name').val(designation_data.short_name)
            $('#department').val(designation_data.department_id)
            $('#status').val(designation_data.status)
            $('.select2').select2({
                dropdownParent: $('#designationModal'),
                width: '100%'
            })
            $('#designationModal').modal('show')
        })
    </script>
@endpush
