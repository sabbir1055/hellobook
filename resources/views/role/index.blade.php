@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{asset('/')}}assets/select2/select2.min.css">
@endsection
@section('content')
    @permission('manage_role')
    @include('common.addButton')
    @endpermission
    @include('breadcrumb', ['title' => 'Role List'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> Name </th>
                            <th> Display Name </th>
                            <th> Permissions </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $role->appends($req)->firstItem(); @endphp
                        @foreach($role as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->name}} </td>
                                <td class="align-middle"> {{$value->display_name}} </td>
                                <td>
                                    @foreach($value->permissions as $permissionData)
                                        @php $color = rand(1000,9999)  @endphp
                                        <span class="badge" style="background: #{{$color}}">{{$permissionData->display_name}}</span>
                                    @endforeach
                                </td>
                                <td class="align-middle">
                                    <a href="javascript:void(0)" data-role="{{$value}}" data-permission ="{{$value->permissions->pluck('id')}}" class="edit-btn">
                                        <button class="btn btn-sm btn-icon btn-secondary "><i class="fa fa-pencil-alt"></i></button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example" class="m-3">
                    <span>Showing {{ $role->appends($req)->firstItem() }} to {{ $role->appends($req)->lastItem() }} of {{ $role  ->appends($req)->total() }} entries</span>
                    <div>{{ $role->appends($req)->render() }}</div>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.roleModal')
@endsection
@push('script')
    <script src="{{asset('/')}}assets/select2/select2.min.js"></script>
    <script>
        $('#add-new-btn').click(function(){
            $('.add-btn').text('Add')
            $('#roleModalLabel').text('Create Role')
            var url = "{{route('role.store')}}"
            $('#role_form').attr('action', url)
            $('#role_form').find("input[type=text],textarea").val("")
            $('#permission').val('')
            $('.select2').select2({
                dropdownParent: $('#roleModal'),
                width: '100%'
            })
            $('#roleModal').modal('show')
        })
        $('.edit-btn').click(function(){
            $('.add-btn').text('Update');
            $('#roleModalLabel').text('Update Role')
            var role_data = $(this).data('role')
            var permission_data = $(this).data('permission')
            var url = window.location.pathname+'/'+role_data.id+'/update'
            $('#role_form').attr('action', url)
            $('#name').val(role_data.name)
            $('#display_name').val(role_data.display_name)
            $('#description').val(role_data.description)
            $('#permission').val(permission_data)
            $('.select2').select2({
                dropdownParent: $('#roleModal'),
                width: '100%'
            })
            $('#roleModal').modal('show')
        })
    </script>
@endpush
