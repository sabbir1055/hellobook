<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.6.95/css/materialdesignicons.css">
    <link rel="stylesheet" href="{{asset('/')}}/css/userinfo.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<div class="container" style="padding: 0px 30px 0px 30px; margin-top: 10px;">
        <div class="row d-flex justify-content-center">
            <div class="col-xl-6 col-md-12">
                <div class="card user-card-full">
                    <div class="row m-l-0 m-r-0">
                        <div class="col-sm-4 bg-c-lite-green user-profile">
                            <div class="card-block text-center text-white">
                                <div class="m-b-25">
                                    {{-- <img src="http://hellobook.silkensewing.com/uploads/user.png" class="img-thumbnail" width="120" alt="User-Profile-Image"> --}}
                                    <img src="{{asset('/uploads').'/'.$user->image}}" class="img-thumbnail" width="120" alt="User-Profile-Image">
                                </div>
                                <h6 class="f-w-600">{{$user->name}}</h6>
                                <p>
                                    {{$user->getDesignation->name}}
                                    @if(!in_array($user->employee_id, explode(',',env('NOT_SHOWING_DEPT'))))
                                        <small>({{$user->getDepartment->name}})</small>
                                    @endif
                                </p>
                                <ul class="social-link list-unstyled m-t-40 m-b-10">
                                    <li><a href="tel:{{$user->msisdn}}" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="phone" data-abc="true"><i class="mdi mdi-phone feather icon-phone phone" aria-hidden="true"></i></a></li>
                                    <li><a href="mailto:{{$user->email}}" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="email" data-abc="true"><i class="mdi mdi-email feather icon-email email" aria-hidden="true"></i></a></li>
                                    <li><a href="{{$user->linkedin_profile}}" target="_blank" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="linkedin" data-abc="true"><i class="mdi mdi-linkedin feather icon-linkedin linkedin" aria-hidden="true"></i></a></li>
                                    <li><a href="https://wa.me/{{$user->msisdn}}" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="whatsapp" data-abc="true"><i class="mdi mdi-whatsapp feather icon-whatsapp whatsapp" aria-hidden="true"></i></a></li>
                                    {{-- <li><a href="http://www.silkensewing.com" target="_blank" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="url" data-abc="true"><i class="mdi mdi-web feather icon-web web" aria-hidden="true"></i></a></li> --}}
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="card-block">
                                <div class="row profile-info">
                                    <div class="col-sm-6">
                                        <p class="text-muted mb-2 f-w-450">
                                            <i class="mdi mdi-phone feather icon-phone phone" aria-hidden="true"></i>
                                            <span class="item-title">Phone</span>
                                        </p>
                                        <h5 class="f-w-600">
                                            <a href="tel:{{$user->msisdn}}">{{$user->msisdn}}</a>
                                        </h5>
                                        <hr>
                                    </div>
                                    <div class="text-muted col-sm-6">
                                        <p class="mb-2 f-w-450">
                                            <i class="mdi mdi-email feather icon-email email" aria-hidden="true"></i>
                                            <span class="item-title">Email</span>
                                        </p>
                                        <h5 class="f-w-600">
                                            <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                                        </h5>
                                        <hr>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="text-muted mb-2 f-w-450">
                                            <i class="mdi mdi-web feather icon-web web" aria-hidden="true"></i>
                                            <span class="item-title">Website</span>
                                        </p>
                                        <h5 class="text-muted f-w-600">
                                            <a href="http://www.silkensewing.com">http://www.silkensewing.com</a>
                                        </h5>
                                        <hr>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="text-muted mb-2 f-w-450">
                                            <i class="mdi mdi-office-building feather icon-office-building office-building" aria-hidden="true"></i>
                                            <span class="item-title">Corporate Office</span>
                                        </p>
                                        <h5 class="text-muted f-w-600">
                                            <a href="https://www.google.com/maps/place/SILKEN+SEWING+LTD.,+House+%23+02+Rd+No+20A,+Dhaka+1230/data=!4m2!3m1!1s0x3755c517ad392669:0x261cc8bea7c40e47?utm_source=mstt_1&entry=gps&lucs=47068615&g_ep=CAESCTExLjgyLjMwMRgAIOW7AyoINDcwNjg2MTVCAkJE">
                                                House- 02, Road- 20/A, Sector- 03, Uttara, Dhaka, Bangladesh.
                                            </a>
                                        </h5>
                                        <hr>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="text-muted mb-2 f-w-450">
                                            <i class="mdi mdi-factory feather icon-factory factory" aria-hidden="true"></i>
                                            <span class="item-title">Factory</span>
                                        </p>
                                        <h5 class="f-w-600">
                                            <a href="https://www.google.com/maps/place/Baniarchala,+member+bari,+gazipur/@24.1580198,90.4101578,17.54z/data=!4m6!3m5!1s0x3756777456953129:0xfd5004a26d3b4eec!8m2!3d24.1584575!4d90.4119551!16s%2Fg%2F11kj7ydt1g?entry=ttu">
                                                Baniarchala (Memberbari), Vobanipur, Gazipur    
                                            </a> 
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <img src="{{asset('/cerificate-image-qr.png')}}" class="img img-fluid mb-2" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</body>
</html>