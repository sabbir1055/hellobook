@extends('layout.master')
@section('content')
    @include('breadcrumb', ['title' => 'User Bulk Upload'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid">
                <div class="card-body">
                    <i class="fa fa-info-circle mb-2"> </i> Click
                        <a href="{{asset('/uploads/sample.xlsx')}}"> here
                    </a>
                    to download sample
                    {!! Form::open(['route' => 'user.bulk.store','method'=>'post','enctype'=>'multipart/form-data']) !!}
                    {!! Form::token() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('file','User File')!!}
                                {!! Form::file('file', ['class'=>'form-control mb-3','id'=>'file']);!!}
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary add-btn">ADD</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
