@extends('layout.master')
@section('content')
    @permission('add_user')
    @include('common.addButton')
    @endpermission
    @include('breadcrumb',['title' => 'User List'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card card-fluid mb-3">
                <div class="card-body">
                    <form action="">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('f_name','Name')!!}
                                    {!! Form::text('f_name', @$req['f_name'], ['class'=>'form-control mb-3','id'=>'f_name']);!!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('f_msisdn','Mobile Number')!!}
                                    {!! Form::text('f_msisdn', @$req['f_msisdn'], ['class'=>'form-control mb-3','id'=>'f_msisdn']);!!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('f_department','Select Department')!!}
                                    {!! Form::select('f_department', [''=>'Select a department']+$department, @$req['f_department'], ['class'=>'form-control mb-3 select2','id'=>'f_department']);!!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('f_designation','Select Designation')!!}
                                    {!! Form::select('f_designation', [''=>'Select a designation']+[], @$req['f_designation'], ['class'=>'form-control mb-3 select2','id'=>'f_designation']);!!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('f_is_complain_solver','Is Complain Solver')!!}
                                    {!! Form::select('f_is_complain_solver', [''=>'Select a option', 1 => 'Yes', 0 => 'No'], @$req['f_is_complain_solver'], ['class'=>'form-control mb-3 select2','id'=>'f_is_complain_solver']);!!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('f_status','User Status')!!}
                                    {!! Form::select('f_status', [''=>'Select a option', 1 => 'Active', 0 => 'InActive'], @$req['f_status'], ['class'=>'form-control mb-3 select2','id'=>'f_status']);!!}
                                </div>
                            </div>
                            <div class="col-md-5"></div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">&nbsp</label>
                                    <button type="submit" class="btn btn-primary btn-block">Filter</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card card-fluid">
                <div class="table-responsive table-bordered">
                    <table class="table">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> Name </th>
                            @if(in_array(auth()->user()->roles()->first()->name,['super_admin','admin']))
                                <th> User Name </th>
                            @endif
                            <th> Employee ID </th>
                            <th> Email </th>
                            @if(in_array(auth()->user()->roles()->first()->name,['super_admin','admin']))
                                <th> Role </th>
                            @endif
                            <th> Department </th>
                            <th> Designation </th>
                            <th> Mobile No </th>
                            <th> Reporting To </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $users->appends($req)->firstItem(); @endphp
                        @foreach($users as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->name}} </td>
                                @if(in_array(auth()->user()->roles()->first()->name,['super_admin','admin']))
                                    <td class="align-middle"> {{$value->username}} </td>
                                @endif
                                <td class="align-middle"> {{$value->employee_id}} </td>
                                <td class="align-middle"> {{$value->email}} </td>
                                @if(in_array(auth()->user()->roles()->first()->name,['super_admin','admin']))
                                    <td class="align-middle"> {{$value->roles->first()->display_name}} </td>
                                @endif
                                <td class="align-middle"> {{$value->getDepartment->name}} </td>
                                <td class="align-middle"> {{$value->getDesignation->name}} </td>
                                <td class="align-middle"> {{$value->msisdn}} </td>
                                <td class="align-middle"> {{$value->reportingTo ? $value->reportingTo->name. " (".$value->reportingTo->employee_id.")" : null}} </td>
                                <td class="align-middle">
                                    <div class="d-flex">
                                        @permission('add_user')
                                        <a href="javascript:void(0)" data-user="{{$value}}" data-role="{{$value->roles->first()->id}}" class="edit-btn">
                                            <button class="btn btn-sm btn-icon btn-secondary" title="edit user"><i class="fa fa-pencil-alt"></i></button>
                                        </a>
                                        <a href="{{route('user.complainSolver.status.toggle', ['user' => $value])}}">
                                            <button class="btn btn-sm btn-icon btn-secondary" title="update complain solver status">
                                                @if($value->is_complain_solver)
                                                    <i class="fa fa-ban"></i>
                                                @else
                                                    <i class="fa fa-check"></i>
                                                @endif
                                            </button>
                                        </a>
                                        @endpermission
                                        @permission('generate_qr')
                                        <a href="{{route('user.generate.qr', $value)}}">
                                            <button class="btn btn-sm btn-icon btn-secondary" title="generate qr code"><i class="fa fa-qrcode"></i></button>
                                        </a>
                                        @endpermission
                                        <a href="javascript:void(0)" data-qr="{{$value->qr_image}}" class="qr-view-btn">
                                            <button class="btn btn-sm btn-icon btn-secondary" title="view qr code"><i class="fa fa-eye"></i></button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example" class="m-3">
                    <span>Showing {{ $users->appends($req)->firstItem() }} to {{ $users->appends($req)->lastItem() }} of {{ $users->appends($req)->total() }} entries</span>
                    <div>{{ $users->appends($req)->render() }}</div>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.userModal')
    @include('modals.userQrModal')
@endsection
@push('script')
    <script>
        $('#add-new-btn').click(function() {
            $('.add-btn').text('Add')
            $('#userModalLabel').text('Create User')
            var url = "{{route('user.store')}}"
            $('#user_form').attr('action', url)
            $('#user_form').find("input[type=text]").val("")
            $("option:selected").prop("selected", false)
            $('#userModal').modal('show')
            $('#role').val('')
            $('#designation').val('')
            // $('.select2').select2()
        })
        $('.edit-btn').click(function() {
            $('.add-btn').text('Update');
            $('#userModalLabel').text('Update User')
            var user_data = $(this).data('user')
            var role = $(this).data('role')
            var url = window.location.pathname + '/' + user_data.id + '/update'
            $('#user_form').attr('action', url)
            $('#name').val(user_data.name)
            $('#username').val(user_data.username)
            $('#employee_id').val(user_data.employee_id)
            $('#email').val(user_data.email)
            $('#status').val(user_data.status)
            $('#role').val(role)
            $('#msisdn').val(user_data.msisdn)
            $('#reporting_person_employee_id').val(user_data.reporting_person_employee_id)
            $('#department').val(user_data.department_id)
            $('#department').trigger('change')
            setTimeout(() => {
                $('#designation').val(user_data.designation)
                // $('.select2').select2()
            }, 1000)
            // $('.select2').select2()
            $('#userModal').modal('show')
        })
    </script>
    <script>
        $(document).ready(() => {
            $('#department').change(() => {
                $.ajax({
                    url: "{{route('department.designation')}}",
                    method: 'POST',
                    'data': {
                        '_token': "{{csrf_token() }}",
                        'department': $("#department").val()
                    },
                    success: function(data) {
                        var dom = `<option value="">Select a designation </option>`;
                        $.each(data, function(key, value) {
                            dom += `<option value="${value.id}">${value.name}</option>`
                        })
                        $('#designation').html(dom)
                    },
                    err: function(err) {
                        alert('No data found !!!!')
                    }
                });
            })
            $('#f_department').change(() => {
                $.ajax({
                    url: "{{route('department.designation')}}",
                    method: 'POST',
                    'data': {
                        '_token': "{{csrf_token() }}",
                        'department': $("#f_department").val()
                    },
                    success: function(data) {
                        var dom = `<option value="">Select a designation </option>`;
                        $.each(data, function(key, value) {
                            dom += `<option value="${value.id}">${value.name}</option>`
                        })
                        $('#f_designation').html(dom)
                    },
                    err: function(err) {
                        alert('No data found !!!!')
                    }
                });
            })
        })
    </script>
    <script>
        $('.qr-view-btn').click(function(){
            var qrPath = $(this).data('qr')
            document.getElementById("qr-image-container").src = "{{asset('/uploads/')}}"+`/${qrPath}`;
            $('#qrModal').modal('show')
        })
    </script>
@endpush
