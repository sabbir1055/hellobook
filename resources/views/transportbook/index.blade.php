@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{asset('/')}}assets/select2/select2.min.css">
    <link rel="stylesheet" href="{{asset('/assets/Material-Time-Picker-Plugin-jQuery-MDTimePicker/mdtimepicker.css')}}">
@endsection
@section('content')
    @permission('add_transport_request')
    @include('common.addButton')
    @endpermission
    @include('breadcrumb', ['title' => 'Transport Booking'])
    <div class="page-section">
        <div class="card card-fluid">
            <div class="table table-striped table-bordered table-responsive">
                <table class="table text-center table-bordered">
                    <thead>
                    <tr class="bg-silken text-white">
                        <th> SL </th>
                        <th> Title </th>
                        <th> Booked By </th>
                        <th> Car Detail </th>
                        <th> Date </th>
                        <th> Time From </th>
                        <th> Time To </th>
                        <th> Applied Date </th>
                        <th> Status </th>
                        <th> Action </th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $sl = $bookTransport->appends($req)->firstItem(); @endphp
                    @foreach($bookTransport as $value)
                        <tr>
                            <td class="align-middle"> {{$sl++}} </td>
                            <td class="align-middle"> {{$value->title}} </td>
                            <td class="align-middle"> {{$value->bookedBy->name}} </td>
                            <td class="align-middle"> {{$value->transportDetail->car_name}} ( {{$value->transportDetail->car_number}} ) </td>
                            <td class="align-middle">{{date('d-M,y',strtotime($value->booked_date))}}</td>
                            <td class="align-middle">{{$value->booked_time_from ? date('h:i A',strtotime($value->booked_time_from)) : null }}</td>
                            <td class="align-middle">{{$value->booked_time_to ? date('h:i A',strtotime($value->booked_time_to)) : null }}</td>
                            <td class="align-middle">{{date('d M, Y h:i A', strtotime($value->created_at))}}</td>
                            <td class="align-middle">
                                @if($value->status === 0 && (date('Y-m-d H:i') < date('Y-m-d H:i',strtotime(strtotime($value->booked_date.' '.($value->booked_time_from ?: '00:00'))))))
                                    <span class="badge badge-pill badge-primary">Pending</span>
                                @elseif($value->status === 1)
                                    <span class="badge badge-pill badge-warning">Approved</span>
                                @elseif($value->status === 2)
                                    <span class="badge badge-pill badge-secondary">Declined</span>
                                @elseif($value->status === 3)
                                    <span class="badge badge-pill badge-dark">Withdraw</span>
                                @else
                                    <span class="badge badge-pill badge-danger">Trip time expired</span>
                                @endif
                            </td>
                            <td class="align-middle">
                                @if($value->status === 0 &&
                                    $value->booked_by === auth()->user()->id &&
                                    auth()->user()->can('update_transport_request') &&
                                    (date('Y-m-d H:i') < date('Y-m-d H:i',strtotime(strtotime(($value->booked_date.' '.($value->booked_time_from ?: '00:00'))))))
                                )
                                    <a href="{{route('transport.request.status.delete', $value->id)}}">
                                        <button class="btn btn-danger btn-sm" title="Delete Trip">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <nav aria-label="Page navigation example" class="m-3">
                <span>Showing {{ $bookTransport->appends($req)->firstItem() }} to {{ $bookTransport->appends($req)->lastItem() }} of {{ $bookTransport  ->appends($req)->total() }} entries</span>
                <div>{{ $bookTransport->appends($req)->render() }}</div>
            </nav>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.transportBooking')
    @include('modals.transportBookingDecline')
@endsection
@section('script')
    <script src="{{asset('/')}}assets/select2/select2.min.js"></script>
    <script src="{{asset('/assets/Material-Time-Picker-Plugin-jQuery-MDTimePicker/mdtimepicker.js')}}"></script>
@endsection
@push('script')
    <script>
        $(document).ready(() => {
            $('#transportBookModal').on('shown.bs.modal', function () {
                $('.select2').select2({
                    dropdownParent: $('#transportBookModal'),
                    width: '100%'
                })
            });

            // Destroy Select2 instance when the modal is hidden
            $('#transportBookModal').on('hidden.bs.modal', function () {
                $('.select2').each(function() {
                    if ($(this).data('select2')) {
                        $(this).select2('destroy');
                    }
                });
            });
        })

        $('#visit_type').change(function(){
            if($(this).val() === 'office'){
                $('.time-to-select-div').addClass('d-none');
                $('.time-from-select-div').addClass('d-none');
                $('#time_from').prop('required', false);
                $('#time_true').prop('required', false);
                $('#trip_id').prop('required', true);
            }else{
                $('.time-to-select-div').removeClass('d-none');
                $('.time-from-select-div').removeClass('d-none');
                $('#time_from').prop('required', true);
                $('#time_true').prop('required', true);
                $('#trip_id').prop('required', false);
                $('.car-trip-choose-div').addClass('d-none');
            }
        })

        $('#transport_id').change(function(){
            if($(this).val() && $('#visit_type').val() === 'office'){
                getTripDetails()
            }
        })

        $('#add-new-btn').click(function(){
            $('.add-btn').text('Add')
            $('#transportBookModalLabel').text('Book Transport')
            var url = "{{route('transport.request.store')}}"
            $('#user_form').attr('action', url)
            $('#user_form').find("input[type=text]").val("")
            $('#time_to').mdtimepicker({
                minTime:"09:00 AM",
                maxTime:"09:00 PM"
            })
            $('#time_from').mdtimepicker({
                minTime:"09:00 AM",
                maxTime:"09:00 PM"
            })

            $("option:selected").prop("selected", false)

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date()
            });

            $('#transportBookModal').modal('show')
        })

        $('#reject_request').click(function(){
            var request_id = $(this).data('request')
            $('#booking_id').val(request_id)
            $('#transportBookingDecline').modal('show')
        })

        function getAvailableCar(){
            var visit_type = $('#visit_type').val()
            var time_from = $('#time_from').val()
            var time_to = $('#time_to').val()
            var date = $('#date').val()
            var callApi = false;
            if(visit_type === 'office' && date){
                callApi = true
            } else if(visit_type === 'outside_visit' && date && time_from && time_to){
                callApi = true
            } else {
                callApi = false
            }
            if(callApi) {
                $.ajax({
                    url: "{{route('available.transport')}}",
                    method: 'POST',
                    'data': {
                        '_token': "{{csrf_token()}}",
                        'time_from': time_from,
                        'time_to': time_to,
                        'date': date,
                        'visit_type': visit_type
                    },
                    success: function (data) {
                        if (data) {
                            $('#transport_id').html(data)
                        }
                        $('#transport_id').select2()
                    },
                    err: function (err) {
                        alert('No data found !!!!')
                    }
                });
            }
        }

        function getTripDetails(){
            console.log('Here')
            var visit_type = $('#visit_type').val()
            var date = $('#date').val()
            var transport_id = $('#transport_id').val()
            var callApi = false;
            if(visit_type === 'office' && date && transport_id){
                callApi = true
            } else {
                callApi = false
            }
            if(callApi) {
                $.ajax({
                    url: "{{route('available.trip')}}",
                    method: 'POST',
                    'data': {
                        '_token': "{{csrf_token()}}",
                        'date': date,
                        'visit_type': visit_type,
                        'transport_id': transport_id
                    },
                    success: function (data) {
                        if (data) {
                            $('.car-trip-choose-div').removeClass('d-none')
                            $('#trip_id').html(data)
                        }
                        $('#trip_id').select2()
                    },
                    err: function (err) {
                        alert('No data found !!!!')
                    }
                });
            }
        }
    </script>
@endpush
