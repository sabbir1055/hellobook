@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{asset('/')}}assets/select2/select2.min.css">
    <link rel="stylesheet" href="{{asset('/assets/Material-Time-Picker-Plugin-jQuery-MDTimePicker/mdtimepicker.css')}}">
@endsection
@section('content')
    @include('breadcrumb', ['title' => 'Transport Booking'])
    <div class="page-section">
        <div class="card card-fluid">
            <div class="table table-striped table-bordered table-responsive">
                <table class="table text-center table-bordered">
                    <thead>
                    <tr class="bg-silken text-white">
                        <th> SL </th>
                        <th> Title </th>
                        <th> Booked By </th>
                        <th> Car Detail </th>
                        <th> Date </th>
                        <th> Time From </th>
                        <th> Time To </th>
                        <th> Applied Date </th>
                        <th> Status </th>
                        <th> Action </th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $sl = $bookTransport->appends($req)->firstItem(); @endphp
                    @foreach($bookTransport as $value)
                        <tr>
                            <td class="align-middle"> {{$sl++}} </td>
                            <td class="align-middle"> {{$value->title}} </td>
                            <td class="align-middle"> {{$value->bookedBy->name}} </td>
                            <td class="align-middle"> {{$value->transportDetail->car_name}} ( {{$value->transportDetail->car_number}} ) </td>
                            <td>{{date('d-M,y',strtotime($value->booked_date))}}</td>
                            <td>{{$value->booked_time_from ? date('h:i A',strtotime($value->booked_time_from)) : null }}</td>
                            <td>{{$value->booked_time_to ? date('h:i A',strtotime($value->booked_time_to)) : null }}</td>
                            <td class="align-middle">{{date('d M, Y h:i A', strtotime($value->created_at))}}</td>
                            <td>
                                @if($value->status === 0 && (date('Y-m-d H:i') < date('Y-m-d H:i',strtotime(strtotime(($value->booked_date.' '.($value->booked_time_from ?: '00:00')))))))
                                    <span class="badge badge-pill badge-primary">Pending</span>
                                @elseif($value->status === 1)
                                    <span class="badge badge-pill badge-warning">Approved</span>
                                @elseif($value->status === 2)
                                    <span class="badge badge-pill badge-secondary">Declined</span>
                                @elseif($value->status === 3)
                                    <span class="badge badge-pill badge-dark">Withdraw</span>
                                @else
                                    <span class="badge badge-pill badge-danger">Trip time expired</span>
                                @endif
                            </td>
                            <td class="align-middle">
                                @if($value->status === 0 &&
                                    auth()->user()->can('approve_reject_transport_request') &&
                                    (date('Y-m-d H:i') < date('Y-m-d H:i',strtotime(strtotime(($value->booked_date.' '.($value->booked_time_from ?: '00:00'))))))
                                )
                                    <a href="{{route('transport.request.status.approve', $value->id)}}">
                                        <button class="btn btn-success btn-sm" title="Make Trip Finish">
                                            <i class="fas fa-check"></i>
                                        </button>
                                    </a>
                                    @if($value->booked_by !== auth()->user()->id)
                                        <a href="#">
                                            <button class="btn btn-danger btn-sm reject_request" title="Delete Trip" data-request="{{$value->id}}">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </a>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <nav aria-label="Page navigation example" class="m-3">
                <span>Showing {{ $bookTransport->appends($req)->firstItem() }} to {{ $bookTransport->appends($req)->lastItem() }} of {{ $bookTransport  ->appends($req)->total() }} entries</span>
                <div>{{ $bookTransport->appends($req)->render() }}</div>
            </nav>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.transportBooking')
    @include('modals.transportBookingDecline')
@endsection
@section('script')
    <script src="{{asset('/')}}assets/select2/select2.min.js"></script>
    <script src="{{asset('/assets/Material-Time-Picker-Plugin-jQuery-MDTimePicker/mdtimepicker.js')}}"></script>
@endsection
@push('script')
    <script>
        $('.reject_request').click(function(){
            var request_id = $(this).data('request')
            $('#booking_id').val(request_id)
            $('#transportBookingDecline').modal('show')
        })
    </script>
@endpush
