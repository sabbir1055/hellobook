<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body { font-family: Arial, sans-serif; margin: 20px; }
        h1, h2 { color: #2c3e50; }
        table { width: 100%; border-collapse: collapse; margin-top: 20px; }
        th, td { border: 1px solid #bdc3c7; padding: 10px; text-align: left; }
        th { background-color: #ecf0f1; }
        .highlight { color: #e74c3c; font-weight: bold; }
        .table-column{font-weight: bold; width: 50px !important; border: #0a0a0a solid 1px !important;}
        tr > td {
            border: 1px solid #000; /* Black border */
            padding: 8px; /* Add padding for better spacing */
            text-align: left; /* Align text to the left */
        }
    </style>
</head>
<body>

<h1>Monthly Complaint Management Report</h1>
<p><strong>Reporting Period:</strong> [{{$req['f_registered_date']}}]</p>

<h2>1. Summary Overview</h2>
<ul>
    <li><strong>Total Complaints Registered:</strong> {{$summaryData->total_complain ?? 0}}</li>
    <li><strong>Resolved Complaints:</strong> {{$summaryData->resolved_complain ?? 0}}</li>
    <li><strong>Pending Complaints:</strong> {{$summaryData->pending_complain ?? 0}}</li>
    <li><strong>Average Resolution Time:</strong> {{$summaryData->avg_resolution_time ?? 0}} Days</li>
    <li><strong>Escalated Complaints:</strong> <span class="highlight">{{$summaryData->escalated_complain ?? 0}}</span> (Not resolved within the deadline)</li>
</ul>

<h2>2. Complaints by Category</h2>
<table>
    <tr>
        <th style="border: 1px solid #000; font-weight: bold">Complaint Category</th>
        <th style="border: 1px solid #000; font-weight: bold">Complaints Registered</th>
        <th style="border: 1px solid #000; font-weight: bold">Resolved</th>
        <th style="border: 1px solid #000; font-weight: bold">Pending</th>
    </tr>
    @forelse($categoryData as $category)
    <tr>
        <td style="border: 1px solid #000;">{{ $category->complain_category }}</td>
        <td style="border: 1px solid #000;">{{ $category->total_complain ?? 0 }}</td>
        <td style="border: 1px solid #000;">{{ $category->resolved_complain ?? 0 }}</td>
        <td style="border: 1px solid #000;">{{ $category->pending_complain ?? 0 }}</td>
    </tr>
    @empty
    <tr>
        <td colspan="4" class="text-center">No data available</td>
    </tr>
    @endforelse
</table>

<h2>3. Escalated Complaints (Missed Deadline)</h2>
<table>
    <tr>
        <th style="border: 1px solid #000; font-weight: bold">Complain ID</th>
        <th style="border: 1px solid #000; font-weight: bold">Title</th>
        <th style="border: 1px solid #000; font-weight: bold">Registered Date</th>
        <th style="border: 1px solid #000; font-weight: bold">Estimated Date</th>
        <th style="border: 1px solid #000; font-weight: bold">Done Date</th>
        <th style="border: 1px solid #000; font-weight: bold">Status</th>
    </tr>
    @forelse($overdueData as $overdue)
    <tr>
        <td style="border: 1px solid #000;">{{ $overdue->complain_id }}</td>
        <td style="border: 1px solid #000;">{{ $overdue->title }}</td>
        <td style="border: 1px solid #000;">{{ date('d M, Y h:i A', strtotime($overdue->registerd_date)) }}</td>
        <td style="border: 1px solid #000;">{{ date('d M, Y h:i A', strtotime($overdue->estimated_date)) }}</td>
        <td style="border: 1px solid #000;">{{ $overdue->done_date ? date('d M, Y h:i A', strtotime($overdue->done_date)) : '-' }}</td>
        <td style="border: 1px solid #000;">{{ $overdue->status }}</td>
    </tr>
    @empty
    <tr>
        <td colspan="5" class="text-center">No data available</td>
    </tr>
    @endforelse
</table>

<h2>4. Average Times</h2>
<p><strong>Average response time:</strong> {{round(($responseTimeData->total_response_time/$responseTimeData->total_response_count) / 60, 2)}} Hours</p>
<p><strong>Average done time:</strong> {{round(($responseTimeData->total_done_time/$responseTimeData->total_done_count) / 60, 2)}} Hours</p>

</body>
</html>
