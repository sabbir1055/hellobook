@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{asset('/')}}assets/select2/select2.min.css">
    <link rel="stylesheet" href="{{asset('/assets/Material-Time-Picker-Plugin-jQuery-MDTimePicker/mdtimepicker.css')}}">
@endsection

@section('content')
    @include('breadcrumb',['title' => 'Room Booking'])
    @permission('book_room')
    @include('common.addButton')
    @endpermission
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card mb-3">
                <div class="card-body">
                    <form action="">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('f_booking_date','Booking Date')!!}
                                    {!! Form::text('f_booking_date', @$req['f_booking_date'], ['class'=>'form-control mb-3','id'=>'f_booking_date']);!!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('f_booked_by','Booked By')!!}
                                    {!! Form::select('f_booked_by', $fUsers->pluck('name','id'), @$req['f_booked_by'], ['class'=>'form-control mb-3 select2-filter','id'=>'f_booked_by', 'placeholder' => 'Select a user']);!!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('f_room','Booked Room')!!}
                                    {!! Form::select('f_room', $room, @$req['f_room'], ['class'=>'form-control mb-3 select2-filter','id'=>'f_room', 'placeholder' => 'Select a room']);!!}
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">&nbsp</label>
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <i class="fas fa-search"> Search</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="table table-striped table-bordered table-responsive">
                    <table class="table text-center">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> Title </th>
                            <th> Booked By </th>
                            <th> Included </th>
                            <th> Room Name </th>
                            <th> Meeting Date </th>
                            <th> Time From </th>
                            <th> Time To </th>
                            <th> Refreshment </th>
                            <th> With VC </th>
                            <th> With Projector </th>
                            <th> Booking Time </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $roomBook->appends($req)->firstItem(); @endphp
                        @foreach($roomBook as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->title}} </td>
                                <td class="align-middle"> {{$value->bookedBy->name}} </td>
                                <td class="align-middle">
                                    @foreach(json_decode($value->allocate_with) as $user)
                                        <span class="badge badge-success">{{getUser($user)->name}}</span><br>
                                    @endforeach
                                </td>
                                <td class="align-middle"> {{$value->roomData->name}} </td>
                                <td>{{date('d-M,y',strtotime($value->booked_date))}}</td>
                                <td>{{date('h:i A',strtotime($value->booked_time_from))}}</td>
                                <td>{{date('h:i A',strtotime($value->booked_time_to))}}</td>
                                <td>@if($value->refreshment) <span class="badge badge-success">{{$value->refreshment}}</span> @else <span class="badge badge-info">No</span> @endif </td>
                                <td>@if($value->vc) <span class="badge badge-success">YES</span> @else <span class="badge badge-info">No</span> @endif </td>
                                <td>@if($value->projector) <span class="badge badge-success">YES</span> @else <span class="badge badge-info">No</span> @endif </td>
                                <td class="align-middle"> {{date('d M, Y h:i A', strtotime($value->created_at))}} </td>
                                <td class="align-middle">
                                    @if( date('Y-m-d H:i A',strtotime($value->booked_date.' '. date('h:i A',strtotime($value->booked_time_to)))) <= date('Y-m-d H:i A'))
                                        <span class="badge badge-success"> Meeting deadline finished</span>
                                    @elseif($value->status === 0)
                                        <span class="badge badge-success"> Meeting canceled</span>
                                    @else
                                        @if($value->booked_by == auth()->user()->id && $value->status === 1)
                                            <a onclick="return confirm('Are you sure to remove this meeting?')" href="{{route('booking-room.delete',$value->id)}}" class="btn btn-sm btn-icon btn-secondary edit-btn">
                                                <i class="fa fa-trash"></i>
                                                <span class="sr-only">Delete</span>
                                            </a>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sm pagination-custom">
                    <nav aria-label="Page navigation example" class="m-3">
                        <span>Showing {{ $roomBook->appends($req)->firstItem() }} to {{ $roomBook->appends($req)->lastItem() }} of {{ $roomBook  ->appends($req)->total() }} entries</span>
                        <div>{{ $roomBook->appends($req)->onEachSide(1)->render() }}</div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.roomBooking')
@endsection

@section('script')
    <script src="{{asset('/')}}assets/select2/select2.min.js"></script>
    <script src="{{asset('/assets/Material-Time-Picker-Plugin-jQuery-MDTimePicker/mdtimepicker.js')}}"></script>
@endsection

@push('script')
    <script>
        $(document).ready(() => {
            $('.select2-filter').select2({
                width: '100%'
            })
            $('#f_booking_date').datepicker({
                autoclose:true,
                format: 'yyyy-mm-dd',
                clearBtn: true
            })

            $('#bookRoomModal').on('shown.bs.modal', function () {
                $('.select2').select2({
                    dropdownParent: $('#bookRoomModal'),
                    width: '100%'
                })
            });

            // Destroy Select2 instance when the modal is hidden
            $('#bookRoomModal').on('hidden.bs.modal', function () {
                $('.select2').each(function() {
                    if ($(this).data('select2')) {
                        $(this).select2('destroy');
                    }
                });
            });
        })

        $('#add-new-btn').click(function(){
            $('.add-btn').text('Add')
            $('#bookRoomModalLabel').text('Book Room')
            var url = "{{route('booking-room.store')}}"
            $('#room_booking_form').attr('action', url)
            $('#room_booking_form').find("input[type=text]").val("")

            $('#time_to').mdtimepicker({
                minTime:"09:00 AM",
                maxTime:"09:00 PM"
            })
            $('#time_from').mdtimepicker({
                minTime:"09:00 AM",
                maxTime:"09:00 PM"
            })
            $("option:selected").prop("selected", false)

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date()
            });
            $('#bookRoomModal').modal('show')
        })

        $('.add-btn').click(function(){
            $('#room_booking_form').submit();
            $(this).attr('disabled',true)
        })

        function getAvailableRoom(){
            var time_from = $('#time_from').val()
            var time_to = $('#time_to').val()
            var date = $('#date').val()
            $.ajax({
                url: "{{route('available.room')}}",
                method: 'POST',
                'data': {
                    '_token': "{{csrf_token() }}",
                    'time_from' : time_from,
                    'time_to' : time_to,
                    'date' : date
                },
                success: function(data) {
                    let html = '<option value=""> Select a room </option>';
                    data.map(function(d){
                        html += `<option value="${d.id}"> ${d.name}</option>`
                    })
                    $('#room_no').html(html).select2()
                },
                err: function(err) {
                    alert('No data found !!!!')
                }
            });
        }

        $('#room_no').change(function(){
            var vc_room = ['2','4','11','6']
            if(vc_room.indexOf($(this).val()) !== -1){
                $('#vc').attr('disabled',false);
            } else{
                $('#vc').val(0);
                $('#vc').attr('disabled',true);
            }
        })
    </script>
@endpush
