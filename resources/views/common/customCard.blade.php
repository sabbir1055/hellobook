<div class="c_card-card bg-silken">
    <div class="c_card-title">{{$title}}</div>
    <div class="d-flex justify-content-center align-items-center">
        <i class="{{$icon}} c_card-icon text-white"></i>
        <div class="c_card-count text-white">{{$data}}</div>
    </div>
</div>
