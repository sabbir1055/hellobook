@if($status == 0)
    <h3>Dear {{$receiverName}},</h3>
    <p>A new complaint has been assigned to you{{$isSupervisor? 'r department' : ' in the Complaint Management System.'}} </p>
    <h4> Below are the details:</h4>
    <ul>
        <li><b>Complaint ID:</b> {{$complain->complain_id}}</li>
        <li><b>Title:</b> {{$complain->title}}</li>
        <li><b>Submitted By:</b> {{$complain->registerBy->name}}</li>
        <li><b>Submission Date:</b> {{date('d M, Y h:i A', strtotime($complain->created_at))}}</li>
        <li><b>Department:</b> {{$complain->registerBy->getDepartment->name ?? 'N\A'}}</li>
    </ul>
    <p>Please take the necessary steps to address this complaint.
        You can view and update the status of the complaint by logging into the system using the link below:
    </p>
    <a href="{{route('complain.view', ['complain' => $complain])}}">Access Complaint Management System</a>

    <p><b>Important:</b> Please update the estimated timeline for resolution and status as soon as possible.</p>

    <p>If you have any questions, feel free to contact the support team.</p>

    <h4>Best regards,</h4>
    <strong>{{$actorName}}</strong>
@elseif (in_array($status, [1,4,5]))
    <h3>Dear {{$receiverName}},</h3>
    <p>The status of your complaint has been updated. </p>
    <h4>Below are the latest details:</h4>
    <ul>
        <li><b>Complaint ID:</b> {{$complain->complain_id}}</li>
        <li><b>Title:</b> {{$complain->title}}</li>
        <li><b>Updated Status:</b> {{$complain->status}}</li>
        <li><b>Updated By:</b> {{$actorName}}</li>
        <li><b>Estimated Resolution Date:</b> {{$complain->estimated_date}}</li>
    </ul>
    <p>If you have any questions or need further assistance, please feel free to reach out.</p>

    <p>Please take the necessary steps to address this complaint.
        You can view and update the status of the complaint by logging into the system using the link below:
    </p>
    <a href="{{route('complain.view', ['complain' => $complain])}}">Access Complaint Management System</a>

    <p><b>Thank you for your patience!</b></p>

    <h4>Best regards,</h4>
    <strong>{{$actorName}}</strong>
@elseif($status == 2)
    <h3>Dear {{$receiverName}},</h3>
    <p>The resolution for the following complaint has been withdraw by the complainer. </p>
    <h4>Below are the latest details:</h4>
    <ul>
        <li><b>Complaint ID:</b> {{$complain->complain_id}}</li>
        <li><b>Title:</b> {{$complain->title}}</li>
        <li><b>Withdraw By:</b> {{$actorName}}</li>
        <li><b>Withdraw Date:</b> {{date('d M, Y h:i A')}}</li>
        <li><b>Reason for Withdraw (if provided):</b> {{$complain->withdraw_reason}}</li>
    </ul>

    <p>Please take the necessary steps to address this complaint.
        You can view and update the status of the complaint by logging into the system using the link below:
    </p>
    <a href="{{route('complain.view', ['complain' => $complain])}}">Access Complaint Management System</a>

    <h4>Best regards,</h4>
    <strong>{{$actorName}}</strong>
@elseif($status == 3)
    <h3>Dear {{$receiverName}},</h3>
    <p>The resolution for the following complaint has been rejected by the {{$actorName}}. </p>
    <h4>Below are the latest details:</h4>
    <ul>
        <li><b>Complaint ID:</b> {{$complain->complain_id}}</li>
        <li><b>Title:</b> {{$complain->title}}</li>
        <li><b>Reject By:</b> {{$actorName}}</li>
        <li><b>Reject Date:</b> {{date('d M, Y h:i A')}}</li>
        <li><b>Reason for Rejection (if provided):</b> {{$complain->reject_reason}}</li>
    </ul>

    <p>Please take the necessary steps to address this complaint.
        You can view and update the status of the complaint by logging into the system using the link below:
    </p>
    <a href="{{route('complain.view', ['complain' => $complain])}}">Access Complaint Management System</a>

    <h4>Best regards,</h4>
    <strong>{{$actorName}}</strong>
@else
    <h2>No Preview for invalid booking status</h2>
@endif
