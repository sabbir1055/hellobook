<h3>Dear Concern,</h3>
@if($status)
    <p>As an arrival of a new employee with the conformation from the HR, Please provide a Corporate mobile SIM with {{$balance}} Tk of balance. And please note that if this user already assigned a sim please remove it and assign provided one.Please see the below details -</p>
@else
    <p>Please find the user info bellow and unassign a sim from this user.</p>
@endif
    </hr>
    <table>
        <tr>
            <td> Employee name</td>
            <td> :</td>
            <td> <strong>{{$user->name}}</strong></td>
        </tr>
        <tr>
            <td> Employee ID</td>
            <td> :</td>
            <td> <strong>{{$user->employee_id}}</strong></td>
        </tr>
        <tr>
            <td> Designation </td>
            <td> :</td>
            <td> <strong>{{ $user->getDesignation->name }}</strong></td>
        </tr>
        <tr>
            <td> Department </td>
            <td> :</td>
            <td> <strong>{{ $user->getDepartment->name }}</strong></td>
        </tr>
        <tr>
            <td> SIM No</td>
            <td> :</td>
            <td> <strong>{{$msisdn}}</strong></td>
        </tr>
        @if($status)
        <tr>
            <td> Limit </td>
            <td> :</td>
            <td> <strong>{{$balance}} Tk</strong></td>
        </tr>
        @endif
    </table>
<p></p>
<h4>Thank you</h4>
<hr>
<strong>{{auth()->user()->name}}</strong><br>
<strong>{{auth()->user()->getDesignation->name}}</strong><br>
<strong>Contact No : {{(auth()->user()->assignedNumber->msisdn ?? auth()->user()->msisdn)}}</strong>
