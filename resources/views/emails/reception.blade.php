<h3>Dear Concern,</h3>
@if($status)
    <p>One of our colleague {{$fromUser->name}} has been prepared the meeting.  Hope you would facilitate the meeting in an orderly fashion and make sure everyone is accommodated. Check the meeting details below - </p>
    </hr>
    <table>
        <tr>
            <td> Title of the meeting</td>
            <td> :</td>
            <td> <strong>{{$booking->title}}</strong></td>
        </tr>
        <tr>
            <td> Meeting host</td>
            <td> :</td>
            <td> <strong>{{$fromUser->name}}</strong></td>
        </tr>
        <tr>
            <td> Required room</td>
            <td> :</td>
            <td> <strong>{{$booking->roomData->name}}</strong></td>
        </tr>
        <tr>
            <td> Meeting Date</td>
            <td> :</td>
            <td> <strong>{{$booking->booked_date}}</strong></td>
        </tr>
        <tr>
            <td> Meeting Time</td>
            <td> :</td>
            <td> <strong>{{date('h:i A',strtotime($booking->booked_time_from))}} - {{date('h:i A',strtotime($booking->booked_time_to))}}</strong></td>
        </tr>
        <tr>
            <td> Refreshment </td>
            <td> :</td>
            <td> <strong>{{$booking->refreshment}}</strong></td>
        </tr>
        <tr>
            <td> VC </td>
            <td> :</td>
            <td> <strong>{{$booking->vc ? 'Yes' : 'No'}}</strong></td>
        </tr>
        <tr>
            <td> Projector </td>
            <td> :</td>
            <td> <strong>{{$booking->projector ? 'Yes' : 'No' }}</strong></td>
        </tr>
        <tr>
            <td> Note</td>
            <td> :</td>
            <td> <strong>{{$booking->narration}}</strong></td>
        </tr>
    </table>
@else
    <p>Booked room : {{getRoom($booking->room_id)->name}} for a meeting which title : {{$booking->title}} and Time : {{$booking->booked_date }}, {{date('h:i A',strtotime($booking->booked_time_from))}}  is canceled for some reason. Sorry for the inconveniences. </p>
@endif
<p></p>
<h4>Thank you</h4>
<hr>
<strong>{{$fromUser->name}}</strong><br>
<strong>{{$fromUser->getDesignation->name}}</strong><br>
<strong>Contact No : {{($fromUser->assignedNumber->msisdn ?? $fromUser->msisdn)}}</strong>
