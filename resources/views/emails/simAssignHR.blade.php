<h3>Dear Concern,</h3>
<p>There has been an arrival of a new colleague in the company. I am requesting to HR would approve of a new Corporate mobile SIM with sufficient amount of balance for my new team member. Please see the below details –</p>
</hr>
<table>
    <tr>
        <td> Employee name</td>
        <td> :</td>
        <td> <strong>{{$user->name}}</strong></td>
    </tr>
    <tr>
        <td> Employee ID</td>
        <td> :</td>
        <td> <strong>{{$user->employee_id}}</strong></td>
    </tr>
    <tr>
        <td> Designation </td>
        <td> :</td>
        <td> <strong>{{ $user->getDesignation->name }}</strong></td>
    </tr>
    <tr>
        <td> Department </td>
        <td> :</td>
        <td> <strong>{{ $user->getDepartment->name }}</strong></td>
    </tr>
</table>
<p></p>
<h4>Thank you</h4>
<hr>
<strong>{{auth()->user()->name}}</strong><br>
<strong>{{auth()->user()->getDesignation->name}}</strong><br>
<strong>Contact No : {{(auth()->user()->assignedNumber->msisdn ?? auth()->user()->msisdn)}}</strong>
