@if($status == 0)
    <h3>Dear Concern,</h3>
    <p>You have received a new transport {{$booking->visit_type === 'office' ? "Seat" : null }} requisition for review and approval. Below are the details of the request.</p>
    <h4>Request Details:</h4>
    <ul>
        <li><b>Employee Name:</b> {{$booking->bookedBy->name}}</li>
        <li><b>Date:</b> {{$booking->booked_date}}</li>
        @if($booking->visit_type === 'outside_visit')
            <li><b>Time:</b> {{date('h:i A',strtotime($booking->booked_time_from))}} - {{date('h:i A',strtotime($booking->booked_time_to))}}</li>
        @endif
        <li><b>Vehicle Number:</b> {{$booking->transportDetail->car_number}}</li>
        @if($booking->visit_type === 'office')
            <li><b>Trip:</b> Trip {{$booking->trip_no}}</li>
        @endif
        <li><b>Reason (if provided):</b> {{$booking->comments}}</li>
    </ul>
    <p>Please review the requisition and take the necessary action at your earliest convenience.</p>

    <h4>Best regards,</h4>
    <strong>{{$actorName}}</strong>
@elseif ($status == 1)
    <h3>Dear {{$booking->bookedBy->name}},</h3>
    <p>This is to inform you that your transport {{$booking->visit_type === 'office' ? "Seat" : null }} requisition for {{$booking->booked_date}} has been approved. A seat has been successfully allocated to you as per your request.</p>
    <h4>Request Details:</h4>
    <ul>
        <li><b>Date:</b> {{$booking->booked_date}}</li>
        @if($booking->visit_type === 'outside_visit')
            <li><b>Time:</b> {{date('h:i A',strtotime($booking->booked_time_from))}} - {{date('h:i A',strtotime($booking->booked_time_to))}}</li>
        @endif
        <li><b>Vehicle Number:</b> {{$booking->transportDetail->car_number}}</li>
        @if($booking->visit_type === 'office')
            <li><b>Trip:</b> Trip {{$booking->trip_no}}</li>
        @endif
    </ul>
    <p>Please ensure that you are at the pickup point on time to avoid any inconvenience.</p>

    <h4>Best regards,</h4>
    <strong>{{$actorName}}</strong>
@elseif($status == 2)
    <h3>Dear {{$booking->bookedBy->name}},</h3>
    <p>We regret to inform you that your transport {{$booking->visit_type === 'office' ? "Seat" : null }} requisition for {{$booking->booked_date}} has not been approved due to {{$booking->reject_reason}}.</p>
    <h4>Request Details:</h4>
    <ul>
        <li><b>Employee Name:</b> {{$booking->bookedBy->name}}</li>
        <li><b>Date:</b> {{$booking->booked_date}}</li>
        @if($booking->visit_type === 'outside_visit')
            <li><b>Time:</b> {{date('h:i A',strtotime($booking->booked_time_from))}} - {{date('h:i A',strtotime($booking->booked_time_to))}}</li>
        @endif
        <li><b>Vehicle Number:</b> {{$booking->transportDetail->car_number}}</li>
        @if($booking->visit_type === 'office')
            <li><b>Trip:</b> Trip {{$booking->trip_no}}</li>
        @endif
    </ul>
    <p>We understand the importance of your request and apologize for any inconvenience caused.</p>

    <h4>Best regards,</h4>
    <strong>{{$actorName}}</strong>
@elseif($status == 3)
    <h3>Dear Concern,</h3>
    <p>I withdraw my transport {{$booking->visit_type === 'office' ? "Seat" : null }} requisition for some of my personal dependencies.</p>
    <h4>Request Details:</h4>
    <ul>
        <li><b>Employee Name:</b> {{$booking->bookedBy->name}}</li>
        <li><b>Date:</b> {{$booking->booked_date}}</li>
        @if($booking->visit_type === 'outside_visit')
            <li><b>Time:</b> {{date('h:i A',strtotime($booking->booked_time_from))}} - {{date('h:i A',strtotime($booking->booked_time_to))}}</li>
        @endif
        <li><b>Vehicle Number:</b> {{$booking->transportDetail->car_number}}</li>
        @if($booking->visit_type === 'office')
            <li><b>Trip:</b> Trip {{$booking->trip_no}}</li>
        @endif
        <li><b>Reason (if provided):</b> {{$booking->comments}}</li>
    </ul>

    <h4>Best regards,</h4>
    <strong>{{$actorName}}</strong>
@else
    <h2>No Preview for invalid booking status</h2>
@endif
