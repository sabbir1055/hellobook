<h3>Dear Concern,</h3>
@if($status)
    <p>One of our colleague {{$fromUser->name}} has been called a meeting.
        Hope you would facilitate the meeting in an orderly fashion and make sure facilities are available in required room.
        Check the meeting details below -
    </p>
    <br>
    <table>
        <tr>
            <td> Title</td>
            <td> :</td>
            <td> <strong>{{$booking->title}}</strong></td>
        </tr>
        <tr>
            <td> Responsible Person</td>
            <td> :</td>
            <td> <strong>{{$fromUser->name}}</strong></td>
        </tr>
        <tr>
            <td> Room name</td>
            <td> :</td>
            <td> <strong>{{$booking->roomData->name}}</strong></td>
        </tr>
        <tr>
            <td> Meeting Date</td>
            <td> :</td>
            <td> <strong>{{$booking->booked_date}}</strong></td>
        </tr>
        <tr>
            <td> Meeting Time</td>
            <td> :</td>
            <td> <strong>{{date('h:i A',strtotime($booking->booked_time_from))}} - {{date('h:i A',strtotime($booking->booked_time_to))}}</strong></td>
        </tr>
        <tr>
            <td> Projector Availability </td>
            <td> :</td>
            <td> <strong>Yes</strong></td>
        </tr>
    </table>
@else
    <p>One of our colleague {{$fromUser->name}} booked a meeting which title {{$booking->title}} and Time slot {{$booking->booked_date }}, {{date('h:i A', strtotime($booking->booked_time_from))}} is canceled for some reason. Sorry for the inconvenience. </p>
@endif
<p></p>
<h4>Thank you</h4>
<hr>
<strong>{{$fromUser->name}}</strong><br>
<strong>{{$fromUser->getDesignation->name}}</strong><br>
<strong>Contact No : {{($fromUser->assignedNumber->msisdn ?? $fromUser->msisdn)}}</strong>
