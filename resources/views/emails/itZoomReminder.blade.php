<h3>Dear Concern,</h3>
@if($status)
    <p>One of our colleague {{$fromUser->name}} has been prepared the virtual meeting. Hope you would facilitate the meeting in an orderly fashion and make sure everyone is align with the meeting and accommodated. Check the meeting details below -
    </hr>
    <table>
        <tr>
            <td> Title of the meeting</td>
            <td> :</td>
            <td> <strong>{{$booking->title}}</strong></td>
        </tr>
        <tr>
            <td> Meeting host</td>
            <td> :</td>
            <td> <strong>{{$fromUser->name}}</strong></td>
        </tr>
        <tr>
            <td> Required room </td>
            <td> :</td>
            <td> <strong>Silken VC’s Personal Meeting Room at {{$booking->roomData->name}}</strong></td>
        </tr>
        <tr>
            <td> Meeting ID </td>
            <td> :</td>
            <td> <strong>659 813 8390</strong></td>
        </tr>
        <tr>
            <td> Passcode  </td>
            <td> :</td>
            <td> <strong>silken#123</strong></td>
        </tr>
        <tr>
            <td> Meeting Date</td>
            <td> :</td>
            <td> <strong>{{$booking->booked_date}}</strong></td>
        </tr>
        <tr>
            <td> Meeting Time</td>
            <td> :</td>
            <td> <strong>{{date('h:i A',strtotime($booking->booked_time_from))}} - {{date('h:i A',strtotime($booking->booked_time_to))}}</strong></td>
        </tr>
    </table>
@else
    <p>One of our colleague {{$fromUser->name}} booked a meeting which title {{$booking->title}} and Time slot {{$booking->booked_date }}, {{date('h:i A', strtotime($booking->booked_time_from))}} is canceled for some reason. Sorry for the inconvenience. </p>
@endif
<p></p>
<h4>Thank you</h4>
<hr>
<strong>{{$fromUser->name}}</strong><br>
<strong>{{$fromUser->getDesignation->name}}</strong><br>
<strong>Contact No : {{($fromUser->assignedNumber->msisdn ?? $fromUser->msisdn)}}</strong>

