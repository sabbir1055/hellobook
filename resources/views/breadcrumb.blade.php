<div class="col-12">
    <div class="dashboard_header mb_10">
        <div class="row">
            <div class="col-lg-6">
                <div class="dashboard_header_title">
                    <h4>{{$title}}@if(isset($sub_title)) <small>({{$sub_title}})</small> @endif</h4>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="dashboard_breadcam text-right">
                    @if(!$title !== 'Dashboard') <p><a href="{{route('dashboard')}}">Dashboard</a> <i class="fas fa-caret-right"></i> {{$title}}</p> @endif
                </div>
            </div>
        </div>
    </div>
</div>
