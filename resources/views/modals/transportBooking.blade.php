<div class="modal fade" id="transportBookModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="transportBookModalLabel">Add Premission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'booking-room.store','method'=>'post','id'=>'user_form']) !!}
            {!! Form::token() !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('title','Booking Title')!!}
                            {!! Form::text('title', '', ['class'=>'form-control mb-3','required'=>'required','id'=>'title']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('visit_type','Request Type')!!}
                            {!! Form::select('visit_type', ['office' => 'Office', 'outside_visit' => 'Outside Visit'], null, ['class'=>'form-control mb-3 select2','required'=>'required','id'=>'visit_type']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('date','Request Date')!!}
                            {!! Form::text('date', '', ['class'=>'form-control mb-3 datepicker','required'=>'required','id'=>'date','onChange'=>'getAvailableCar()']);!!}
                        </div>
                    </div>
                    <div class="col-md-6 d-none time-from-select-div">
                        <div class="form-group">
                            {!! Form::label('time_from','Time From')!!}
                            {!! Form::text('time_from', '', ['class'=>'form-control mb-3 timepicker','id'=>'time_from','onChange'=>'getAvailableCar()']);!!}
                        </div>
                    </div>
                    <div class="col-md-6 d-none time-to-select-div">
                        <div class="form-group">
                            {!! Form::label('time_to','Time To')!!}
                            {!! Form::text('time_to', '', ['class'=>'form-control mb-3 timepicker','id'=>'time_to','onChange'=>'getAvailableCar()']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('transport_id','Select Car')!!}
                            {!! Form::select('transport_id', [''=>'Select a car'], null, ['class'=>'form-control mb-3 select2','id'=>'transport_id','required'=>'required']);!!}
                        </div>
                    </div>
                    <div class="col-md-6 d-none car-trip-choose-div">
                        <div class="form-group">
                            {!! Form::label('trip_id','Trip Name')!!}
                            {!! Form::select('trip_id', [''=>'Select a trip'], null, ['class'=>'form-control mb-3 select2','id'=>'trip_id']);!!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('comment','Comment')!!}
                            {!! Form::text('comment',null, ['class'=>'form-control mb-3','id'=>'comment']);!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary add-btn">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
