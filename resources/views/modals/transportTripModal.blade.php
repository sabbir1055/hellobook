<div class="modal fade" id="transportTripModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="transportTripLabel">Add User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'transport.store','method'=>'post','id'=>'trip_form']) !!}
            {!! Form::token() !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('allocate_users','Allocate Employees')!!}
                            {!! Form::select('allocate_users[]', $users, null, ['class'=>'form-control mb-3 select2','required'=>'required','id'=>'allocate_users','multiple'=>'multiple']);!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary add-btn">Update</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
