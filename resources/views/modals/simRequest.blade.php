<div class="modal fade" id="simRequestModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="simRequestModalLabel">Add Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'sim.store','method'=>'post','id'=>'sim_request_form','enctype'=>'multipart/form-data']) !!}
            {!! Form::token() !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('user_id','Name *')!!}
                            @if(auth()->user()->can('global_sim_request'))
                                {!! Form::select('user_id', [''=>'Please select a sim user']+$users,null, ['class'=>'form-control mb-3 select2','id'=>'user_id','required'=>true]);!!}
                            @else
                                {!! Form::select('user_ids', [''=>'Please select a sim user']+$users,auth()->user()->id, ['class'=>'form-control mb-3','id'=>'user_id','required'=>true,'disabled'=>true]);!!}
                                {!! Form::hidden('user_id',auth()->user()->id) !!}
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('priority','Priority *')!!}
                            {!! Form::select('priority', [''=>'Select a priority','1'=>'Low','2'=>'Mid','3'=>'High'],null, ['class'=>'form-control mb-3 select2','id'=>'priority']);!!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('subject','Subject *')!!}
                            {!! Form::text('subject', null, ['class'=>'form-control mb-3','id'=>'subject']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('file','Attachments')!!}
                            {!! Form::file('file', ['class'=>'form-control mb-3','id'=>'file']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('limit','Limit *')!!}
                            {!! Form::number('limit',null, ['class'=>'form-control mb-3','id'=>'limit','required' => true]);!!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('reason','Messsage')!!}
                            {!! Form::textarea('reason', '', ['class'=>'form-control mb-3','id'=>'reason', 'rows' => 3]);!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary add-btn">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
