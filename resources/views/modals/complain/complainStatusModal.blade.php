<div class="modal fade" id="complainStatusModal" tabindex="-1" role="dialog" aria-labelledby="complainStatusRegisterByModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="permissionModalLabel">Update Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'complain.status.update','method'=>'post','id'=>'complainStatusRegisterBy_form']) !!}
            {!! Form::token() !!}
            <div class="modal-body">
                <div class="row">
                    {!! Form::hidden('complain_id', '', ['class'=>'form-control mb-3','id'=>'complain_id']);!!}
                    {!! Form::hidden('view_type', $registerBy ? 'registerBy' : 'registerTo' , ['class'=>'form-control mb-3','id'=>'view_type']);!!}
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('status','Status')!!}
                            {!! Form::select('status',  $registerBy ? [2 => 'Withdraw', 5 => 'Complete'] : [1 => 'In Progress', 3 => 'Reject', 4 => 'Done'], null, ['class'=>'form-control select2 mb-3','required'=>'required','id'=>'status', 'placeholder' => 'Select status']);!!}
                        </div>
                    </div>
                    <div class="col-md-12 d-none reason_block">
                        <div class="form-group">
                            {!! Form::label('reason','Reason')!!}
                            {!! Form::text('reason',  null, ['class'=>'form-control mb-3','id'=>'reason']);!!}
                        </div>
                    </div>
                    <div class="col-md-12 d-none rating_block">
                        <div class="form-group">
                            <div class="rating">
                                <input type="radio" name="rating" value="5" id="5"><label for="5">☆</label>
                                <input type="radio" name="rating" value="4" id="4"><label for="4">☆</label>
                                <input type="radio" name="rating" value="3" id="3"><label for="3">☆</label>
                                <input type="radio" name="rating" value="2" id="2"><label for="2">☆</label>
                                <input type="radio" name="rating" value="1" id="1"><label for="1">☆</label>
                            </div>
{{--                            {!! Form::number('rating',  null, ['class'=>'form-control mb-3','id'=>'rating']);!!}--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary add-btn">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
