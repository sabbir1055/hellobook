<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="userModalLabel">Add Premission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'user.store','method'=>'post','id'=>'user_form','enctype'=>'multipart/form-data']) !!}
            {!! Form::token() !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('name','Name')!!}
                            {!! Form::text('name', '', ['class'=>'form-control mb-3','id'=>'name','required'=>true]);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('username','User Name')!!}
                            {!! Form::text('username', '', ['class'=>'form-control mb-3','id'=>'username','required'=>true]);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('employee_id','Employee ID')!!}
                            {!! Form::text('employee_id', '', ['class'=>'form-control mb-3','id'=>'employee_id','required'=>true]);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('email','Email')!!}
                            {!! Form::text('email', '', ['class'=>'form-control mb-3','id'=>'email']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('msisdn','Mobile No')!!}
                            {!! Form::text('msisdn', '', ['class'=>'form-control mb-3','id'=>'msisdn']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('department','Select Department')!!}
                            {!! Form::select('department', [''=>'Select a department']+$department, null, ['class'=>'form-control mb-3','id'=>'department','required'=>true]);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('designation','Select Designation')!!}
                            {!! Form::select('designation', [''=>'Select a designation']+[], null, ['class'=>'form-control mb-3','id'=>'designation','required'=>true]);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('reporting_person_employee_id','Reporting To')!!}
                            {!! Form::text('reporting_person_employee_id',  null, ['class'=>'form-control mb-3','id'=>'reporting_person_employee_id']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('role','Select Role')!!}
                            {!! Form::select('role', [''=>'Select a role']+$role, null, ['class'=>'form-control mb-3','id'=>'role','required'=>true]);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('image','Image')!!}
                            {!! Form::file('image', ['class'=>'form-control mb-3','id'=>'image']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('status','Select status')!!}
                            {!! Form::select('status', [1=>'Active',0=>'Inactive'], null, ['class'=>'form-control mb-3','id'=>'status','required'=>true]);!!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('linkedin_profile','LinkedIn Profile')!!}
                            {!! Form::text('linkedin_profile', null, ['class'=>'form-control mb-3','id'=>'linkedin_profile']);!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary add-btn">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
