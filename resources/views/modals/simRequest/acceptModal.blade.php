<div class="modal fade" id="acceptModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="permissionModalLabel">Assign Sim</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => ['sim.request.accept',$sim_request->id],'method'=>'post','id'=>'permission_form']) !!}
            {!! Form::token() !!}
            <div class="modal-body">
                <div class="row">
                    @if(!$assgned_sim)
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('assign_msisdn','Please Assign a sim')!!}
                                {!! Form::select('assign_msisdn',[''=>'Select a sim']+$un_assgned_sim, null, ['class'=>'form-control select2 mb-3','id'=>'assign_msisdn','required'=>true]);!!}
                            </div>
                        </div>
                    @else
                        <input type="hidden" name="assign_msisdn" value="{{$assgned_sim->msisdn}}">
                    @endif
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('req_amount','Request Limit')!!}
                            {!! Form::number('req_amount',$sim_request->limit, ['class'=>'form-control mb-3','id'=>'req_amount','required'=>true]);!!}
                        </div>
                    </div>
                    {!! Form::hidden('user_id',$sim_request->user_id,['id'=>'user_id']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button onclick="return confirm('Are you sure to assign?')" type="submit" class="btn btn-primary add-btn">Assign</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
