<div class="modal fade" id="updateSimRequestModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateSimRequestModalLabel">Sim Request Update</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => ['sim.request.update',$sim_request->id],'method'=>'post','id'=>'update_sim_request_form','enctype'=>'multipart/form-data']) !!}
            {!! Form::token() !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('name','Name *')!!}
                            {!! Form::text('name', $sim_request->getUser->name, ['class'=>'form-control mb-3','id'=>'name']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('priority','Priority *')!!}
                            {!! Form::select('priority', [''=>'Select a priority','1'=>'Low','2'=>'Mid','3'=>'High'],$sim_request->priority, ['class'=>'form-control mb-3','id'=>'priority']);!!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('subject','Subject *')!!}
                            {!! Form::text('subject', $sim_request->subject, ['class'=>'form-control mb-3','id'=>'subject']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('file','Attachments')!!}
                            {!! Form::file('file', ['class'=>'form-control mb-3','id'=>'file']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('limit','Limit *')!!}
                            {!! Form::number('limit', $sim_request->limit, ['class'=>'form-control mb-3','id'=>'limit']);!!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('reason','Messsage')!!}
                            {!! Form::textarea('reason', $sim_request->reason, ['class'=>'form-control mb-3','id'=>'reason', 'cols' => 3]);!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary add-btn">Update</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
