<div class="modal fade" id="transportBookingDecline" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="permissionModalLabel">Request Reject</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'transport.request.status.decline','method'=>'post','id'=>'transport_form']) !!}
            {!! Form::token() !!}
            <div class="modal-body">
                <div class="row">
                    {!! Form::hidden('booking_id', '', ['class'=>'form-control mb-3','id'=>'booking_id']);!!}
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('reject_reason','Reject Reason')!!}
                            {!! Form::text('reject_reason',  null, ['class'=>'form-control mb-3','required'=>'required','id'=>'reject_reason']);!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary add-btn">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
