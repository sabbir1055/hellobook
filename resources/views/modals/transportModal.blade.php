<div class="modal fade" id="transportModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="permissionModalLabel">Add Premission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'transport.store','method'=>'post','id'=>'transport_form']) !!}
            {!! Form::token() !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('name','Car Name')!!}
                            {!! Form::text('car_name', '', ['class'=>'form-control mb-3','id'=>'name']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('car_number','Car Number')!!}
                            {!! Form::text('car_number', '', ['class'=>'form-control mb-3','id'=>'number']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('capacity','Capacity')!!}
                            {!! Form::text('capacity', '', ['class'=>'form-control mb-3','id'=>'capacity']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('trip_count','Total Trip')!!}
                            {!! Form::number('trip_count', 1, ['class'=>'form-control mb-3','id'=>'trip_count']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('car_type','Car Type')!!}
                            {!! Form::select('car_type', [''=> 'Select a transport', 'office_transport' => 'Office transport','public_transport' => 'Public transport', 'others_transport' => 'Others transport'], null, ['class'=>'form-control mb-3','required'=>'required','id'=>'car_type']);!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary add-btn">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
