<div class="modal fade" id="bookRoomModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="bookRoomModalLabel">Add Premission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'booking-room.store','method'=>'post','id'=>'room_booking_form']) !!}
            {!! Form::token() !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mx-sm-3 mb-2">
                            {!! Form::label('title','Meeting Title')!!}
                            {!! Form::text('title', old('title'), ['class'=>'form-control mb-3','required'=>'required','id'=>'title']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mx-sm-3 mb-2">
                            {!! Form::label('include','Add Colleagues')!!}
                            {!! Form::select('include[]', $users, old('include'), ['class'=>'form-control mb-3 select2','required'=>'required','id'=>'include','multiple'=>true]);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mx-sm-3 mb-2">
                            {!! Form::label('date','Meeting Date')!!}
                            {!! Form::text('date', old('date'), ['class'=>'form-control mb-3 datepicker','required'=>'required','id'=>'date','onChange'=>'getAvailableRoom()']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mx-sm-3 mb-2">
                            {!! Form::label('time_from','Time From')!!}
                            {!! Form::text('time_from', old('time_from'), ['class'=>'form-control mb-3 timepicker','required'=>'required','id'=>'time_from','onChange'=>'getAvailableRoom()']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mx-sm-3 mb-2">
                            {!! Form::label('time_to','Time To')!!}
                            {!! Form::text('time_to', old('time_to'), ['class'=>'form-control mb-3 timepicker','required'=>'required','id'=>'time_to','onChange'=>'getAvailableRoom()']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mx-sm-3 mb-2">
                            {!! Form::label('room_no','Room Name')!!}
                            {!! Form::select('room_no', [''=>'Select a room'], old('room_no'), ['class'=>'form-control mb-3 select2','id'=>'room_no','required'=>'required']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mx-sm-3 mb-2">
                            {!! Form::label('refreshment','Refreshment')!!}
                            {!! Form::select('refreshment', [' '=>'No need','snacks'=>'Snacks','lunch'=>'Lunch','tea'=>'Tea','coffee'=>'Coffee'], old('refreshment'), ['class'=>'form-control mb-3','id'=>'refreshment']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mx-sm-3 mb-2">
                            {!! Form::label('projector','Projector')!!}
                            {!! Form::select('projector', [0=>'No',1=>'Yes'], old('projector'), ['class'=>'form-control mb-3','id'=>'projector','required'=>'required']);!!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group mx-sm-3 mb-2">
                            {!! Form::label('vc','VC Type')!!}
                            {!! Form::select('vc', [0=>'Without VC',1=>'With VC'], old('vc'), ['class'=>'form-control mb-3','id'=>'vc','required'=>'required']);!!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group mx-sm-3 mb-2">
                            {!! Form::label('naration','Special Requirement')!!}
                            {!! Form::textarea('naration', old('naration'), ['class'=>'form-control mb-3','id'=>'narration', 'rows' => 5]);!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary add-btn">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
