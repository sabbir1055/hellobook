<div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="roleModalLabel">Add Premission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'role.store','method'=>'post','id'=>'role_form']) !!}
            {!! Form::token() !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('name','Role Name')!!}
                            {!! Form::text('name', '', ['class'=>'form-control mb-3','id'=>'name']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('display_name','Role Display Name')!!}
                            {!! Form::text('display_name', '', ['class'=>'form-control mb-3','id'=>'display_name']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('permission','Select Permission')!!}
                            {!! Form::select('permission[]', $permission, [], ['class'=>'form-control mb-3 select2','id'=>'permission','multiple'=>'multiple']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('description','Role Description')!!}
                            {!! Form::textarea('description', '', ['class'=>'form-control mb-3','id'=>'description']);!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary add-btn">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
