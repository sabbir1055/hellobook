<div class="modal fade" id="designationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="designationModalLabel">Add Premission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'designation.store','method'=>'post','id'=>'designation_form']) !!}
            {!! Form::token() !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('name','Designation Name')!!}
                            {!! Form::text('name', '', ['class'=>'form-control mb-3','id'=>'name']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('short_name','Designation Short Name')!!}
                            {!! Form::text('short_name', '', ['class'=>'form-control mb-3','id'=>'short_name']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('department','Select Department')!!}
                            {!! Form::select('department', $department, [], ['class'=>'form-control mb-3 select2','id'=>'department', 'placeholder' => 'Select department']);!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('status','Select Status')!!}
                            {!! Form::select('status', ['1'=>'Active','0'=>'Inactive'], [], ['class'=>'form-control mb-3 select2','id'=>'status']);!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary add-btn">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
