@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{asset('/')}}assets/select2/select2.min.css">
@endsection
@section('content')
    @permission('add_sim')
    @include('common.addButton')
    @endpermission
    @include('breadcrumb', ['title' => 'SIM Management'])
    <div class="main_content_iner">
        <div class="container-fluid p-0 sm_padding_15px">
            <div class="card mb-3">
                <div class="card-body">
                    <form action="">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('f_msisdn','Mobile No')!!}
                                    {!! Form::text('f_msisdn',  @$req['f_msisdn'], ['class'=>'form-control mb-3','id'=>'f_msisdn']);!!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('f_status','Status')!!}
                                    {!! Form::select('f_status', [''=>'Select a status',0=>'Un Assigned',1=>'Assigned'], @$req['f_status'], ['class'=>'form-control mb-3 select2','id'=>'f_status']);!!}
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">&nbsp</label>
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <i class="fas fa-search"> Search</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="bg-silken text-white">
                            <th> SL </th>
                            <th> No </th>
                            <th> Max Amount (Tk) </th>
                            <th> Assigned To </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = $sim->appends($req)->firstItem(); @endphp
                        @foreach($sim as $value)
                            <tr>
                                <td class="align-middle"> {{$sl++}} </td>
                                <td class="align-middle"> {{$value->msisdn}} </td>
                                <td class="align-middle"> {{$value->max_amount}} </td>
                                <td class="align-middle">
                                    @if( !$value->is_assigned)
                                        Not Assigned
                                    @else
                                        {{$value->assignedUser->name}}
                                    @endif
                                </td>
                                <td class="align-middle">
                                    <a href="javascript:void(0)" data-sim="{{$value}}" class="edit-btn">
                                        <button class="btn btn-info btn-sm">
                                            <i class="fas fa-pen"></i>
                                        </button>
                                    </a>
                                    @if( !$value->is_assigned)
                                        <a href="javascript:void(0)" data-num="{{$value}}" class="assign-btn" title="Assign to a user">
                                            <button class="btn btn-success btn-sm">
                                                <i class="fas fa-user-plus"></i>
                                            </button>
                                        </a>
                                    @else
                                        <a href="{{route('sim.unassign',$value->id)}}" class="btn-sm" title="Un assign this SIM">
                                            <button class="btn btn-warning btn-sm">
                                                <i class="fas fa-user-minus"></i>
                                            </button>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example" class="m-3">
                    <span>Showing {{ $sim->appends($req)->firstItem() }} to {{ $sim->appends($req)->lastItem() }} of {{ $sim  ->appends($req)->total() }} entries</span>
                    <div>{{ $sim->appends($req)->render() }}</div>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('modals.simManagement')
    @include('modals.simAssignManagement')
@endsection
@push('script')
    <script src="{{asset('/')}}assets/select2/select2.min.js"></script>
    <script>
        $(document).ready(() => {
            $('#assignModal').on('shown.bs.modal', function () {
                $('.select2').select2({
                    dropdownParent: $('#assignModal'),
                    width: '100%'
                })
            });
            // Destroy Select2 instance when the modal is hidden
            $('#assignModal').on('hidden.bs.modal', function () {
                $('.select2').each(function() {
                    if ($(this).data('select2')) {
                        $(this).select2('destroy');
                    }
                });
            });
        })
        $('#add-new-btn').click(function(){
            $('.add-btn').text('Add');
            $('#simManagementModalLabel').text('Add SIM');
            var url = "{{route('sim.store')}}";
            $('#simManagement_form').attr('action', url);
            $('#simManagement_form').find("input[type=text],input[type=textarea]").val("");
            $('#simManagement').modal('show')
        })
        $('.edit-btn').click(function(){
            $('.add-btn').text('Update');
            $('#simManagementModalLabel').text('Update SIM');
            var simManagement_data = $(this).data('sim');
            var url = window.location.pathname+'/'+simManagement_data.id+'/update';
            $('#simManagement_form').attr('action', url);
            $('#msisdn').val(simManagement_data.msisdn)
            $('#simManagement').modal('show')
        })
        $('.assign-btn').click(function(){
            var data = $(this).data('num');
            $("option:selected").prop("selected", false)
            $('#assignModalLabel').text('Assign SIM')
            $('#assign_msisdn').val(data.msisdn)
            $('#balance').val(data.max_amount)
            $("#assignModal").modal('show')
        })
    </script>
@endpush
