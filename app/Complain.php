<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $appends = ['Linked_complain_url'];
    public function registerBy()
    {
        return $this->belongsTo(User::class, 'register_by');
    }

    public function registerTo()
    {
        return $this->belongsTo(User::class, 'register_to');
    }

    public function linkedComplain()
    {
        return $this->belongsTo(Complain::class, 'link_complain_id', 'complain_id');
    }

    public function comments()
    {
        return $this->hasMany(ComplainComment::class, 'complain_id');
    }

    public function getStatusAttribute($value)
    {
        $statusList = statusList();
        return data_get($statusList, $value);
    }

    public function getImagesAttribute($value)
    {
        $images = [];
        foreach (json_decode($value) as $image){
            $images[] = asset('/').$image;
        }
        return $images;
    }

    public function getLinkedComplainUrlAttribute()
    {
        if($this->linkedComplain) {
            return route('complain.view', ['complain' => $this->linkedComplain]);
        } else {
            return null;
        }
    }
}
