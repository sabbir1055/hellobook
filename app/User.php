<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable,EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * An hub users to a role
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'id', 'id');
    }

    public function getDepartment(){
        return $this->belongsTo(Department::class,'department_id');
    }

    public function reportingTo(){
        return $this->belongsTo(User::class,'reporting_person_employee_id', 'employee_id');
    }

    public function getDesignation(){
        return $this->belongsTo(Designation::class,'designation','id');
    }

    public function assignedNumber(){
        return $this->hasOneThrough(Sim::class,UserSim::class,'user_id','id','id','sim_id')->orderBy('status','desc');
    }
}
