<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailLog extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function from(){
        return $this->belongsTo(User::class);
    }

    public function to(){
        return $this->belongsTo(User::class);
    }

    public function booking(){
        return $this->belongsTo(RoomBook::class);
    }
}
