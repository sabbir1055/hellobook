<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportTrip extends Model
{
    protected $table = 'transport_trip';
    protected $guarded = [];

    public function getUsers(){
        return $this->hasMany(TransportTripUser::class, 'trip_id', 'id');
    }
    public function getUserInfo(){
        return $this->hasManyThrough(User::class,TransportTripUser::class, 'trip_id', 'id','id','user_id');
    }
}
