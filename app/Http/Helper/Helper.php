<?php
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

function getUser($id){
    return User::find($id);
}

function getRoom($id){
    return \App\Room::findOrFail($id);
}

function show_date($value, $key) {
    echo $key, ': ', date('r', $value), PHP_EOL;
}

function allocateusers($ids){
    return User::whereIn('id', $ids)->get();
}

function generateQRCode(string $url, User $user)
{
    $filename = 'user-qr-code/'.$user->id.'_qrcode.png'; // Define the filename for the QR code image
    $qrCode = QrCode::size(300)->format('png')
        ->generate($url);

    File::put(public_path().'/uploads/'.$filename, $qrCode);

    return $filename;
}

function statusList()
{
    return [
        0 => 'Pending',
        1 => 'In Progress',
        2 => 'Withdraw',
        3 => 'Reject',
        4 => 'Done',
        5 => 'Complete'
    ];
}

function complainstatusColorList($status)
{
    $statusList =  [
        0 => 'badge-primary',
        1 => 'badge-secondary',
        2 => 'badge-danger',
        3 => 'badge-danger',
        4 => 'badge-info',
        5 => 'badge-success'
    ];
    return data_get($statusList, $status, 'badge-primary');
}

