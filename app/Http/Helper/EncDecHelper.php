<?php

namespace App\Http\Helper;

use Illuminate\Support\Str;

class EncDecHelper
{
    static function __encrypt($data)
    {
        if(!$data){
            return $data;
        }
        $pvtKey = trim(config('app.key'));
        $iv = Str::random(16);
        $encrypted = openssl_encrypt($data, "aes-256-cbc", $pvtKey, 0, $iv);
        return base64_encode($iv . '||' . $encrypted);
    }

    static function __decrypt($data)
    {
        if(!$data){
            return $data;
        }
        $pvtKey = trim(config('app.key'));
        $data = base64_decode($data);
        if(strpos($data, '||') !== 16){
            return null;
        }
        list($iv, $encrypted_data) = explode('||', $data, 2);
        return openssl_decrypt($encrypted_data, "aes-256-cbc", $pvtKey, 0, $iv);
    }
}
