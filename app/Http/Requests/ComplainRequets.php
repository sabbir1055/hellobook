<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ComplainRequets extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'estimated_date' => 'required|date|date_format:Y-m-d|after_or_equal:' .  Date('Y-m-d'),
            'images' => 'nullable|array|max:3',
            'images.*' => 'required|mimes:jpg,png,jpeg|max:3096',
            'link_complain_id' => 'nullable|exists:complains,complain_id',
            'register_to' => ['required', 'integer', Rule::exists('users', 'id')
                ->where('is_complain_solver', true)]
        ];
    }

    public function messages()
    {
        return [
            'images.max' => 'You can attach max 3 images at a time',
            'images.*.max' => 'Your image can be max size of 3MB'
        ];
    }
}
