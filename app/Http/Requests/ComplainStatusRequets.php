<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ComplainStatusRequets extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'complain_id' => 'required|integer',
            'status' => 'required|integer|in:1,2,3,4,5',
            'view_type' => 'required|in:registerBy,registerTo',
            'reason' => ['nullable', Rule::requiredIf(in_array($this->status, [2,3])), 'string'],
            'rating' => ['nullable', 'integer', 'max:5']
        ];
    }
}
