<?php

namespace App\Http\Controllers;

use App\Department;
use App\Designation;
use Illuminate\Http\Request;

class DesignationController extends Controller
{
    public function index(Request $request){
        $req = $request->all();
        $department = Department::pluck('name','id');
        $designation = Designation::paginate(10);
        return view('designation.index',compact('req','designation','department'));
    }

    public function store(Request $request){
        try{
            Designation::Create([
                    'name' => $request->name,
                    'short_name' => $request->short_name,
                    'department_id' => $request->department,
                    'status' => $request->status
                ]
            );
            session()->flash('successMsg','Designation added successfully!!!');
            return back();
        }catch (\Exception $e){
            session()->flash('errorMsg',$e->getMessage());
            // session()->flash('errorMsg','Designation failed to add!!!');
            return back();
        }
    }

    public function update(Request $request, $id){
        $designation = Designation::findOrFail($id);
        $designation->update([
            'name' => $request->name,
            'short_name' => $request->short_name,
            'department_id' => $request->department,
            'status' => $request->status
        ]);
        session()->flash('successMsg','Designation updated successfully!!!');
        return back();
    }
}
