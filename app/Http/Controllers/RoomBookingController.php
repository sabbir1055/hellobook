<?php

namespace App\Http\Controllers;

use App\EmailLog;
use App\Notification;
use App\Room;
use App\RoomBook;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoomBookingController extends Controller
{
    public function index(Request $request)
    {
        $req = $request->all();
        $room = Room::pluck('name', 'id');
        $fUsers = User::where('status', 1)->get();
        $users = $fUsers->Where('id', '!=', auth()->user()->id)
            ->pluck('name', 'id');
        $query = RoomBook::where('status', 1);
        ($request->filled('f_booking_date') ? $query->where('booked_date', $request->f_booking_date) : null);
        ($request->filled('f_booked_by') ? $query->where('booked_by', $request->f_booked_by) : null);
        ($request->filled('f_room') ? $query->where('room_id', $request->f_room) : null);
        $roomBook = $query->orderBy('booked_date', 'desc')->paginate(10);
        return view('roombook.index', compact('roomBook', 'req', 'room', 'users','fUsers'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'include.*' => 'required|exists:users,id',
            'room_no' => 'required|exists:room,id',
            'date' => 'required',
            'time_from' => ['bail','required',function($value,$attribute, $fail) use ($request){
                if(date('Y-m-d H:i', strtotime($request->date.' '.$request->time_from)) < date('Y-m-d H:i')){
                    $fail('please choose a proper time from filed');
                }
            }],
            'time_to' => ['bail','required',
                function ($attribute, $value, $fail) use ($request){
                    if (date('H:i', strtotime($value)) < date('H:i', strtotime($request->time_from))) {
                        $fail('Time must be higher then time from.');
                    }
                }
            ],
        ]);
        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $duplicateRoom = RoomBook::where('booked_date', date('Y-m-d', strtotime($request->date)))
            ->where('room_id', $request->room_no)
            ->where('booked_time_to','>=', date('H:i',strtotime('-5 minutes',strtotime($request->time_from))))
            ->where('booked_time_from','<=', date('H:i',strtotime('-5 minutes',strtotime($request->time_to))))
            ->where('status',1)
            ->first();

        if ($duplicateRoom) {
            Log::debug('duplicate Room ==> '. json_encode($duplicateRoom));
            session()->flash('errorMsg', 'This room is already booked by ' . $duplicateRoom->bookedBy->name . ' & Meeting  title is : ' . $duplicateRoom->title . ' which conflict with your time.');
            return back();
        }

        try {
            DB::beginTransaction();

            $room = RoomBook::create([
                'title' => $request->title,
                'booked_by' => auth()->user()->id,
                'booked_date' => date('Y-m-d', strtotime($request->date)),
                'booked_time_from' => date('H:i', strtotime($request->time_from)),
                'booked_time_to' => date('H:i', strtotime($request->time_to)),
                'allocate_with' => json_encode($request->include),
                'room_id' => $request->room_no,
                'vc' => $request->vc,
                'refreshment' => $request->refreshment,
                'projector' => $request->projector,
            ]);
            if ($request->filled('projector') && $request->projector) {
                $duplicateProjector = RoomBook::where('id','!=', $room->id)
                    ->where('booked_date', date('Y-m-d', strtotime($request->date)))
                    ->where('booked_time_to','>=', date('H:i',strtotime('-5 minutes',strtotime($request->time_from))))
                    ->where('booked_time_from','<=', date('H:i',strtotime('-5 minutes',strtotime($request->time_to))))
                    ->where('status',1)
                    ->where('projector',1)
                    ->first();
                if ($duplicateProjector) {
                    Log::debug('duplicate Projector ==> '. json_encode($duplicateProjector));
                    session()->flash('errorMsg', 'This Projector is already booked by ' . $duplicateProjector->bookedBy->name . ' & Meeting  title is : ' . $duplicateProjector->title . ' which conflict with your time.');
                    return back();
                }
                $this->__createMailLog(['to_email' => 'it@silkensewing.com', 'to_app' => 'IT Projector', 'subject' => 'Meeting Reminder!', 'from_id' => auth()->user()->id, 'booking_id' => $room->id, 'mail_status' => 1]);
            }

            if ($request->filled('vc') && $request->vc) {
                $zoom = Room::where('name', 'Zoom')->first()->id;
                $duplicateZoom = RoomBook::where('booked_date', date('Y-m-d', strtotime($request->date)))
                    ->where('room_id', $zoom)
                    ->where('booked_time_to','>=', date('H:i',strtotime('-5 minutes',strtotime($request->time_from))))
                    ->where('booked_time_from','<=', date('H:i',strtotime('-5 minutes',strtotime($request->time_to))))
                    ->where('status',1)
                    ->first();
                if ($duplicateZoom) {
                    Log::debug('duplicate Zoom ==> '. json_encode($duplicateZoom));
                    session()->flash('errorMsg', 'This Zoom room is already booked by ' . $duplicateZoom->bookedBy->name . ' & Meeting  title is : ' . $duplicateZoom->title . ' which conflict with your time.');
                    return back();
                }
                RoomBook::create([
                    'title' => $request->title,
                    'booked_by' => auth()->user()->id,
                    'booked_date' => date('Y-m-d', strtotime($request->date)),
                    'booked_time_from' => date('H:i', strtotime($request->time_from)),
                    'booked_time_to' => date('H:i', strtotime($request->time_to)),
                    'allocate_with' => json_encode($request->include),
                    'room_id' => $zoom
                ]);
                //  mail to it for zoom
                if(in_array($request->room_no,[2,4,6,11])){
                    $this->__createMailLog(['to_email' => 'it@silkensewing.com', 'to_app' => 'IT Zoom', 'subject' => 'Meeting Reminder!', 'from_id' => auth()->user()->id, 'booking_id' => $room->id, 'mail_status' => 1]);
                }
            }

            $ho_rooom = Room::where('room_no', 'like', '%(HO)%')->pluck('id')->toArray();
            //  mial to perticipants
            $users = User::whereIn('id', $request->include)->get();
            foreach ($users as $user_data) {
                if($user_data->email){
                    $this->__createMailLog(['to_email' => $user_data->email, 'to_app' => $user_data->name, 'subject' => 'Meeting Reminder!', 'from_id' => auth()->user()->id, 'to_id' => $user_data->id, 'booking_id' => $room->id, 'mail_status' => 1]);
                    Notification::create([
                        'from' => auth()->user()->id,
                        'to' => $user_data->id,
                        'body' => $room->bookedBy->name . ' called a meeting on ' . $room->roomData->name . ' at ' . $request->time_from . ', ' . $request->date . '. Please make sure your availability.',
                        'no_type' => 1
                    ]);
                }
            }
            Log::info('Is Head office ==> ' . in_array($request->room_no, $ho_rooom));
            // mail to it for zoom room
            if($request->room_no == 8){
                EmailLog::create(['to_email' => 'it@silkensewing.com', 'to_app' => 'IT Zoom', 'subject' => 'Meeting Reminder!', 'from_id' => auth()->user()->id, 'to_id' => $user_data->id, 'booking_id' => $room->id, 'mail_status' => 1]);
            }
            // Mail to front desk / reception
            if (in_array($request->room_no, $ho_rooom)) {
                $this->__createMailLog(['to_email' => 'frontdesk@silkensewing.com', 'to_app' => 'Front Desk', 'subject' => 'Meeting Reminder!', 'from_id' => auth()->user()->id, 'booking_id' => $room->id, 'mail_status' => 1]);
            } else {
                $this->__createMailLog(['to_email' => 'reception@silkensewing.com', 'to_app' => 'Reception', 'subject' => 'Meeting Reminder!', 'from_id' => auth()->user()->id, 'booking_id' => $room->id, 'mail_status' => 1]);
            }
            DB::commit();
            session()->flash('successMsg', 'Your room has been booked.');
            return back();
        } catch (\Exception $e) {
            Log::error('Room book failed. reason ==> '. $e->getMessage());
            DB::rollback();
            session()->flash('errorMsg', 'Something went wrong');
            return back();
        }
    }

    public function delete(Request $request, $id)
    {
        $room = RoomBook::findOrFail($id);
        // Meeting cancel info mail
        $users = User::whereIn('id', json_decode($room->allocate_with))->get();
        foreach ( $users as $user_data) {
            // Meeting info mail
            if($user_data->email){
                $this->__createMailLog(['to_email' => $user_data->email, 'to_app' => $user_data->name, 'subject' => 'Meeting Cancel!', 'from_id' => auth()->user()->id, 'to_id' => $user_data->id, 'booking_id' => $room->id, 'mail_status' => 0]);
            }
            Notification::create([
                'from' => auth()->user()->id,
                'to' => $user_data->id,
                'body' => 'Meeting canceled. Called by ' . $room->bookedBy->name . ' on ' . $room->roomData->name . ' at ' . $request->time_from . ', ' . $request->date . '. Sorry for this inconvenience.',
                'no_type' => 1
            ]);
        }
        $ho_rooom = Room::where('room_no', 'like', '%(HO)%')->pluck('id')->toArray();
        if (in_array($room->room_id, $ho_rooom)) {
            $this->__createMailLog(['to_email' => 'frontdesk@silkensewing.com', 'to_app' => 'Front Desk', 'subject' => 'Meeting Cancel!', 'from_id' => auth()->user()->id, 'booking_id' => $room->id, 'mail_status' => 0]);
        } else {
            $this->__createMailLog(['to_email' => 'reception@silkensewing.com', 'to_app' => 'Reception', 'subject' => 'Meeting Cancel!', 'from_id' => auth()->user()->id, 'booking_id' => $room->id, 'mail_status' => 0]);
        }

        // Cancel notification mail to IT
        if((in_array($room->room_id,[2,4,6,11]) and $room->vc) or $room->room_id == 8){
            $this->__createMailLog(['to_email' => 'it@silkensewing.com', 'to_app' => 'IT Zoom', 'subject' => 'Meeting Cancel!', 'from_id' => auth()->user()->id, 'booking_id' => $room->id, 'mail_status' => 0]);
        }

        $room->update(['status' => 0]); // disabled
        session()->flash('successMsg', 'Meeting canceled!!!');
        return back();
    }

    public function getAvailableRoom(Request $request)
    {
        $reqTime = array('start' => strtotime($request->time_from), 'end' => strtotime($request->time_to));
        $meetingTime = array('start' => strtotime('10:00 AM'), 'end' => strtotime('11:15 AM'));
        $isConflict = 0;
        //        if (($reqTime['start'] <= $meetingTime['end']) && ($reqTime['end'] >= $meetingTime['start'])) { // TODO :: NEED TO CHANGE AFTER RAMADAN
        //            $isConflict = 1;
        //        }
        $booked_room = [];
        if($request->time_from && $request->time_to){
            $booked_room = RoomBook::where('booked_date', date('Y-m-d', strtotime($request->date)))
                ->where('status',1)
                ->where('booked_time_to','>=', date('H:i',strtotime('-5 minutes',strtotime($request->time_from))))
                ->where('booked_time_from','<=', date('H:i',strtotime('-5 minutes',strtotime($request->time_to))))
                ->pluck('room_id')->toArray();
        }
        $room = Room::whereNotIn('id', $booked_room);
        if ($isConflict) {
            $room->where('id', '!=', 2);
        }
        $room = $room->get();

        return $room;
    }

    private function __createMailLog($data):void
    {
        EmailLog::create($data);
    }
}
