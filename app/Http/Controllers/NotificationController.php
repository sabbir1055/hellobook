<?php

namespace App\Http\Controllers;

use App\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    public function notification()
    {
        $html = null;
        $notification = Notification::select(DB::raw('users.image,users.name,notification.*'))
            ->where('to', auth()->user()->id)
            ->leftjoin('users', 'users.id', 'notification.from')
            ->where('notification.created_at', '>=', Carbon::now()->subDays(7)->format('Y-m-d 00:00:00'))
            ->orderBy('id', 'desc')
            ->get();
        if (count($notification)) {
            foreach ($notification as $data) {
                $notificationUrl = route('notification.detail', ['notification' => $data]);
                $image =  asset('/') . 'uploads/' . $data->image;
                $dateTime = date('d M,Y h:i A', strtotime($data->created_at));
                $html.="<div class='single_notify d-flex align-items-center ".( $data->status ? '' : 'unread-notification' )."'>
                                        <div class='notify_thumb'>
                                            <a href='#'><img src='$image' alt=''></a>
                                        </div>
                                        <div class='notify_content'>
                                            <a href='$notificationUrl'><h5>$data->name </h5></a>
                                            <p class='notification-body'>$data->body</p>
                                            <span class='date'>$dateTime</span>
                                        </div>
                                    </div>";
            }
        }
        return [
            'count' => $notification->where('status', 0)->count(),
            'html' => $html
        ];
    }

    public function readNotification(Request $request)
    {
        $notification = Notification::where('to', auth()->user()->id)->update([
            'status' => 1
        ]);
        session()->flash('successMsg', 'All notification marked as read');
        return back();
    }

    public function detail(Request $request, Notification $notification)
    {
        $notification->update([
           'status' => 1
        ]);

        if($notification->url == '#'){
            return back();
        } else {
            return redirect($notification->url);
        }
    }
}
