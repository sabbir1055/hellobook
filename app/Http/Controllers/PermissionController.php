<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index(Request $request){
        $req = $request->all();
        $permission = Permission::orderBy('name')->paginate(10);
        return view('permission.index',compact('permission','req'));
    }

    public function store(Request $request){
        Permission::firstOrCreate([
            'name' => $request->name
        ],[
            'name' => $request->name,
            'display_name' => $request->display_name,
            'description' => $request->description
        ]);

        session()->flash('successMsg','Permission successfully added');
        return back();
    }

    public function update(Request $request,$id){
        $permission = Permission::findOrFail($id);
        $permission->name = $request->name;
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->save();

        session()->flash('successMsg','Permission successfully updated');
        return back();
    }
}
