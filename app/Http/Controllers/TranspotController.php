<?php

namespace App\Http\Controllers;
use App\EmailLog;
use App\Transport;
use App\TransportationBooking;
use App\TransportTrip;
use App\TransportTripUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TranspotController extends Controller
{
    public function index(Request $request){
        $data['req'] = $request->all();
        $transports = Transport::query();

        $data['transports'] = $transports->paginate(10);

        return view('transport.index',$data);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'car_name' => 'required|max:255',
            'car_number' => 'required|unique:transports,id|max:45',
            'capacity' => 'nullable|numeric',
            'trip_count' => 'required_if:car_type,public_transport|numeric',
            'car_type' => 'required|in:public_transport,others_transport,office_transport',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }
        $transport = Transport::create($request->except('_token'));
        if(in_array($request->car_type, ['public_transport','office_transport'])){
            for($i = 0 ; $i < $request->trip_count ; $i++){
                TransportTrip::create([
                    'transport_id' => $transport->id,
                    'trip_no' => ($i+1)
                ]);
            }
        }
        session()->flash('successMsg','Transport added successfully');
        return back();
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'car_name' => 'required|max:255',
            'car_number' => 'required|max:45|unique:transports,id,'.$id,
            'capacity' => 'nullable|numeric',
            'allocate_users' => ['bail','nullable',function($attribute, $value, $fail) use($request) {
                if($request->car_type === 'public_transport'){
                    if($request->capacity < count($request->allocate_users)){
                        $fail('allocated users exceeded the limit');
                    }
                }
            }],
            'car_type' => 'required|in:public_transport,others_transport,office_transport',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }
        try {
            DB::beginTransaction();
            $transport = Transport::findOrfail($id);
            $transport_old = clone $transport;
            $transport->update($request->except('_token'));
            if($transport_old->trip_count > $transport->trip_count){
                for ($i = ($transport->trip_count+1); $i < $transport_old->trip_count; $i++){
                    TransportTrip::where([
                        'transport_id' => $transport->id,
                        'trip_no' => $i
                    ])->delete();
                }
            } elseif ($transport->trip_count > $transport_old->trip_count){
                for ($i = 1; $i <= $transport->trip_count; $i++){
                    TransportTrip::firstOrCreate([
                        'transport_id' => $transport->id,
                        'trip_no' => $i
                    ]);
                }
            }
            DB::commit();
            session()->flash('successMsg','Transport updated successfully');
            return back();
        }catch (\Exception $e){
            DB::rollback();
            session()->flash('errorMsg','Transport update failed');
            return back();
        }
    }
    public function requestIndex (Request $request){
        $req = $request->all();
        $transports = Transport::pluck('car_name','id');
        $bookTransport =TransportationBooking::where('booked_by', auth()->user()->id);
        ($request->transport_id ? $bookTransport->where('transport_id', $request->transport_id) : null);
        ($request->booked_date ? $bookTransport->where('booked_date', $request->booked_date) : null);
        $bookTransport = $bookTransport->orderBy('booked_date','desc')->paginate(10);
        return view('transportBook.index', compact('req','transports','bookTransport'));
    }
    public function requestApprovalIndex (Request $request){
        $req = $request->all();
        $transports = Transport::pluck('car_name','id');
        $bookTransport =TransportationBooking::query();
        ($request->transport_id ? $bookTransport->where('transport_id', $request->transport_id) : null);
        ($request->booked_date ? $bookTransport->where('booked_date', $request->booked_date) : null);
        $bookTransport = $bookTransport->orderBy('booked_date','desc')->paginate(10);

        return view('transportBook.approvalIndex', compact('req','transports','bookTransport'));
    }
    public function requestStore(Request $request){
        $validator = Validator::make($request->all(), [
            'visit_type' => 'required|in:office,outside_visit',
            'transport_id' => 'required|integer',
            'date' => 'required',
            'time_from' => ['bail','nullable','required_if:visit_type,outside_visit',
                function($value,$attribute, $fail) use ($request){
                    if(date('Y-m-d H:i', strtotime($request->date.' '.$request->time_from)) < date('Y-m-d H:i')){
                        $fail('please choose a proper time from filed');
                    }
                }],
            'time_to' => ['bail','nullable','required_if:visit_type,outside_visit',
                function ($attribute, $value, $fail) use ($request){
                    if (date('H:i', strtotime($value)) < date('H:i', strtotime($request->time_from))) {
                        $fail('Time must be higher then time from.');
                    }
                }
            ],
            'comment' => 'nullable|max:255',
            'trip_id' => 'required_if:visit_type,office',
        ]);

        if ($validator->fails())  return back()->withErrors($validator);

        $alreadyBooked = TransportationBooking::where('transport_id', $request->transport_id)
            ->where('booked_by', auth()->user()->id)
            ->where('booked_date', $request->date)
            ->where('status', '!=', 2)
            ->first();

        if($alreadyBooked){
            session()->flash('errorMsg','You already booked this transport');
            return back();
        }
        $transport = Transport::where('id', $request->transport_id)
            ->first();
        if(!$transport){
            session()->flash('errorMsg','This transport is not valid');
            return back();
        }
        if($request->visit_type === 'office'){
            if($transport->car_type !== 'office_transport'){
                session()->flash('errorMsg','You choosed an invalid transport');
                return back();
            }
            $trip = $transport->getTripList->where('id', $request->trip_id)->first();
            if(!$trip){
                session()->flash('errorMsg','Trip not found');
                return back();
            }
            $fixedUser = $trip->getUsers->count();
            $getBookedUsers = TransportationBooking::where('booked_date', $request->date)
                ->where('transport_id', $transport->id)
                ->where('trip_no', $trip->id)
                ->where('status', 1)
                ->count();

            $totalUser = $fixedUser + $getBookedUsers;
            if($totalUser >= $transport->capacity){
                session()->flash('errorMsg','You chosen trip is already full');
                return back();
            }
        } else if($request->visit_type === 'outside_visit'){
//            $reqTime = array('start' => strtotime($request->time_from), 'end' => strtotime($request->time_to));
//            $meetingTime = array('start' => strtotime('10:00 AM'), 'end' => strtotime('11:15 AM'));
//            $isConflict = 0;
//            if (($reqTime['start'] <= $meetingTime['end']) && ($reqTime['end'] >= $meetingTime['start'])) {
//                $isConflict = 1;
//            }
            $isTransportBooked = TransportationBooking::where('booked_date', date('Y-m-d', strtotime($request->date)))
                ->where('booked_time_to','>=', date('H:i',strtotime('-5 minutes',strtotime($request->time_from))))
                ->where('booked_time_from','<=', date('H:i',strtotime('-5 minutes',strtotime($request->time_to))))
                ->where('transport_id', $request->transport_id)
                ->where('status',1)
                ->first();
            if($isTransportBooked){
                session()->flash('errorMsg','This transport is already booked');
                return back();
            }
        }

        $booking = TransportationBooking::create([
            'visit_type' => $request->visit_type,
            'transport_id' => $request->transport_id,
            'title' => $request->title,
            'booked_by' => auth()->user()->id,
            'booked_date' => $request->date,
            'booked_time_from' => $request->visit_type === 'outside_visit' ? date('H:i', strtotime($request->time_from)) : null,
            'booked_time_to' => $request->visit_type === 'outside_visit' ? date('H:i', strtotime($request->time_to)) : null,
            'trip_no' => $request->visit_type === 'office' ? $request->trip_id : null,
            'comments' => $request->comment
        ]);
        //TODO:: Send Email
        $userToGetMail = $this->mailUser();
        \Log::info("GET TRANSPORT APPROVAL USER => ". json_encode($userToGetMail));
        $this->sendMailTRansportBooking($userToGetMail->email, $userToGetMail->name, auth()->user()->name, $booking, 0);
        session()->flash('successMsg','Transport booking added successfully');
        return back();
    }
    public function requestApprove(Request $request, $id){
        $bookingRequest = TransportationBooking::where('id', $id)
            ->where('status', 0)
            ->first();
        if(!$bookingRequest){
            session()->flash('errorMsg','Invalid booking choosed');
            return back();
        }
        if($bookingRequest->visit_type === 'office'){
            $trip = $bookingRequest->getrip;
            $fixedUser = $trip->getUsers->count();
            $getBookedUsers = TransportationBooking::where('booked_date', $bookingRequest->booked_date)
                ->where('trip_no', $trip->id)
                ->where('status', 1)
                ->count();
            $totalUser = $fixedUser + $getBookedUsers;
            if($totalUser >= $bookingRequest->transportDetail->capacity){
                session()->flash('errorMsg','You chosen trip is already full');
                return back();
            }
        } else if($request->visit_type === 'outside_visit'){
//            $reqTime = array('start' => strtotime($request->time_from), 'end' => strtotime($request->time_to));
//            $meetingTime = array('start' => strtotime('10:00 AM'), 'end' => strtotime('11:15 AM'));
//            $isConflict = 0;
//            if (($reqTime['start'] <= $meetingTime['end']) && ($reqTime['end'] >= $meetingTime['start'])) {
//                $isConflict = 1;
//            }
            $isTransportBooked = TransportationBooking::where('booked_date', date('Y-m-d', strtotime($request->date)))
                ->where('booked_time_to','>=', date('H:i',strtotime('-5 minutes',strtotime($request->time_from))))
                ->where('booked_time_from','<=', date('H:i',strtotime('-5 minutes',strtotime($request->time_to))))
                ->where('transport_id', $request->transport_id)
                ->where('status',1)
                ->first();
            if($isTransportBooked){
                session()->flash('errorMsg','This transport is already booked');
                return back();
            }
        }
        $bookingRequest->update([
            'status' => 1,
            'action_date' => date('Y-m-d'),
            'action_time' => date('Y-m-d H:i:s'),
            'action_by' => auth()->user()->id
        ]);
        $mailUser = $this->mailUser();
        $bookedBy = $bookingRequest->bookedBy;
        $this->sendMailTRansportBooking($bookedBy->email, $bookedBy->name, $mailUser->name, $bookingRequest, 1);
        session()->flash('successMsg','Transport booking approved successfully');
        return back();
    }
    public function requestReject(Request $request){
        $validator = Validator::make($request->all(), [
            'booking_id' => 'required|integer',
            'reject_reason' => 'required|string|max:255',
        ]);

        if ($validator->fails())  return back()->withErrors($validator);
        $bookingRequest = TransportationBooking::where('id', $request->booking_id)
            ->where('status', 0)
            ->first();
        if(!$bookingRequest){
            session()->flash('errorMsg','Invalid booking choosed');
            return back();
        }
        $bookingRequest->update([
            'status' => 2,
            'action_date' => date('Y-m-d'),
            'action_time' => date('Y-m-d H:i:s'),
            'action_by' => auth()->user()->id,
            'reject_reason' => $request->reject_reason
        ]);
        $mailUser = $this->mailUser();
        $bookedBy = $bookingRequest->bookedBy;
        $this->sendMailTRansportBooking($bookedBy->email, $bookedBy->name, $mailUser->name, $bookingRequest, 2);
        session()->flash('successMsg','Transport booking declined successfully');
        return back();
    }
    public function requestStatusWithdraw(Request $request, $id){
        $bookingRequest = TransportationBooking::where('id', $id)
            ->where('booked_by', auth()->user()->id)
            ->where('status', 0)
            ->first();
        if(!$bookingRequest){
            session()->flash('errorMsg','Invalid booking choosed');
            return back();
        }
        $bookingRequest->update([
            'status' => 3,
            'action_date' => date('Y-m-d'),
            'action_time' => date('Y-m-d H:i:s'),
            'action_by' => auth()->user()->id
        ]);
        $mailUser = $this->mailUser();
        $this->sendMailTRansportBooking($mailUser->email, $mailUser->name, auth()->user()->name, $bookingRequest, 3);
        session()->flash('successMsg','Transport booking withdraw successfully');
        return back();
    }
    public function getAvailableTransport(Request $request){
        if($request->visit_type === 'outside_visit'){
//            $reqTime = array('start' => strtotime($request->time_from), 'end' => strtotime($request->time_to));
//            $meetingTime = array('start' => strtotime('10:00 AM'), 'end' => strtotime('11:15 AM'));
            $isConflict = 0;
//            if (($reqTime['start'] <= $meetingTime['end']) && ($reqTime['end'] >= $meetingTime['start'])) {
//                $isConflict = 1;
//            }
            $booked_transport = [];

            if($request->time_from && $request->time_to){
                $booked_transport = TransportationBooking::where('booked_date', date('Y-m-d', strtotime($request->date)))
                    ->where('booked_time_to','>=', date('H:i',strtotime('-5 minutes',strtotime($request->time_from))))
                    ->where('booked_time_from','<=', date('H:i',strtotime('-5 minutes',strtotime($request->time_to))))
                    ->where('status',1)
                    ->pluck('transport_id')->toArray();
            }

            $transport = Transport::whereNotIn('id', $booked_transport);
            if ($isConflict) {
                $transport->where('id', '!=', 2);
            }
            $transport = $transport->get();
            $html = '<option value=""> Select a car </option>';
            foreach ($transport as $data) {
                $html .= '<option value="' . $data->id . '"> ' . $data->car_name . '('.$data->car_number.') </option>';
            }
        } else {
            $transport = Transport::where('car_type', 'office_transport')->get();
            $html = '<option value=""> Select a car </option>';
            foreach ($transport as $data) {
                $html .= '<option value="' . $data->id . '"> ' . $data->car_name . '('.$data->car_number.') </option>';
            }
        }
        return $html;
    }
    public function getAvailableTrip(Request $request){
        $html = '<option value=""> Select a trip </option>';
        if($request->visit_type === 'office'){
            $transport = Transport::where('car_type', 'office_transport')
                ->where('id', $request->transport_id)
                ->first();
            if(!$transport){
                return $html;
            }
            foreach ($transport->getTripList as $data) {
                $fixedUser = $data->getUsers->count();
                $getBookedUsers = TransportationBooking::where('booked_date', $request->date)
                    ->where('transport_id', $request->transport_id)
                    ->where('trip_no', $data->id)
                    ->where('status', 1)
                    ->count();
                $totalUser = $fixedUser + $getBookedUsers;
                $availableSeat = $transport->capacity - ($fixedUser + $getBookedUsers);
                if($totalUser < $transport->capacity){
                    $html .= "<option value=  $data->id> Trip $data->trip_no (Available: $availableSeat) </option>";
                }
            }
        }
        return $html;
    }
    public function tripList (Transport $transport){
        $data['users'] = User::pluck('name', 'id');
        $data['transport'] = $transport;
        return view('transport.trip.index', $data);
    }
    public function tripUpdate(Request $request, Transport $transport, TransportTrip $trip){
        $validator = Validator::make($request->all(), [
            'allocate_users' => ['bail','nullable',function($attribute, $value, $fail) use($transport, $request) {
                if($transport->car_type === 'public_transport'){
                    if($transport->capacity < count($request->allocate_users)){
                        $fail('allocated users exceeded the limit');
                    }
                }
            }]
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        TransportTripUser::where('trip_id', $trip->id)->delete();

        foreach ($request->allocate_users as $user){
            $trip->getUsers()->create([
                'user_id' => $user
            ]);
        }

        session()->flash('successMsg','Trip employee added');

        return back();
    }
    private function sendMailTRansportBooking($email, $name, $actorName, $booking, $status)
    {
        $subject = "Transport ".($booking->visit_type === 'office' ? "Seat" : null )." Requisition for Review";
        switch ($status){
            case 1:
                $subject = "Transport ".($booking->visit_type === 'office' ? "Seat" : null )." Requisition Approved";
                break;
            case 2:
                $subject = "Transport ".($booking->visit_type === 'office' ? "Seat" : null )." Requisition Declined";
                break;
            case 3:
                $subject = "Transport ".($booking->visit_type === 'office' ? "Seat" : null )." Requisition withdraw";
                break;
            default:
                break;
        }
        if(config('app.env') == 'production') {
            Mail::send('emails.transportRequisition', ['actorName' => $actorName,'booking' => $booking, 'status' => $status], function ($m) use($email, $name, $subject) {
                $m->from('hellobook@silkensewing.com', 'HelloBook');
                $m->to($email, $name)->subject('Transport Booking!')
                    ->subject($subject);
            });
        }
    }
    private function mailUser()
    {
        return User::with('roles.permissions')
            ->whereHas('roles.permissions', function ($q) {
                $q->where('name', 'approve_reject_transport_request');
            })->first();
    }
}
