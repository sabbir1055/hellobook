<?php

namespace App\Http\Controllers;

use App\Complain;
use App\Exports\AllComplainExport;
use App\Http\Requests\ComplainCommentRequets;
use App\Http\Requests\ComplainRequets;
use App\Http\Requests\ComplainStatusRequets;
use App\Http\Traits\FileProcessTrait;
use App\Notification;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Excel;

class ComplainManagementController extends Controller
{
    use FileProcessTrait;

    public function index(Request $request)
    {
        $request->merge([
            'f_registered_date' => $request->f_registered_date ?: (Carbon::now()->subDays(15)->format('Y-m-d')." TO ".Carbon::now()->format('Y-m-d'))
        ]);
        $registerDate = explode(' TO ',$request->f_registered_date);
        $req = $request->all();
        $statusList = statusList();
        $complains = Complain::with(['registerBy', 'registerTo', 'linkedComplain'])
            ->where('register_by', auth()->user()->id)
            ->whereBetween('registerd_date', [$registerDate[0]." 00:00:00", $registerDate[1]." 23:59:59"]);
            $request->filled('done_date') ? $complains->whereBetween('done_date', ["$request->done_date 00:00:00", "$request->done_date 23:59:59"]) : null;
        $request->filled('f_status') ? $complains->where('status', $request->f_status) : null;
        $complains = $complains->orderby('id', 'desc')->paginate(10);
        $viewType = 0;
        return view('complain.index', compact('complains', 'statusList', 'req', 'viewType'));
    }

    public function create()
    {
        $complainSolvers = User::join('department', 'users.department_id', 'department.id')
            ->where('is_complain_solver', true)
            ->where('status', 1)
            ->selectRaw("users.id, CONCAT(users.name,'(',department.name,')') name")
            ->get()
            ->pluck('name', 'id')
            ->toArray();
        return view('complain.create', compact('complainSolvers'));
    }

    public function view(Complain $complain)
    {
        $hasPermission = in_array(auth()->user()->id, [$complain->register_by, $complain->register_to]);
        $isSubordinateComplain = false;
        $user = auth()->user();
        if(auth()->user()->can('view_subordinate_complains')){
            $isSubordinateUsers = DB::select("WITH RECURSIVE  cte (id, name_en, employee_id) AS (
                                        SELECT `id`,`name`,`employee_id`
                                        FROM `users`
                                        WHERE `users`.`id` = $user->id
                                        UNION ALL
                                        SELECT   p.`id`,p.`name`,p.`employee_id`
                                        FROM `users` p
                                        INNER JOIN cte
                                        ON p.`reporting_person_employee_id` = cte.employee_id
                                        ) SELECT id FROM cte
                                        where cte.id = ?", [$complain->register_to]);
            $isSubordinateComplain = count($isSubordinateUsers);
        }
        $hasPermission = $hasPermission || auth()->user()->can('view_all_complains') || $isSubordinateComplain;
        if(!$hasPermission){
            session()->flash('errorMsg', 'You aren\'t permitted to comment');
            return back();
        }
        return view('complain.view', compact('complain'));
    }

    public function store(ComplainRequets $request)
    {
        $images = $request->hasFile('images') ? $this->processMultiple($request->file('images'), null, 'uploads/complain') : [];
        $complain = Complain::create([
            'complain_id' => time().auth()->user()->id,
            'register_by' => auth()->user()->id,
            'title' => $request->title,
            'description' => $request->description,
            'images' => json_encode($images),
            'register_to' => $request->register_to,
            'registerd_date' => now(),
            'estimated_date' => $request->estimated_date,
            'link_complain_id' => $request->link_complain_id
        ]);

        Notification::create([
            'to' => $request->register_to,
            'from' => auth()->user()->id,
            'body' => auth()->user()->name."  assigned a complain to you",
            'no_type' => 2,
            'status' => 0,
            'url' => route("complain.view", [$complain])
        ]);

        $requestToUser = User::with('reportingTo')
            ->where('id', $request->register_to)
            ->first();
        $requestToUserSupervisor = $requestToUser->reportingTo;
        // SEND MAIL TO SOLVER
        $isMailSend = $this->sendMailForComplain($requestToUser->email, $requestToUser->name, auth()->user()->name, $complain, 0);
        // SEND MAIL TO $request->register_to & his/her reporting
        if($requestToUserSupervisor){
            Notification::create([
                'to' => $requestToUserSupervisor->id,
                'from' => auth()->user()->id,
                'body' => auth()->user()->name."  assigned a complain to your department",
                'no_type' => 2,
                'status' => 0,
                'url' => route("complain.view", [$complain])
            ]);

            $isMailSend = $this->sendMailForComplain($requestToUserSupervisor->email, $requestToUserSupervisor->name, auth()->user()->name, $complain, 0, true);
        }
        if(!$isMailSend){
            session()->flash('successMsg', 'Complain registered successfully. But for any reason mail failed to send');
        } else {
            session()->flash('successMsg', 'Complain registered successfully');
        }
        return redirect()->route('complain.index');
    }

    public function statusUpdate(ComplainStatusRequets $request)
    {
        $complain = Complain::findOrFail($request->complain_id);
        if(!in_array(auth()->user()->id, [$complain->register_by, $complain->register_to])){
            session()->flash('errorMsg', 'You aren\'t permitted to comment');
            return back();
        }
        if(($request->status == 5 ) && $complain->getOriginal('status') !== 4 ){
            session()->flash('errorMsg', 'You can\'t change status as complete with out solver make it done');
            return back();
        }
        $isMailSend = true;
        if(auth()->user()->id === $complain->register_by && $request->view_type == 'registerBy'){
            if(in_array($request->status, [2,5])){
                $complain->update([
                    'status' => $request->status,
                    'withdraw_reason' =>  $request->reason,
                    'rating' => $request->status == 5 ? $request->rating : null
                ]);
                // SEND MAIL TO SOLVER
                $mailToUser = $complain->registerTo;
                $isMailSend = $this->sendMailForComplain($mailToUser->email, $mailToUser->name, auth()->user()->name, $complain, $request->status);
                Notification::create([
                    'to' => $complain->register_to,
                    'from' => $complain->register_by,
                    'body' => auth()->user()->name."  update complain status",
                    'no_type' => 2,
                    'status' => 0,
                    'url' => route("complain.view", [$complain])
                ]);
            } else {
                session()->flash('errorMsg', 'Invalid status chosen for update');
                return back();
            }
        } else if (auth()->user()->id === $complain->register_to){
            if(in_array($request->status, [1,3,4]) && $request->view_type == 'registerTo' && $complain->getOriginal('status') != 4){
                $complain->update([
                    'status' => $request->status,
                    'reject_reason' =>  $request->reason,
                    'done_date' => ($request->status == 4 ? now() : null),
                    'first_response_time' => (in_array($request->status, [1,3]) ? now() : null)
                ]);
                $mailToUser = $complain->registerBy;
                $isMailSend = $this->sendMailForComplain($mailToUser->email, $mailToUser->name, auth()->user()->name, $complain, $request->status);
                Notification::create([
                    'to' => $complain->register_by,
                    'from' => $complain->register_to,
                    'body' => auth()->user()->name."  update complain status",
                    'no_type' => 2,
                    'status' => 0,
                    'url' => route("complain.view", [$complain])
                ]);
            } else {
                session()->flash('errorMsg', 'Invalid status chosen for update');
                return back();
            }
        }
        if(!$isMailSend){
            session()->flash('successMsg', 'Complain status updated successfully. But for any reason mail failed to send');
        } else {
            session()->flash('successMsg', 'Complain status updated');
        }
        return back();
    }

    public function storeComment(ComplainCommentRequets $request, Complain $complain)
    {
        if(!in_array(auth()->user()->id, [$complain->register_by, $complain->register_to])){
            session()->flash('errorMsg', 'You aren\'t permitted to comment');
            return back();
        }
        $image = $request->hasFile('image') ? $this->processSingle($request->file('image'), null, 'uploads/comments') : null;
        $complain->comments()->create([
            'author_id' => auth()->user()->id,
            'comment' => $request->comment,
            'image' => $image
        ]);

        Notification::create([
            'to' => auth()->user()->id === $complain->register_by ? $complain->register_to : $complain->register_by,
            'from' => auth()->user()->id === $complain->register_to ? $complain->register_by : $complain->register_to,
            'body' => auth()->user()->name."  add a comment on complain",
            'no_type' => 2,
            'status' => 0,
            'url' => route("complain.view", [$complain])
        ]);
        return back();
    }
    public function solverIndex(Request $request)
    {
        $request->merge([
            'f_registered_date' => $request->f_registered_date ?: (Carbon::now()->subDays(15)->format('Y-m-d')." TO ".Carbon::now()->format('Y-m-d'))
        ]);
        $registerDate = explode(' TO ',$request->f_registered_date);
        $req = $request->all();
        $statusList = statusList();
        $complains = Complain::with(['registerBy', 'registerTo', 'linkedComplain'])
            ->where('register_to', auth()->user()->id)
            ->whereBetween('registerd_date', [$registerDate[0]." 00:00:00", $registerDate[1]." 23:59:59"]);
        $request->filled('done_date') ? $complains->whereBetween('done_date', ["$request->done_date 00:00:00", "$request->done_date 23:59:59"]) : null;
        $request->filled('f_status') ? $complains->where('status', $request->f_status) : null;
        $complains = $complains->orderby('id', 'desc')->paginate(10);
        $viewType = 1;
        return view('complain.index', compact('complains', 'statusList', 'req', 'viewType'));
    }

    public function allIndex(Request $request)
    {
        $request->merge([
            'f_registered_date' => $request->f_registered_date ?: (Carbon::now()->subDays(15)->format('Y-m-d')." TO ".Carbon::now()->format('Y-m-d'))
        ]);
        $registerDate = explode(' TO ',$request->f_registered_date);
        $req = $request->all();
        $statusList = statusList();
        $user = auth()->user();
        $complains = Complain::with(['registerBy', 'registerTo', 'linkedComplain'])
            ->whereBetween('registerd_date', [$registerDate[0]." 00:00:00", $registerDate[1]." 23:59:59"]);
        if(!auth()->user()->can('view_all_complains')){
            $complains->whereRaw("register_to in (WITH RECURSIVE  cte (id, name_en, employee_id) AS (
                                        SELECT `id`,`name`,`employee_id`
                                        FROM `users`
                                        WHERE `users`.`id` = $user->id
                                        UNION ALL
                                        SELECT   p.`id`,p.`name`,p.`employee_id`
                                        FROM `users` p
                                        INNER JOIN cte
                                        ON p.`reporting_person_employee_id` = cte.employee_id
                                        ) SELECT id FROM cte)"
            );
        }
        $request->filled('f_done_date') ? $complains->whereBetween('done_date', ["$request->f_done_date 00:00:00", "$request->f_done_date 23:59:59"]) : null;
        $request->filled('f_status') ? $complains->where('status', $request->f_status) : null;

        if($request->filled('download') && $request->download == 'yes'){
            if(auth()->user()->can('can_all_complain_download')) {
                // SUMMARY DATA
                $summaryData = clone $complains;
                $summaryData = $summaryData->selectRaw("
                    COUNT(1) total_complain,
                    SUM(CASE WHEN complains.status = 5 THEN 1 ELSE 0 END) resolved_complain,
                    SUM(CASE WHEN complains.status = 0 THEN 1 ELSE 0 END) pending_complain,
                    ROUND(SUM( CASE WHEN complains.status IN (4,5) THEN TIMESTAMPDIFF(MINUTE,complains.registerd_date, complains.done_date) / 1440 ELSE 0 END ) / SUM(CASE WHEN complains.status IN (4,5) THEN 1 ELSE 0 END), 2) avg_resolution_time,
                    SUM(CASE WHEN complains.done_date > complains.estimated_date THEN 1 ELSE 0 END) escalated_complain")
                    ->first(); // 1440 = 60 * 24
                // COMPLAIN BY CATEGORY
                $categoryData = clone $complains;
                $categoryData = $categoryData->join('users', 'complains.register_to', 'users.id')
                    ->join('department', 'users.department_id', 'department.id')
                    ->selectRaw('department.name complain_category, COUNT(1) total_complain,
                    SUM(CASE WHEN complains.status = 5 THEN 1 ELSE 0 END) resolved_complain,
                    SUM(CASE WHEN complains.status = 0 THEN 1 ELSE 0 END) pending_complain')
                    ->groupBy('department.id')
                    ->get();
                // OVERDUE COMPLAIN
                $overdueData = clone $complains;
                $overdueData = $overdueData->where(function($query) {
                    $query->where(function($query2) {
                        $query2->whereIn('status', [0, 1])
                            ->where('estimated_date', '<', date('Y-m-d'));
                    })->orWhere(function($query) {
                        $query->whereIn('status', [4, 5])
                            ->whereRaw('done_date > estimated_date');
                    });
                })->selectRaw('complains.complain_id, title,complains.registerd_date, complains.estimated_date, complains.done_date, complains.status')
                    ->get();
                // RESPONSE TIME COMPLAIN
                $responseTimeDataQuery = clone $complains;
                $responseTimeData = $responseTimeDataQuery->selectRaw("
                    SUM(CASE WHEN complains.status IN (1,3) THEN TIMESTAMPDIFF(MINUTE, complains.registerd_date, complains.first_response_time) ELSE 0 END) total_response_time,
                    SUM(CASE WHEN complains.status IN (1,3) THEN 1 ELSE 0 END) total_response_count,
                    SUM(CASE WHEN complains.status = 4 THEN TIMESTAMPDIFF(MINUTE, complains.registerd_date, complains.done_date) ELSE 0 END) total_done_time,
                    SUM(CASE WHEN complains.status = 4 THEN 1 ELSE 0 END) total_done_count
                ")->first();
//                return view('exports.complains.exportAllComplains', compact('summaryData', 'categoryData', 'overdueData','responseTimeData', 'req'));
                // FOR PDF
                $html = view('exports.complains.exportAllComplains', compact('summaryData', 'categoryData', 'overdueData','responseTimeData', 'req'))->render();
                $pdf = App::make('dompdf.wrapper');
                $pdf->loadHTML($html);
                return $pdf->download(time().'-all-complains.pdf');
                //FOR EXCEL
//                return Excel::download(new AllComplainExport($summaryData, $categoryData, $overdueData, $responseTimeData, $req), time().'-all-complains.xlsx');
            } else {
                return abort(403, 'You don\'t have permission to download' );
            }
        }

        $complains = $complains->orderby('id', 'desc')->paginate(10);
        $viewType = 2;
        return view('complain.index', compact('complains', 'statusList', 'req', 'viewType'));
    }
    private function sendMailForComplain($email, $name, $actorName, $complain, $status, $isSupervisor = false)
    {
        try{
            $subject = "New Complaint Assigned to You: Complaint ID $complain->complain_id";
            switch ($status){
                case 1:
                case 4:
                case 5:
                    $subject = "Status Update for Your Complaint: Complaint ID $complain->complain_id";
                    break;
                case 2:
                    $subject = "Complaint withdraw by Complainer: Complaint ID $complain->complain_id";
                    break;
                case 3:
                    $subject = "Complaint Rejected by Complainer: Complaint ID $complain->complain_id";
                    break;
                default:
                    break;
            }
            if(config('app.env') == 'production') {
                Mail::send('emails.complainRegistration', ['actorName' => $actorName, 'receiverName' => $name,'complain' => $complain, 'status' => $status, 'isSupervisor' => $isSupervisor], function ($m) use($email, $name, $subject) {
                    $m->from('hellobook@silkensewing.com', 'HelloBook');
                    $m->to($email, $name)->subject('Complain Register!!')
                        ->subject($subject);
                });
            }
            return true;
        }catch (\Exception $e){
            Log::error("MAIL SEND FOR COMPLAIN REGISTER");
            Log::error($e);
            return false;
        }
    }
}
