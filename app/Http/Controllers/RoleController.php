<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index(Request $request){
        $req = $request->all();
        $permission = Permission::pluck('display_name','id')->toArray();
        $role = Role::paginate(10);
        return view('role.index',compact('role','req','permission'));
    }

    public function store(Request $request){
        try{
            $role = Role::firstOrCreate([
                'name' => $request->name
            ],[
                'name' => $request->name,
                'display_name' => $request->display_name,
                'description' => $request->description
            ]);
            $role->attachPermissions($request->permission);
            session()->flash('successMsg','Role successfully added');
        }catch(\Exception $e){
            session()->flash('errorMsg','Role failed to add');
        }
        return back();
    }

    public function update(Request $request,$id){
        try{
            $role = Role::findOrFail($id);
            $role->name = $request->name;
            $role->display_name = $request->display_name;
            $role->description = $request->description;
            $role->save();
            $role->perms()->sync([]);
            $role->attachPermissions($request->permission);
            session()->flash('successMsg','Role successfully added');
        }catch(\Exception $e){
            session()->flash('errorMsg','Role failed to add');
        }
        return back();
    }
}
