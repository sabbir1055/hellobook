<?php

namespace App\Http\Controllers;

use App\Complain;
use App\Room;
use App\RoomBook;
use App\Sim;
use App\SimRequest;
use App\User;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashbarodController extends Controller
{
    public function index(Request $request){
        $data['room'] = Room::count();
        $data['past_total_meeting'] = RoomBook::whereBetween('booked_date',[Carbon::now()->subDay(6)->toDateString(),Carbon::now()->toDateString()])
            ->where('room_booking.status', 1)
            ->count();
        $data['coming_total_meeting'] = RoomBook::where('booked_date','>',Carbon::now()->toDateString())
            ->where('room_booking.status', 1)->count();
        $data['total_request'] = SimRequest::count();
        $data['total_complain_request'] = Complain::count();
        $data['user_meeting'] = User::select(DB::raw('users.*,count(room_booking.booked_by) as total'))
            ->join('room_booking','room_booking.booked_by','users.id')
            ->where('room_booking.status', 1)
            ->whereMonth('room_booking.created_at',Carbon::now()->month)
            ->whereYear('room_booking.created_at',Carbon::now()->year)
            ->groupBy('room_booking.booked_by')
            ->orderBy('total','desc')
            ->orderBy('created_at','desc')
            ->take(5)
            ->get();
        $data['total_sim'] = Sim::select( DB::raw('sum(case when is_assigned = 1 then 1 else 0 end) as assigned, sum(case when is_assigned = 0 then 1 else 0 end) as un_assigned'))->first();
        $data['total_meeting'] = RoomBook::where('booked_date','>=',Carbon::now()->subMonths(12)->toDateString())
            ->where('status', 1)
            ->get();
        $meting_call = [];
        foreach($data['total_meeting'] as $meeting){
            array_push($meting_call,[
                'id' => $meeting->roomData->name,
                'title' => $meeting->title,
                'end' => Carbon::parse($meeting->booked_date.' '.date('H:i:s',strtotime($meeting->booked_time_from)))->format('Y-m-d\TH:i:s.uP T'),
                'start' => $meeting->booked_date."T".date('H:i:s',strtotime($meeting->booked_time_from))
            ]);
        }

        $data['total_meeting'] = json_encode($meting_call);

        $data['meeting_man_hour'] = RoomBook::whereBetween('booked_date',[Carbon::now()->subDays(30)->toDateString(),Carbon::now()->toDateString()])
            ->where('status', 1)
            ->selectRaw("booked_date day, ROUND(SUM((TIME_TO_SEC(TIMEDIFF(room_booking.booked_time_to, room_booking.booked_time_from))/3600) * (LENGTH(`allocate_with`) - LENGTH(REPLACE(`allocate_with`, ',', '')) + 1)), 0) manhour")
            ->groupBy("booked_date")
            ->get();

        $data['complain_status_ratio'] = Complain::selectRaw("
                    SUM(CASE WHEN complains.status = 5 THEN 1 ELSE 0 END) resolved_complain,
                    SUM(CASE WHEN complains.status = 0 THEN 1 ELSE 0 END) pending_complain,
                    SUM(CASE WHEN complains.done_date > complains.estimated_date THEN 1 ELSE 0 END) escalated_complain")
            ->first();
        return view('dashboard',$data);
    }
}
