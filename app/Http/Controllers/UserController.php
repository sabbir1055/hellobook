<?php

namespace App\Http\Controllers;

use App\Department;
use App\Designation;
use App\Http\Helper\EncDecHelper;
use App\Notification;
use App\Role;
use App\Sim;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Excel;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\UserSim;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request):object
    {
        $req = $request->all();
        $role = Role::query();
        if(!in_array(auth()->user()->roles->first()->name, ['admin', 'super_admin'])){
            $role->whereNotIn('name', ['admin', 'super_admin']);
        }
        $role = $role->pluck('display_name', 'id')->toArray();
        $designation = Designation::pluck('name', 'id')->toArray();
        $department = Department::pluck('name', 'id')->toArray();
        $users = User::with(['reportingTo','getDepartment','getDesignation']);
        $request->filled('f_name') ? $users->where('name', 'like', '%' . $request->f_name . '%') : null;
        $request->filled('f_msisdn') ? $users->where('msisdn', 'like', '%' . $request->f_msisdn . '%') : null;
        $request->filled('f_designation') ? $users->where('designation', $request->f_designation) : null;
        $request->filled('f_department') ? $users->where('department_id', $request->f_department) : null;
        $request->filled('f_status') ? $users->where('status', $request->f_status) : null;
        $request->filled('f_is_complain_solver') ? $users->where('is_complain_solver', $request->f_is_complain_solver) : null;
        $users = $users->orderBy('id','asc')->paginate(10);
        return view('user.index', compact('users', 'req', 'role', 'designation', 'department'));
    }

    public function store(Request $request):object
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'msisdn' => ['nullable', 'unique:sim,msisdn', 'regex:^(?:\+?+88)?01[345-9]\d{8}$^'],
            'username' => 'required|max:20|unique:users,username',
            'employee_id' => 'required|max:20|unique:users,employee_id',
            'email' => 'nullable|unique:users,email',
            'role' => 'required',
            'designation' => 'required',
            'image' => 'nullable|mimes:jpeg,jpg,png,webp',
            'linkedin_profile' => 'nullable|url',
            'reporting_person_employee_id' => 'nullable|string|exists:users,employee_id'
        ], [
            'msisdn.regex' => 'mobile number invalid'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            DB::beginTransaction();
            $image = 'user.png';
            if ($request->hasFile('image')) {
                $avatar = $request->file('image');
                $name = time() . rand(11111, 99999);
                $image =  $name . '.'  . $avatar->getClientOriginalExtension();
                move_uploaded_file($_FILES['image']['tmp_name'], public_path('/uploads/' . $image));
            }
            $user = User::create([
                'name' => $request->name,
                'username' => $request->username,
                'employee_id' => $request->employee_id,
                'email' => $request->email,
                'designation' => $request->designation,
                'image' => $image,
                'status' => $request->status,
                'department_id' => $request->department,
                'msisdn' => '+880' . substr($request->msisdn, -10),
                'linkedin_profile' => $request->linkedin_profile,
                'reporting_person_employee_id' => $request->reporting_person_employee_id,
                'password' => bcrypt('12345678')
            ]);
            $user->attachRole($request->role);
            DB::commit();
            session()->flash('successMsg', 'User successfully added. Default password set to 12345678 for this user.');
            return back();
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('errorMsg', 'User failed to add.');
            return back();
        }
    }

    public function update(Request $request, $id):object
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'nullable|unique:users,email,' . $id,
            'msisdn' => 'nullable|regex:^(?:\+?+88)?01[345-9]\d{8}$^||unique:users,msisdn,'. $id,
            'username' => 'required|max:20|unique:users,username,' . $id,
            'employee_id' => 'required|max:20|unique:users,employee_id,' . $id,
            'role' => 'required',
            'designation' => 'required',
            'image' => 'nullable|mimes:jpeg,jpg,png,webp',
            'linkedin_profile' => 'nullable|url',
            'reporting_person_employee_id' => 'nullable|string|exists:users,employee_id'
        ], [
            'msisdn.regex' => 'mobile number invalid',
            'msisdn.unique' => 'mobile number already taken',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $user = User::findOrfail($id);
            $image = $user->image;
            if ($request->hasFile('image')) {
                $avatar = $request->file('image');
                $name = time() . rand(11111, 99999);
                $image =  $name . '.'  . $avatar->getClientOriginalExtension();
                move_uploaded_file($_FILES['image']['tmp_name'], public_path('/uploads/' . $image));
            }
            $user->update([
                'name' => $request->name,
                'username' => $request->username,
                'employee_id' => $request->employee_id,
                'email' => $request->email,
                'designation' => $request->designation,
                'department_id' => $request->department,
                'msisdn' => '+880' . substr($request->msisdn, -10),
                'image' => $image,
                'linkedin_profile' => $request->linkedin_profile,
                'reporting_person_employee_id' => $request->reporting_person_employee_id,
                'status' => $request->status
            ]);
            $user->roles()->sync((array)$request->role);
            session()->flash('successMsg', 'User successfully updated.');
            return back();
        } catch (\Exception $e) {
            session()->flash('errorMsg', 'User failed to update.');
            return back();
        }
    }

    public function profile(Request $request):object
    {
        return view('profile');
    }

    public function profileUpdate(Request $request):object
    {
        $validator = Validator::make($request->all(), [
            'image' => 'nullable|mimes:jpeg,jpg,png,webp',
            'old_password' => 'nullable|min:8',
            'password' => 'nullable|min:8',
            'password_confirmation' => 'nullable|same:password',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = auth()->user();
        $image = $user->image;
        if ($request->hasFile('image')) {
            $avatar = $request->file('image');
            $name = time() . rand(11111, 99999);
            $image =  $name . '.'  . $avatar->getClientOriginalExtension();
            move_uploaded_file($_FILES['image']['tmp_name'], public_path('/uploads/' . $image));
            $user->update([
                'image' => $image
            ]);
            session()->flash('successMsg', 'Image updated.');
            return back();
        }
        if ($request->filled('password')) {
            if (Hash::check($request->old_password, $user->password)) {
                $user->update([
                    'password' => bcrypt($request->password)
                ]);
                session()->flash('successMsg', 'Password updated.');
                return back();
            } else {
                session()->flash('errorMsg', 'Invalid old password.');
                return back();
            }
        }
    }

    public function addBulk(Request $request)
    {
        return view('user.bulk');
    }

    public function storeBulk(Request $request)
    {
        $data = null;
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:xlsx'
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator->errors())
                ->withInput();
        } else {
            try {
                $un_reg = [];
                $file = $request->file('file');
                $file_name = time() . rand(1111, 9999) . '.' . $file->getClientOriginalExtension();
                $file->move('uploads', $file_name);
                $result = $this->admin_import('uploads/' . $file_name);
                if ($result->status == 200) {
                    session()->flash('successMsg', $result->message);
                    return back();
                } else {
                    session()->flash('errorMsg', $result->message);
                    return back();
                }
            } catch (\Exception $e) {
                session()->flash('errorMsg', $e->getMessage());
                return back();
            }
        }
    }

    private function admin_import($file_name)
    {
        try {
            $result  = Excel::load($file_name, function ($reader) {
                $reader->all();
            })->get();
            $i = 0;
            $j = 0;
            foreach ($result as $value) {
                if (empty($value->username && $value->employee_id)) {
                    $j++;
                    continue;
                }
                if ($value->username && $value->name && $value->employee_id) {
                    $department = Department::where('name', $value->department)->first();
                    if (!$department) {
                        return (object)[
                            'status' => 500,
                            'message' => 'Users failed to add in list.'.$value->department .' department not found.'
                        ];
                    }
                    $designation = Designation::where('department_id', $department->id)
                        ->where('name', $value->designation)
                        ->first();
                    if (!$designation) {
                        return (object)[
                            'status' => 500,
                            'message' => 'Users failed to add in list.'.$value->designation .' designation not found under '.$value->department. ' department'
                        ];
                    }
                    $user = User::where([
                        'username' => $value->username,
                        'employee_id' => $value->employee_id
                    ])->first();
                    if (!$user) {
                        $user = User::Create([
                            'name' => $value->name,
                            'username' => $value->username,
                            'email' => $value->email,
                            'employee_id' => $value->employee_id,
                            'msisdn' => $value->msisdn ? '+880' . substr(str_replace('-', '', $value->msisdn), -10) : null,
                            'department_id' => $department->id,
                            'designation' => $designation->id,
                            'password' => bcrypt('12345678')
                        ]);
                        $user->roles()->sync((array)3);
                        // $user->attachRole(3);
                        if ($value->msisdn) {
                            $sim = Sim::firstOrCreate([
                                'msisdn' => '+880' . substr(str_replace('-', '', $value->msisdn), -10),
                                'max_amount' => $value->limit,
                                'is_assigned' => 1
                            ]);
                            UserSim::create([
                                'user_id' => $user->id,
                                'sim_id' => $sim->id,
                                'status' => 1
                            ]);
                        }
                        $i++;
                    }
                }
            }
            return (object)[
                'status' => 200,
                'message' => 'Successfully file uploaded! Total '.$i.' new user added and '.$j.' invalid user entered.'
            ];
        } catch (\Exception $e) {
            return (object)[
                'status' => 500,
                'message' => 'Users failed to add in list'
            ];
        }
    }

    public function generateQr(Request $request, User $user)
    {
        $encValue = EncDecHelper::__encrypt(json_encode($user->only(['id', 'name', 'email'])));
        $url =  app()->make('url')->to('/user-info').'/'.$encValue;
        $qr = generateQRCode($url, $user);
        $user->update([
            'qr_image' => $qr,
            'qr_string' => $encValue
        ]);
        session()->flash('successMsg', 'QR generated successfuly');
        return back();
    }

    public function userInfo(Request $request, $data)
    {
        $decryptedData = EncDecHelper::__decrypt($data);
        if(!$decryptedData || !isset($decryptedData)){
            abort(403);
        }
        $user = User::find(json_decode($decryptedData)->id);
        return view('user.profileInfo', compact('user'));
    }

    public function complainSolverStatusToggle(Request $request, User $user)
    {
        $user->update([
            'is_complain_solver' => !$user->is_complain_solver
        ]);
        session()->flash('successMsg', 'Complain solver status updated successfully');
        return back();
    }
}
