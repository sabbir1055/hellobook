<?php

namespace App\Http\Controllers;

use App\Sim;
use App\SimRequest;
use App\User;
use App\UserSim;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class SimManagementController extends Controller
{
    public function index(Request $request)
    {
        $req = $request->all();
        $unused_sim = Sim::whereNotIn('id', UserSim::pluck('sim_id')->toArray())->get();
        $sim = Sim::with('assignedUser');
        ($request->f_msisdn ? $sim->where('msisdn', 'like', '%' . $request->f_msisdn . '%') : null);
        ($request->f_status !== null ? $sim->where('is_assigned', $request->f_status) : null);
        $sim = $sim->orderBy('id', 'desc')->paginate(10);
        $users = User::whereNotIn('id', UserSim::pluck('user_id')->toArray())->pluck('name', 'id')->toArray();
        return view('sim-management.index', compact('unused_sim', 'sim', 'req', 'users'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'msisdn' => ['required', 'unique:sim,msisdn', 'regex:^(?:\+?+88)?01[345-9]\d{8}$^'],
        ], [
            'msisdn' => 'This mobile number already added.',
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        Sim::create([
            'msisdn' => '+880' . substr($request->msisdn, -10),
            'max_amount' => 0
        ]);

        session()->flash('successMsg', 'Sim successfully added');
        return back();
    }

    public function update(Request $request, $id)
    {
        Sim::findOrFail($id)->update([
            'msisdn' => '+880' . substr($request->msisdn, -10)
        ]);

        session()->flash('successMsg', 'Sim successfully updated');
        return back();
    }

    public function assign(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'assign_msisdn' => [
                'required', 'regex:^(?:\+?+88)?01[345-9]\d{8}$^',
                Rule::exists('sim', 'msisdn')->where(function ($query) {
                    return $query->where('is_assigned', 0);
                })
            ],
            'user_id' => ['required', 'exists:users,id', function ($attribute, $value, $fail) {
                return User::find($value)->assignedNumber ? $fail('User already assigned a sim.') : true;
            }],
            'balance' => 'required|numeric'
        ], [
            'assign_msisdn' => 'This mobile number already assigned.',
            'user_id' => 'This user already assigned.'
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $sim = Sim::where('msisdn', $request->assign_msisdn)->firstOrFail();
        try {
            DB::beginTransaction();
            UserSim::where('sim_id', $sim->id)->delete();
            $sim->is_assigned = 1;
            $sim->save();
            UserSim::create([
                'user_id' => $request->user_id,
                'sim_id' => $sim->id,
            ]);
            $this->sendMailSim(User::find($request->user_id), $request->assign_msisdn, $request->balance, 1);
            session()->flash('successMsg', 'Sim successfully assigned');
            DB::commit();
            return back();
        } catch (\Exception $e) {
            session()->flash('errorMsg', 'Sim failed to assign');
            return back();
        }
    }

    public function unAssign($id)
    {
        try {
            DB::beginTransaction();
            $sim = Sim::findOrFail($id);
            $sim->is_assigned = 0;
            $sim->save();
            $sim_user = UserSim::where('sim_id', $sim->id)->where('status', 1)->first();
            UserSim::where('sim_id', $sim->id)->where('status', 1)->update([
                'status' => 0,
                'unassigned_date' => date('Y-m-d H:i:s')
            ]);
            $this->sendMailSim(User::find($sim_user->user_id), $sim->msisdn, 0, 0);
            session()->flash('successMsg', 'Sim successfully unAssigned');
            DB::commit();
            return back();
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('errorMsg', 'Sim failed to unAssign');
            return back();
        }
    }

    public function request(Request $request)
    {
        $req = $request->all();
        $sim_request = SimRequest::where('request_by', auth()->user()->id)->orderBy('id', 'desc')->paginate(10);
        $users = User::where('status', 1)->pluck('name', 'id')->toArray();
        return view('sim-request.index', compact('sim_request', 'req', 'users'));
    }

    public function requestView(Request $request, $id)
    {
        $sim_request = SimRequest::findOrFail($id);
        return view('sim-request.view', compact('sim_request'));
    }

    public function requestActionView(Request $request, $id)
    {
        $sim_request = SimRequest::findOrFail($id);
        $un_assgned_sim = Sim::where('is_assigned', 0)->pluck('msisdn', 'msisdn')->toArray();
        $assgned_sim = User::find($sim_request->user_id)->assignedNumber()->where('is_assigned',1)->first();
        return view('sim-request.action_view', compact('sim_request', 'un_assgned_sim','assgned_sim'));
    }

    public function requestStore(Request $request)
    {
        $image = null;
        try {
            if (isset($request->file) && $request->file('file')) {
                $avatar = $request->file('file');
                $name = time() . rand(11111, 99999);
                $image =  $name . '.'  . $avatar->getClientOriginalExtension();
                move_uploaded_file($_FILES['file']['tmp_name'], public_path('/uploads/' . $image));
            }
            SimRequest::create([
                'user_id' => $request->user_id,
                'reason' => $request->reason,
                'subject' => $request->subject,
                'priority' => $request->priority,
                'limit' => $request->limit,
                'request_by' => auth()->user()->id,
                'attachnent' => $image
            ]);
            $user = User::with('getDesignation', 'getDepartment')->Find($request->user_id);
            // $email_user = User::whereHas('roles', function ($q) {
            //     $q->whereHas('permissions', function ($query) {
            //         $query->where('name', 'get_sim_email');
            // });
            // })->first();
            // Kamrul Hassan Kacon <kamrul.hassan@silkensewing.com>
            
            Mail::send('emails.simAssignHR', ['user' => $user, 'msisdn'], function ($m) {
                $m->from('hellobook@silkensewing.com', 'HelloBook');
                $m->to("kamrul.hassan@silkensewing.com", "Kamrul Hassan Kacon")->subject('Sim Card Reminder!');
                //            $m->to('sabbirchowdury000@gmail.com', 'Sabbir')->subject('Sim Card Reminder!');
            });

            session()->flash('successMsg', 'Sim request successfully sent');
        } catch (\Exception $e) {
            Log::error('Request error ===> ' . json_encode($e->getMessage()));
            session()->flash('errorMsg', 'Sim request failed to sent. Please contact with administrator.');
        }
        return back();
    }

    public function requestupdate(Request $request, $id)
    {
        $sim_request = SimRequest::findOrFail($id);
        $image = $sim_request->attachnent;
        if (isset($request->file) && $request->file('file')) {
            $avatar = $request->file('file');
            $name = time() . rand(11111, 99999);
            $image =  $name . '.'  . $avatar->getClientOriginalExtension();
            move_uploaded_file($_FILES['file']['tmp_name'], public_path('/uploads/' . $image));
        }
        $sim_request->update([
            'reason' => $request->reason,
            'subject' => $request->subject,
            'priority' => $request->priority,
            'attachnent' => $image,
            'limit' => $request->limit
        ]);
        session()->flash('successMsg', 'Sim request successfully updated');
        return back();
    }

    public function requestList(Request $request)
    {
        $req = $request->all();
        $sim_request = SimRequest::orderBy('id', 'desc')->paginate(10);
        return view('sim-request.list', compact('sim_request', 'req'));
    }

    public function requestAccept(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'assign_msisdn' =>'required', 'regex:^(?:\+?88)?01[345-9]\d{8}$^|exists:sim,msisdn',
            'user_id' => 'required|exists:users,id',
            'req_amount' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $sim = Sim::where('msisdn', $request->assign_msisdn)->firstOrFail();
        $user = User::findOrFail($request->user_id);
        try {
            DB::beginTransaction();
            $sim->max_amount = $request->req_amount;
            $sim->save();
            $sim_request = SimRequest::findOrFail($id);
            $assgned_sim = User::find($sim_request->user_id)->assignedNumber()->where('is_assigned',1)->first();
            if(!$assgned_sim){
                // $user_sim = UserSim::where('user_id', $request->user_id)->where('status', 1)->first();
                // if ($user_sim) {
                //     Sim::FindOrFail($user_sim->sim_id)->update([
                //         'is_assigned' => 0
                //     ]);
                // }

                // UserSim::where('user_id', $request->user_id)->where('status', 1)->update([
                //     'status' => 0,
                //     'unassigned_date' => date('Y-m-d H:i:s')
                // ]);

                $sim->is_assigned = 1;
                $sim->save();

                UserSim::Create([
                    'user_id' => $request->user_id,
                    'sim_id' => $sim->id,
                ]);
                $user->update([
                    'msisdn' => $request->assign_msisdn
                ]);
            }
            $sim_request->update([
                'accept_by' => auth()->user()->id,
                'is_accepted' => 1,
                'limit' => $request->req_amount,
                'approve_date' => date('Y-m-d H:i:s')
            ]);
            $this->sendMailSim(User::find($request->user_id), $request->assign_msisdn, $request->req_amount, 1);
            session()->flash('successMsg', 'Sim successfully assigned');
            DB::commit();
            return redirect()->route('sim.request.list');
        } catch (\Exception $e) {
            session()->flash('errorMsg', 'Sim failed to assign or user already assigned a sim');
            return back();
        }
    }

    public function requestDecline(Request $request, $id)
    {
        SimRequest::findOrFail($id)->update([
            'accept_by' => auth()->user()->id,
            'is_accepted' => 2,
            'decline_reason' => $request->reason
        ]);
        session()->flash('successMsg', 'Sim successfully assigned');
        return back();
    }

    public function report(Request $request, $id)
    {
        $req = $request->all();
        $sim = Sim::with('assignedUser')->where('is_assigned', $id)->paginate(10);
        return view('report.index', compact('sim', 'req', 'id'));
    }

    private function sendMailSim($user, $msisdn, $balance, $status)
    {
        if(config('app.env') == 'production') {
            Mail::send('emails.simAssign', ['user' => $user, 'msisdn' => $msisdn, 'balance' => $balance, 'status' => $status], function ($m) {
                $m->from('hellobook@silkensewing.com', 'HelloBook');
                $m->to('mamun.basher@silkensewing.com', 'Md. Mamun Al Basher')->subject('Sim Card Reminder!');
                //            $m->to('sabbirchowdury000@gmail.com', 'Sabbir')->subject('Sim Card Reminder!');
            });
        }
    }
}
