<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use Illuminate\Http\Request;
use Validator;
use App\User;
use Hash;
use Str;
use Log;

class LoginController extends Controller
{

    use ApiResponseTrait;

    public function login(Request $request){
        Log::info("login hited");
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,username',
            'password' => 'required|min:8'
        ],
        [
            'password.required' => 'Password field is required',
            'password.min' => 'Password minimum length 8',
        ]);

        if ($validator->fails()) return $this->set_response(null, 422, 'error', $validator->errors()->all());
        $user = User::where('username', $request->email)->first();
        if( Hash::check($request->password,$user->password)){
            $token = Str::random(60) . $user->email;
            $user->api_token = $token;
            $user->save();
            $roles = $user->roles->first();
            $permissions = $roles->permissions->pluck('name');
            $data['user'] = [
                'id' => $user->id,
                'name' => $user->name,
                "username"=> $user->usernmae,
                "email"=>  $user->email,
                "image"=> asset('/uploads').'/'.$user->image,
                "msisdn"=> $user->msisdn,
                "designation"=> $user->getDesignation->name,
                "department"=> $user->getDepartment->name,
                "api_token"=> $user->api_token,
            ];
            $data['roles'] = $roles->name;
            $data['permissions'] = $permissions; 
            return $this->set_response($data, 200, 'success', ['successfully logged in']);
        } else {
            return $this->set_response(null, 422, 'error', ['invalid username or password']);
        }
    }

    public function logout(Request $request){
        $user = auth()->user();
        $user->api_token = null;
        $user->save();
        return $this->set_response(null, 200, 'success', ['successfully logged out']);
    }
}
