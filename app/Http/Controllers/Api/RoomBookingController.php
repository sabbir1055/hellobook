<?php

namespace App\Http\Controllers\Api;

use App\Notification;
use App\Room;
use App\RoomBook;
use App\User;
use Illuminate\Support\Facades\Mail;
use Validator;
use DB;
use Log;
use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use Illuminate\Http\Request;

class RoomBookingController extends Controller
{

    use ApiResponseTrait;

    public function index(Request $request)
    {
        $roomBook = RoomBook::orderBy('booked_date', 'desc')
            ->where('created_at', '>', date('Y-m-d H:i:s', strtotime('-15 days')))
            ->paginate(7);
        $data = $roomBook->setCollection(
            $roomBook->getCollection()->map(function ($data) {
                return [
                    "id" => $data->id,
                    "booked_by_id" => $data->bookedBy->id,
                    "room" => $data->roomData->name,
                    "title" => $data->title,
                    "booked_by" => $data->bookedBy->name,
                    "booked_date" => $data->booked_date,
                    "booked_time_from" => $data->booked_time_from,
                    "booked_time_to" => $data->booked_time_to,
                    "allocate_with" => $this->getAllocatedPersonsList($data->allocate_with),
                    "status" => $data->status,
                    "created_at" => date('Y-m-d h:i A', strtotime($data->created_at)),
                    "vc" => $data->vc,
                    "refreshment" => $data->refreshment,
                    "projector" => $data->projector
                ];
            })
        );
        return $this->set_response([
            "paginator" => [
                "current_page" =>  $data->currentPage(),
                "total_pages" =>  $data->lastPage(),
                "previous_page_url" =>  null,
                "next_page_url" =>  null,
                "record_per_page" => $data->perPage(),
                "pagination_last_page" => $data->lastPage()
            ],
            'data' => $data->items()
        ], 200, 'success', ['Booked rooms']);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'include.*' => 'required|exists:users,id',
            'room_no' => 'required|exists:room,id',
            'date' => 'required',
            'time_from' => ['bail', 'required', function ($value, $attribute, $fail) use ($request) {
                if (date('Y-m-d H:i', strtotime(date('Y-m-d', strtotime($request->date)) . ' ' . date('H:i', strtotime($request->time_from)))) < date('Y-m-d H:i')) {
                    $fail('please choose a proper time from field');
                }
            }],
            'time_to' => [
                'bail', 'required',
                function ($attribute, $value, $fail) use ($request) {
                    if (date('H:i', strtotime($value)) < date('H:i', strtotime($request->time_from))) {
                        $fail('Time must be higher then time from.');
                    }
                }
            ],
            'vc' => 'required',
            'projector' => 'required',
            'refreshment' => 'required'
        ]);
        if ($validator->fails()) return $this->set_response(null, 422, 'error', $validator->errors()->all());

        $duplicateRoom = RoomBook::where('booked_date', date('Y-m-d', strtotime($request->date)))
            ->where('room_id', $request->room_no)
            ->where('booked_time_to', '>=', date('H:i', strtotime('-5 minutes', strtotime($request->time_from))))
            ->where('booked_time_from', '<=', date('H:i', strtotime('-5 minutes', strtotime($request->time_to))))
            ->where('status', 1)
            ->first();
        if ($duplicateRoom) {
            Log::debug('duplicate Room ==> ' . json_encode($duplicateRoom));
            return $this->set_response(null, 422, 'error', ['This room is already booked by ' . $duplicateRoom->bookedBy->name . ' & Meeting  title is : ' . $duplicateRoom->title . ' which conflict with your time.']);
        }
        try {
            DB::beginTransaction();
            $room = RoomBook::create([
                'title' => $request->title,
                'booked_by' => auth()->user()->id,
                'booked_date' => date('Y-m-d', strtotime($request->date)),
                'booked_time_from' => date('H:i', strtotime($request->time_from)),
                'booked_time_to' => date('H:i', strtotime($request->time_to)),
                'allocate_with' => json_encode($request->include),
                'room_id' => $request->room_no,
                'vc' => $request->vc,
                'refreshment' => $request->refreshment,
                'projector' => $request->projector,
            ]);
            if ($request->filled('vc') && $request->vc) {
                $zoom = Room::where('name', 'Zoom')->first()->id;
                $duplicateZoom = RoomBook::where('booked_date', date('Y-m-d', strtotime($request->date)))
                    ->where('room_id', $zoom)
                    ->where('booked_time_to', '>=', date('H:i', strtotime('-5 minutes', strtotime($request->time_from))))
                    ->where('booked_time_from', '<=', date('H:i', strtotime('-5 minutes', strtotime($request->time_to))))
                    ->where('status', 1)
                    ->first();
                if ($duplicateZoom) {
                    Log::debug('duplicate Zoom ==> ' . json_encode($duplicateZoom));
                    return $this->set_response(null, 422, 'error', ['Zoom room already booked in your prefered time']);
                }
                RoomBook::create([
                    'title' => $request->title,
                    'booked_by' => auth()->user()->id,
                    'booked_date' => date('Y-m-d', strtotime($request->date)),
                    'booked_time_from' => date('H:i', strtotime($request->time_from)),
                    'booked_time_to' => date('H:i', strtotime($request->time_to)),
                    'allocate_with' => json_encode($request->include),
                    'room_id' => $zoom
                ]);
                //  mail to it
                if (in_array($request->room_no, [2, 4])) {
                    if (config('app.env') === 'production') {
                        Mail::send('emails.itreminder', ['room' => $room, 'status' => 1], function ($m) {
                            $m->from('hellobook@silkensewing.com', 'HelloBook');
                            $m->to('it@silkensewing.com', 'IT')->subject('Meeting Reminder!');
                        });
                    }
                }
            }
            $ho_rooom = Room::where('room_no', 'like', '%(HO)%')->pluck('id')->toArray();
            //  mial to perticipants
            foreach ($request->include as $user) {
                $user_data = User::findOrFail($user);
                if (config('app.env') === 'production') {
                    if ($user_data->email) {
                        Mail::send('emails.reminder', ['user' => $user_data, 'room' => $room, 'status' => 1], function ($m) use ($user_data) {
                            $m->from('hellobook@silkensewing.com', 'HelloBook');
                            $m->to($user_data->email, $user_data->name)->subject('Meeting Reminder!');
                        });
                        Notification::create([
                            'from' => auth()->user()->id,
                            'to' => $user,
                            'body' => $room->bookedBy->name . ' called a meeting on ' . $room->roomData->name . ' at ' . $request->time_from . ', ' . $request->date . '. Please make sure your availability.',
                            'no_type' => 1
                        ]);
                    }
                }
            }
            Log::info('Is Head office ==> ' . in_array($request->room_no, $ho_rooom));
            // mail to it
            if (config('app.env') === 'production') {
                if ($request->room_no == 8) {
                    Mail::send('emails.itreminder', ['room' => $room, 'status' => 1], function ($m) {
                        $m->from('hellobook@silkensewing.com', 'HelloBook');
                        $m->to('it@silkensewing.com', 'IT')->subject('Meeting Reminder!');
                    });
                }
                if (in_array($request->room_no, $ho_rooom)) {
                    Mail::send('emails.reception', ['room' => $room, 'status' => 1], function ($m) use ($user_data) {
                        $m->from('hellobook@silkensewing.com', 'HelloBook');

                        $m->to('frontdesk@silkensewing.com', 'Front Desk')->subject('Meeting Reminder!');
                    });
                } else {
                    Mail::send('emails.reception', ['room' => $room, 'status' => 1], function ($m) use ($user_data) {
                        $m->from('hellobook@silkensewing.com', 'HelloBook');

                        $m->to('reception@silkensewing.com', 'Reception')->subject('Meeting Reminder!');
                    });
                }
            }
            DB::commit();
            return $this->set_response(null, 200, 'success', ['This room booked successfully']);
        } catch (\Exception $e) {
            Log::error('Room book failed. reason ==> ' . $e->getMessage());
            DB::rollback();
            return $this->set_response(null, 422, 'error', ['something went wrong']);
        }
    }

    public function delete(Request $request)
    {
        try {
            $room = RoomBook::findOrFail($request->meeting_id);
            // Meeting cancel info mail
            foreach (json_decode($room->allocate_with) as $user) {
                // Meeting info mail
                $user_data = User::findOrFail($user);
                Mail::send('emails.reminder', ['user' => $user_data, 'room' => $room, 'status' => 0], function ($m) use ($user_data) {
                    $m->from('hellobook@silkensewing.com', 'HelloBook');

                    $m->to($user_data->email, $user_data->name)->subject('Meeting Cancel!');
                });
                Notification::create([
                    'from' => auth()->user()->id,
                    'to' => $user,
                    'body' => 'Meeting canceled. Called by ' . $room->bookedBy->name . ' on ' . $room->roomData->name . ' at ' . $request->time_from . ', ' . $request->date . '. Sorry for this inconvenience.',
                    'no_type' => 1
                ]);
            }
            $ho_rooom = Room::where('room_no', 'like', '%(HO)%')->pluck('id')->toArray();
            if (in_array($room->room_id, $ho_rooom)) {
                Mail::send('emails.reception', ['room' => $room, 'status' => 0], function ($m) use ($user_data) {
                    $m->from('hellobook@silkensewing.com', 'HelloBook');

                    $m->to('frontdesk@silkensewing.com', 'Front Desk')->subject('Meeting Cancel!');
                });
            } else {
                Mail::send('emails.reception', ['room' => $room, 'status' => 0], function ($m) use ($user_data) {
                    $m->from('hellobook@silkensewing.com', 'HelloBook');

                    $m->to('reception@silkensewing.com', 'Reception')->subject('Meeting Cancel!');
                });
            }
            $room->delete();
            return $this->set_response(null, 422, 'error', ['successMsg', 'Meeting canceled!!!']);
        } catch (\Exception $e) {
            Log::error('Room book failed. reason ==> ' . $e->getMessage());
            DB::rollback();
            return $this->set_response(null, 422, 'error', ['something went wrong']);
        }
    }

    public function getAvailableRoom(Request $request)
    {
        $reqTime = array('start' => date('H:i',strtotime($request->time_from)), 'end' => date('H:i',strtotime($request->time_to)));
        $meetingTime = array('start' => strtotime('10:00 AM'), 'end' => strtotime('11:15 AM'));
        $isConflict = 0;
        if (($reqTime['start'] <= $meetingTime['end']) && ($reqTime['end'] >= $meetingTime['start'])) {
            $isConflict = 1;
        }
        $booked_room = [];
        if ($request->time_from && $request->time_to) {
            $booked_room = RoomBook::where('booked_date', date('Y-m-d', strtotime($request->date)))
                ->where('status', 1)
                ->where('booked_time_to', '>=', date('H:i', strtotime('-5 minutes', strtotime($request->time_from))))
                ->where('booked_time_from', '<=', date('H:i', strtotime('-5 minutes', strtotime($request->time_to))))
                ->pluck('room_id')->toArray();
        }
        $room = Room::whereNotIn('id', $booked_room);
        if ($isConflict) {
            $room->where('id', '!=', 2);
        }
        $room = $room->get()->map(function ($r) {
            return [
                'value' => $r->id,
                'label' => $r->name
            ];
        });

        return $this->set_response($room, 200, 'success', ['This room list']);
    }

    public function getAllocatedPersonsList($data)
    {
        return allocateusers(json_decode($data))->map(function ($u) {
            return [
                'id' => $u->id,
                'name' => $u->name,
                'email' => $u->email,
                'msisdn' => $u->msisdn,
                'employee_id' => $u->employee_id,
                'username' => $u->username
            ];
        });
    }

    public function additionalBookingData(Request $request)
    {
        $data['room'] = Room::all()->map(function ($r) {
            return [
                'value' => $r->id,
                'label' => $r->name
            ];
        });
        $data['users'] = User::Where('id', '!=', auth()->user()->id)->get()->map(function ($r) {
            return [
                'value' => $r->id,
                'label' => $r->name
            ];
        });
        return $this->set_response($data, 200, 'success', ['additional data']);
    }
}
