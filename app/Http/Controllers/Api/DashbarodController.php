<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use App\Room;
use App\RoomBook;
use App\Sim;
use App\SimRequest;
use App\User;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashbarodController extends Controller
{

    use ApiResponseTrait;

    public function index(Request $request)
    {
        $meetings = RoomBook::orderBy('id', 'desc')->take(100)->get();
        foreach ($meetings as $meeting) {
            if(!isset($data['total_meeting'][$meeting->booked_date])){
                $data['total_meeting'][$meeting->booked_date] = [];
            }
            array_push($data['total_meeting'][$meeting->booked_date], [
                'room' => $meeting->roomData->name,
                'name' => $meeting->title,
                'date' => $meeting->booked_date,
                'start' => date('H:i A', strtotime($meeting->booked_date . ' ' . $meeting->booked_time_from)),
                'end' => date('H:i A', strtotime($meeting->booked_date . ' ' . $meeting->booked_time_to)),
                'host' => $meeting->bookedBy->name,
                'host_image' => asset('/uploads') . '/' . $meeting->bookedBy->image
            ]);
        };
        return $this->set_response($data, 200, 'success', ['dashboard data retrived']);
    }

    public function report(Request $request)
    {
        $data['room'] = Room::count();
        $data['past_total_meeting'] = RoomBook::whereBetween('booked_date', [Carbon::now()->subDay(6)->toDateString(), Carbon::now()->toDateString()])->count();
        $data['coming_total_meeting'] = RoomBook::where('booked_date', '>', Carbon::now()->toDateString())->count();
        $data['total_request'] = SimRequest::count();
        $data['user_meeting'] = User::select(DB::raw('users.*,count(room_booking.booked_by) as total'))
            ->join('room_booking', 'room_booking.booked_by', 'users.id')
            ->whereMonth('room_booking.created_at', Carbon::now()->month)
            ->whereYear('room_booking.created_at', Carbon::now()->year)
            ->groupBy('room_booking.booked_by')
            ->orderBy('total', 'desc')
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get()->map(function($data){
                return [
                    'name' => $data->name,
                    'image' => asset('/uploads') . '/' . $data->image,
                    "designation"=> $data->getDesignation->name,
                    "department"=> $data->getDepartment->name,
                    "email"=> $data->email,
                    'total' => $data->total
                ];
            });
        $data['total_sim'] = Sim::select(DB::raw('sum(case when is_assigned = 1 then 1 else 0 end) as assigned, sum(case when is_assigned = 0 then 1 else 0 end) as un_assigned'))->first();
        return $this->set_response($data, 200, 'success', ['report data retrived']);
    }
}
