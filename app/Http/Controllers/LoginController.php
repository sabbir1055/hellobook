<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request){

        $request->validate([
            'email' => 'required|exists:users,username|max:20',
            'password' => 'required|min:8',
        ]);
        auth()->attempt(['username'=>$request->email,'password'=>$request->password,'status'=>1]);
        if(auth()->check()){
            return redirect()->route('dashboard');
        }else{
            session()->flash('errorMsg','Username and password doesn\'t match.');
            return back()->withInput();
        };
    }

    public function logout(Request $request){
        auth()->logout();
        return redirect('/');
    }
}
