<?php

namespace App\Http\Controllers;

use App\Department;
use App\Designation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{
    public function index(Request $request){
        $req = $request->all();
        $department = Department::with('complainSolver')
            ->paginate(10);
        return view('department.index',compact('req','department'));
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|unique:department,name',
            'short_name' => 'required|max:10'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        Department::firstOrCreate([
            'name' => $request->name,
            'short_name' => $request->short_name
        ]);
        session()->flash('successMsg','Department added successfully!!!');
        return back();
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|unique:department,name,'.$id,
            'short_name' => 'required|max:10'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $department = Department::findOrFail($id);
        $department->update([
            'name' => $request->name,
            'short_name' => $request->short_name
        ]);
        session()->flash('successMsg','Department updated successfully!!!');
        return back();
    }

    public function designation(Request $request){
        return Designation::where('department_id',$request->department)->get();
    }
}
