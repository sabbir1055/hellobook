<?php

namespace App\Http\Controllers;

use App\Room;
use Validator;
use Log;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    public function index(Request $request){
        $req = $request->all();
        $room = Room::with(['getMeetings' => function($q){
            $q->where('status', 1);
        }])->paginate(10);
        return view('room.index',compact('room','req'));
    }

    public function store(Request $request){
        Log::info('Room Request ==> ' . json_encode($request->all()));
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'short_name' => 'required',
            'available_seat' => 'required'
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        Room::firstOrCreate([
            'name' => $request->name,
            'short_name' => $request->short_name,
            'room_no' => $request->room_no,
            'available_seat' => $request->available_seat
        ]);
        session()->flash('successMsg','Room added successfully!!!');
        return back();
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'short_name' => 'required',
            'available_seat' => 'required'
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $room = Room::findOrFail($id);
        $room->update([
            'name' => $request->name,
            'short_name' => $request->short_name,
            'room_no' => $request->room_no,
            'available_seat' => $request->available_seat
        ]);
        session()->flash('successMsg','Room added successfully!!!');
        return back();
    }
}
