<?php

namespace App\Http\Traits;

use Zend\Diactoros\Request;

trait Curl{

    private $data = null;
    private function get($url,$body=[]){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('BASE_URL').'/'.$url.'/?'.http_build_query($body),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
//                "Authorization: Bearer ".$request->session()->get('token') ?? ''//$request->session()->put('token','token');
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $this->data = (json_decode($response));
        }

        return $this->data;
    }


    private function post($request,$url,$body=[]){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('BASE_URL').'/'.$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
                "Authorization: Bearer ".$request->session()->get('token') ?? '' //$request->session()->put('token','token');
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $this->data = (json_decode($response));
        }

        return $this->data;
    }
}