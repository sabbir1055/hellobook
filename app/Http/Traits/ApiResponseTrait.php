<?php
namespace App\Http\Traits;

trait ApiResponseTrait
{
    /***
     * API response
     *
     * @return request feedback with status code
     */
    public function  set_response($data, $status_code, $status, $details)
    {
        $resData = response(json_encode(
                [
                    'status'        =>  $status, // true or false
                    'code'          =>  $status_code,
                    'data'          =>  $data,
                    'message'       =>  $details
                ]
        ), 200)
        ->header('Content-Type', 'application/json');

        $data = [];

        return $resData;
    }
}
