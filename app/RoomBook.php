<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomBook extends Model
{
    protected $table = 'room_booking';
    protected $guarded =  [];

    public function bookedBy(){
        return $this->belongsTo(User::class,'booked_by','id');
    }

    public function roomData(){
        return $this->belongsTo(Room::class,'room_id','id');
    }
}
