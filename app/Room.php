<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'room';
    protected $guarded =  [];

    public function getMeetings(){
        return $this->hasMany(RoomBook::class,'room_id','id')->where('booked_date','>=',date('Y-m-d'))->orderBy('booked_date','asc');
    }
}
