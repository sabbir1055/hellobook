<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sim extends Model
{
    protected $table = "sim";

    protected $guarded = [];

    public function assignedUser(){
        return $this->hasOneThrough(User::class,UserSim::class,'sim_id','id','id','user_id');
    }

    public function getUnAssigned(){
        return $this->hasOne(UserSim::class,'sim_id')->orderBy('unassigned_date','desc');
    }
}
