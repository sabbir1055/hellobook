<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class AllComplainExport implements FromView, WithTitle
{
    private $summaryData;
    private $categoryData;
    private $overdueData;
    private $responseTimeData;
    private $req;

    public function __construct($summaryData, $categoryData, $overdueData, $responseTimeData, $req)
    {
        $this->summaryData = $summaryData;
        $this->categoryData = $categoryData;
        $this->overdueData = $overdueData;
        $this->responseTimeData = $responseTimeData;
        $this->req = $req;
    }

    public function title(): string
    {
        // Ensure the title is within the 31-character limit
        return substr('Complaint Report Summary', 0, 30);
    }
    public function view(): View
    {
        $summaryData = $this->summaryData;
        $categoryData = $this->categoryData;
        $overdueData = $this->overdueData;
        $responseTimeData = $this->responseTimeData;
        $req = $this->req;
        return view('exports.complains.exportAllComplains', compact('summaryData', 'categoryData', 'overdueData', 'responseTimeData', 'req'));
    }
}
