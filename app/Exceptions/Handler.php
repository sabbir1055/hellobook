<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use App\Http\Traits\ApiResponseTrait;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{

    use ApiResponseTrait;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        $urls = explode('/public/', url()->full());
        if (isset($urls[1]) && explode('/', $urls[1])[0] === 'api') {
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
            if ($e instanceof HttpResponseException) {
                $status = Response::HTTP_INTERNAL_SERVER_ERROR;
            } elseif ($e instanceof MethodNotAllowedHttpException) {
                $status = Response::HTTP_METHOD_NOT_ALLOWED;
                $e = new MethodNotAllowedHttpException([], 'Method not allowed', $e);
            } elseif ($e instanceof NotFoundHttpException) {
                $status = Response::HTTP_NOT_FOUND;
                $e = new NotFoundHttpException('404 not found');
            } elseif ($e instanceof AuthorizationException) {
                $status = Response::HTTP_FORBIDDEN;
                $e = new AuthorizationException('403 action forbidden', $status);
            } elseif ($e instanceof \Dotenv\Exception\ValidationException && $e->getResponse()) {
                $status = Response::HTTP_BAD_REQUEST;
                $e = new \Dotenv\Exception\ValidationException('403 bad request', $status, $e);
            } elseif ($e) {
                $e = new HttpException($status, 'Server error');
            }
            return $this->set_response(null, $status, 'error', [$e->getMessage()]);
        } else {
            return parent::render($request, $e);
        }
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($exception->guards()[0] === 'api') {
            return $this->set_response(null, 401, 'error', ['unauthorized please login first']);
        } else {
            return redirect($exception->redirectTo());
        }
    }
}
