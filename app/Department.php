<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'department';
    protected $guarded =  [];

    public function complainSolver()
    {
        return $this->belongsTo(User::class, 'complain_solver_employee_id', 'employee_id');
    }
}
