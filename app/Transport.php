<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    protected $table = 'transports';
    protected $guarded = [];

    public function getTripList(){
        return $this->hasMany(TransportTrip::class, 'transport_id', 'id');
    }
}
