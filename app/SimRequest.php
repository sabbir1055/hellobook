<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SimRequest extends Model
{
    protected $table = "sim_request_list";
    protected $guarded = [];

    protected $appends = ['get_status'];
 
    public function getUser(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function getRequestUser(){
        return $this->belongsTo(User::class,'request_by');
    }

    public function getApproveUser(){
        return $this->hasOne(User::class,'id','accept_by');
    }

    public function getGetStatusAttribute(){
        $status[0] = 'Pending';
        $status[1] = 'Accepted';
        $status[2] = 'Declined';
        return $status[$this->is_accepted];
    }
}
