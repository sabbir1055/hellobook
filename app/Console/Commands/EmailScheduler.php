<?php

namespace App\Console\Commands;

use App\EmailLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class EmailScheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send email from queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $queueEmails = EmailLog::where('status', 0)->take(10)->get();
        foreach ($queueEmails as $email){
            $email->update([
                'status' => 2,
                'send_at' => now()
            ]);
            try {
                Mail::send($this->__getTemplate($email), ['to_user' => $email->to,'fromUser' => $email->from, 'booking' => $email->booking, 'status' => $email->mail_status], function ($m) use($email) {
                    $m->from($email->from_email, $email->from_app);
                    $m->to($email->to_email, $email->to_app)->subject($email->subject);
                });
                $email->update([
                    'status' => 1,
                    'send_at' => now()
                ]);
            }catch (\Exception $e){
                Log::error($e);
            }
        }
    }

    private function __getTemplate($email)
    {
        switch ($email->to_app){
            case 'Front Desk':
                return 'emails.reception';
                break;
            case 'Reception':
                return 'emails.reception';
                break;
            case 'IT Zoom':
                return 'emails.itZoomReminder';
                break;
            case 'IT Projector':
                return 'emails.itProjectorReminder';
                break;
            default:
                return 'emails.reminder';
                break;
        }
    }
}
