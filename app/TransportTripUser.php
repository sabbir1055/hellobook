<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportTripUser extends Model
{
    protected $table = 'trip_user';
    protected $guarded = [];
    public $timestamps = false;
}
