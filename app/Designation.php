<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $table = 'designations';
    protected $guarded =  [];

    public function getDepartment(){
        return $this->belongsTo(Department::class,'department_id','id');
    }
}
