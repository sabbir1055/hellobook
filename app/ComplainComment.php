<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplainComment extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function getImageAttribute($value)
    {
        return $value ? asset('/').$value : null;
    }
}
