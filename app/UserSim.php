<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSim extends Model
{
    protected $table = "user_sim";
    protected $guarded = [];
    public $timestamps = false;
}
