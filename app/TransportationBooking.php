<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportationBooking extends Model
{
    protected $table = 'transportation_booking';
    protected $guarded = [];

    public function transportDetail () {
        return $this->belongsTo(Transport::class, 'transport_id','id');
    }

    public function getrip()
    {
        return $this->hasOne(TransportTrip::class, 'id', 'trip_no');
    }

    public function bookedBy () {
        return $this->belongsTo(User::class, 'booked_by','id');
    }
}
