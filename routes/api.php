<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    Route::post('/login', 'LoginController@login');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/dashboard', 'DashbarodController@index');
        Route::post('/report', 'DashbarodController@report');
        Route::post('/logout', 'LoginController@logout');
        Route::post('/meeting-book', 'RoomBookingController@store');
        Route::post('/meeting-list', 'RoomBookingController@index');
        Route::post('/meeting-cancel', 'RoomBookingController@delete');
        Route::post('/get-available-room', 'RoomBookingController@getAvailableRoom');
        Route::post('/get-additional-data', 'RoomBookingController@additionalBookingData');
    });
});