<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/login');
});
Route::get('/login', function () {
    return view('login');
});
Route::post('/login', 'LoginController@login')->name('login');
Route::get('/user-info/{data}', 'UserController@userInfo')->name('user.info');

Route::group(['middleware'=>'auth:web'],function (){
    Route::get('/dashboard','DashbarodController@index')->name('dashboard');
    Route::get('/profile','UserController@profile')->name('profile');
    Route::post('/profile','UserController@profileUpdate')->name('profile.update');
    Route::get('/logout','LoginController@logout')->name('logout');
    // Designation Route
    Route::group(['prefix'=>'/designation'],function (){
        Route::get('/','DesignationController@index')->name('designation.index')->middleware('permission:designation_manage');
        Route::post('/store','DesignationController@store')->name('designation.store')->middleware('permission:designation_manage');
        Route::post('/{id}/update','DesignationController@update')->name('designation.update')->middleware('permission:designation_manage');
    });
    // Department Route
    Route::group(['prefix'=>'/department'],function (){
        Route::get('/','DepartmentController@index')->name('department.index')->middleware('permission:department_manage');
        Route::post('/store','DepartmentController@store')->name('department.store')->middleware('permission:department_manage');
        Route::post('/{id}/update','DepartmentController@update')->name('department.update')->middleware('permission:department_manage');
        Route::post('/get-designation','DepartmentController@designation')->name('department.designation')->middleware('permission:department_manage');
    });
    // Role Route
    Route::group(['prefix'=>'/role'],function (){
        Route::get('/','RoleController@index')->name('role.index')->middleware('permission:manage_role');
        Route::post('/store','RoleController@store')->name('role.store')->middleware('permission:manage_role');
        Route::post('/{id}/update','RoleController@update')->name('role.update')->middleware('permission:manage_role');
    });
    // Permission Route
    Route::group(['prefix'=>'/permission'],function (){
        Route::get('/','PermissionController@index')->name('permission.index')->middleware('permission:manage_permission');
        Route::post('/store','PermissionController@store')->name('permission.store')->middleware('permission:manage_permission');
        Route::post('/{id}/update','PermissionController@update')->name('permission.update')->middleware('permission:manage_permission');
    });
    // User Route
    Route::group(['prefix'=>'/user'],function (){
        Route::get('/','UserController@index')->name('user.index')->middleware('permission:manage_user');
        Route::post('/store','UserController@store')->name('user.store')->middleware('permission:add_user');
        Route::get('/{user}/update-complain-solver-status','UserController@complainSolverStatusToggle')->name('user.complainSolver.status.toggle')->middleware('permission:add_user');
        Route::post('/{id}/update','UserController@update')->name('user.update')->middleware('permission:add_user');
        Route::get('/add-bulk','UserController@addBulk')->name('user.bulk')->middleware('permission:add_bulk_user');
        Route::post('/store-bulk','UserController@storeBulk')->name('user.bulk.store')->middleware('permission:add_bulk_user');
        Route::get('/generate-qr/{user}', 'UserController@generateQr')->name('user.generate.qr')->middleware('permission:generate_qr');
    });
    // Room Route
    Route::group(['prefix'=>'/room'],function (){
        Route::get('/','RoomController@index')->name('room.index')->middleware('permission:room_list');
        Route::post('/store','RoomController@store')->name('room.store')->middleware('permission:add_room');
        Route::post('/{id}/update','RoomController@update')->name('room.update')->middleware('permission:add_room');
    });
    // Booking Room Route
    Route::group(['prefix'=>'/booking-room'],function (){
        Route::get('/','RoomBookingController@index')->name('booking-room.index')->middleware('permission:booked_room_list');
        Route::post('/store','RoomBookingController@store')->name('booking-room.store')->middleware('permission:book_room');
        Route::post('/{id}/update','RoomBookingController@update')->name('booking-room.update')->middleware('permission:book_room');
        Route::get('/{id}/delete','RoomBookingController@delete')->name('booking-room.delete')->middleware('permission:book_room');
        Route::post('/available-room','RoomBookingController@getAvailableRoom')->name('available.room');
    });
    Route::get('/read-all','NotificationController@readNotification')->name('read.all');
    Route::post('/notification-all','NotificationController@notification')->name('notification.all');
    Route::get('/notification/{notification}/detail','NotificationController@detail')->name('notification.detail');
    // Transport Routes
    Route::group(['prefix'=>'/transport'],function (){
        Route::get('/','TranspotController@index')->name('transport.index')->middleware('permission:add_transport');
        Route::post('/store','TranspotController@store')->name('transport.store')->middleware('permission:add_transport');
        Route::post('/{id}/update','TranspotController@update')->name('transport.update')->middleware('permission:update_transport');
        Route::group(['prefix'=> '/trip'],function(){
            Route::get('/{transport}','TranspotController@tripList')->name('transport.trip.list');
            Route::post('/{transport}/{trip}/update','TranspotController@tripUpdate')->name('transport.trip.update');
        });
    });
    // Transport Request
    Route::group(['prefix'=>'/transport-request'],function (){
        Route::get('/','TranspotController@requestIndex')->name('transport.request.index')->middleware('permission:add_transport_request');
        Route::post('/store','TranspotController@requestStore')->name('transport.request.store')->middleware('permission:add_transport_request');
        Route::post('/{id}/delete','TranspotController@requestDelete')->name('transport.request.delete')->middleware('permission:update_transport_request');
        Route::get('/{id}/status-approve','TranspotController@requestApprove')->name('transport.request.status.approve')->middleware('permission:approve_reject_transport_request');
        Route::get('/approval/list','TranspotController@requestApprovalIndex')->name('transport.request.approval.index')->middleware('permission:approve_reject_transport_request');
        Route::get('/{id}/status-withdraw','TranspotController@requestStatusWithdraw')->name('transport.request.status.delete')->middleware('permission:update_transport_request');
        Route::post('/status-decline','TranspotController@requestReject')->name('transport.request.status.decline')->middleware('permission:approve_reject_transport_request');
        Route::post('/available-transport','TranspotController@getAvailableTransport')->name('available.transport');
        Route::post('/available-trip','TranspotController@getAvailableTrip')->name('available.trip');
    });
    // Sim management Route
    Route::group(['prefix'=>'/sim-management'],function (){

        Route::get('/','SimManagementController@index')->name('sim.index')->middleware('permission:sim_add_assign');
        Route::post('/store','SimManagementController@store')->name('sim.store')->middleware('permission:add_sim');
        Route::post('/{id}/update','SimManagementController@update')->name('sim.update')->middleware('permission:add_sim');
        Route::post('/assign','SimManagementController@assign')->name('sim.assign')->middleware('permission:sim_assign');
        Route::get('/{id}/unassign','SimManagementController@unAssign')->name('sim.unassign')->middleware('permission:sim_assign');
        Route::get('/report/{id}','SimManagementController@report')->name('sim.report')->middleware('permission:sim_manage');

        Route::get('/request-list','SimManagementController@requestList')->name('sim.request.list')->middleware('permission:sim_request_pending_list');
        Route::post('/request','SimManagementController@requestStore')->name('sim.request.store')->middleware('permission:add_sim_request');
        Route::post('/request/{id}/update','SimManagementController@requestupdate')->name('sim.request.update')->middleware('permission:add_sim_request');


        Route::get('/request','SimManagementController@request')->name('sim.request')->middleware('permission:sim_request_list');
        Route::get('/request/{id}/view','SimManagementController@requestView')->name('sim.request.view')->middleware('permission:add_sim_request');
        Route::get('/request-list/{id}/view','SimManagementController@requestActionView')->name('sim_rqeuest_action.view')->middleware('permission:sim_request_list');

        Route::post('/request-list/{id}/accept','SimManagementController@requestAccept')->name('sim.request.accept')->middleware('permission:sim_assign');
        Route::post('/request-list/{id}/decline','SimManagementController@requestDecline')->name('sim.request.decline')->middleware('permission:sim_assign');


    });

    // Complain management
    Route::group(['prefix'=>'/complain'],function (){
        Route::get('/','ComplainManagementController@index')->name('complain.index')->middleware('permission:register_complain');
        Route::get('/solving-complains','ComplainManagementController@solverIndex')->name('complain.solvable.index')->middleware('permission:solve_complain');
        Route::get('/all-complains','ComplainManagementController@allIndex')->name('complain.all.index')->middleware('permission:solve_complain|view_subordinate_complains|view_all_complains');
        Route::get('/register','ComplainManagementController@create')->name('complain.create')->middleware('permission:register_complain');
        Route::post('/store','ComplainManagementController@store')->name('complain.store')->middleware('permission:register_complain');
        Route::get('/{complain}/detail','ComplainManagementController@view')->name('complain.view')->middleware('permission:register_complain|solve_complain|view_subordinate_complains|view_all_complains');
        Route::post('/status-update','ComplainManagementController@statusUpdate')->name('complain.status.update')->middleware('permission:register_complain|solve_complain');
        Route::post('/{complain}/add-comment','ComplainManagementController@storeComment')->name('complain.comment.add')->middleware('permission:register_complain|solve_complain');
    });
});
